module.exports = {

    /**
     * Methed check id null
     * return 
     */
    check_id_null : (strInp) => {
        let isFlat = true;
        if(strInp == null || strInp == '' || strInp == "" ) {
            isFlat = false;
        }
        return isFlat;
    },
    
    /**
     * Methed check object null
     * return 
     */
    check_obj_null : (objInp) => {
        let isFlat = true;
        if(objInp == null || !objInp ) {
            isFlat = false;
        }
        return isFlat;
    },
    
}