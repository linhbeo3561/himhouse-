module.exports = {
    'MESS_PRO_OO1' : 'Product is empty',
    'MESS_PRO_OO2' : 'Product is already exist',
    'MESS_PRO_OO3' : 'Add Product successfully',
    'MESS_PRO_OO4' : 'Delete Product successfully',
    'MESS_PRO_OO5' : 'Add Product Fail',
    'MESS_PRO_OO7' : 'Product not found',
}