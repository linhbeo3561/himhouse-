const express = require('express');
const router = express.Router();
const productTypeService = require('../repo/productTypeRepo');
const validCommon = require('../utils/common/valid');

/**
 * Method find all category
 * Return list data category
 */
router.get('/', (req, res, next) => {
    try {
        productTypeService.findAllProductType().then(cat => {
            if (cat && cat != null) {
                res.end(JSON.stringify(cat));
            } else {
                res.end(categoryMeg.MESS_CAT_OO1);
            }
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all category
 * Return list data category
 */
router.get('/:id', (req, res, next) => {
    try {
        let id_category = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_category);
        if (check_null_id) {
            productTypeService.findProductTypeByID(id_category).then(cat => {
                if (cat && cat != null) {
                    res.end(JSON.stringify(cat));
                } else {
                    res.end(categoryMeg.MESS_CAT_OO1);
                }
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all category
 * Return list data category
 */
router.get('/categories/:id', (req, res, next) => {
    try {
        let id_category = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_category);
        if (check_null_id) {
            productTypeService.findProductTypeByCategoryId(id_category).then(cat => {
                if (cat && cat != null) {
                    res.end(JSON.stringify(cat));
                } else {
                    res.end(categoryMeg.MESS_CAT_OO1);
                }
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method delete product by id
 */
router.post('/:id', (req, res) => {
    let id_category = req.params['id'];
    let check_null_id = validCommon.check_id_null(id_category);
    if (check_null_id) {
        productTypeService.deleteTypeById(id_category).then(result => {
            res.end(JSON.stringify(result));
        });
    }
});

/**
 * Method delete product type by id
 */
router.get('/delete-prod-type/:id', (req, res) => {
    let id_category = req.params['id'];
    let check_null_id = validCommon.check_id_null(id_category);
    if (check_null_id) {
        productTypeService.deleteTypeById(id_category).then(result => {
            res.end(JSON.stringify(result));
        });
    }
});

/**
 * Method delete product type by id
 */
router.get('/is-exit-prod-name/:name', (req, res) => {
    let name = req.params['name'];
    let check_null_name = validCommon.check_id_null(name);
    if (check_null_name) {
        productTypeService.checkExitProdName(name).then(result => {
            res.end(JSON.stringify(result));
        });
    }
});

/**
 * Create new prod type
 */
router.post('/insert/prod-type', (req, res) => {
    let prodType = req.body;
    let check_null_prodType = validCommon.check_id_null(prodType);
    if (check_null_prodType) {
        let jsonBulkCreata = [];
        let prodTypeNameArray = prodType.name.split(",");
        prodTypeNameArray.forEach(element => {
            let json = {
                "categories_id": prodType.categories_id,
                "name": element,
            }
            jsonBulkCreata.push(json);
        });
        productTypeService.insertProductType(jsonBulkCreata).then(result => {
            res.end(JSON.stringify(result));
        });
    }
});

module.exports = router;