const express = require('express');
const router = express.Router();
const validCommon = require('../utils/common/valid');
const accountMeg = require('../utils/message/accountMessage');
const accountService = require('../repo/accountRepo');
const userService = require('../repo/userRepo');

/**
 * Method update account by id
 */
router.get('/update-account-by-id/:id', (req, res) => {
    try {
        let isFlat = false;
        let id = req.params['id'];
        let role_id = req.query['role'] || '-1';
        let active = req.query['active'] || '-1';
        if (validCommon.check_id_null(id)) {
            accountService.findAccountByID(id).then(result => {
                if (result) {
                    let acc = result.dataValues;
                    // ------------------------------------------------------
                    if (role_id != '-2') {
                        accountService.changeRole(acc.id, role_id).then(_res => {
                            if (_res) {
                                isFlat = true;
                            }
                        });
                    }
                    // ------------------------------------------------------
                    if (active != '-1') {
                        let isBool = true;
                        if (active == 0) {
                            isBool = false;
                        }
                        accountService.deleteAccount(acc.id, isBool).then(_res => {
                            if (_res) {
                                isFlat = true;
                            }
                        });
                    }
                    res.end(JSON.stringify(isFlat));
                } else {
                    res.end(JSON.stringify(isFlat));
                }
            });
        };
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find by id
 */
router.get('/find-by-id/:id', (req, res) => {
    try {
        let id = req.params['id'];
        let check_null_id = validCommon.check_id_null(id);
        if (check_null_id) {
            accountService.findAccountByID(id).then(user => {
                res.end(JSON.stringify(user));
            });
        } else {
            res.end(JSON.stringify(""));
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all acc
 */
router.get('/:role_id', (req, res) => {
    try {
        let role_id = req.params['role_id'];
        let check_null_id = validCommon.check_id_null(role_id);
        if (check_null_id) {
            accountService.findAllByRoleId(role_id).then(acc => {
                res.end(JSON.stringify(acc));
            });
        } else {
            res.end(JSON.stringify(""));
        }
    } catch (error) {
        console.log(error);
    }
});



/**
 * Method find all acc by username
 */
router.get('/find-by-uname/:username', (req, res) => {
    try {
        let username = req.params['username'];
        let check_null_username = validCommon.check_id_null(username);
        if (check_null_username) {
            accountService.findAllByUsername(username).then(acc => {
                res.end(JSON.stringify(acc));
            });
        } else {
            res.end(JSON.stringify(""));
        }
    } catch (error) {
        console.log(error);
    }
});


/**
 * Method find all acc
 */
router.get('/find-by-role/:role_id', (req, res) => {
    try {
        let role_id = req.params['role_id'];
        let check_null_id = validCommon.check_id_null(role_id);
        if (check_null_id) {
            accountService.findAllByRole(role_id).then(acc => {
                res.end(JSON.stringify(acc));
            });
        } else {
            res.end(JSON.stringify(""));
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all acc
 */
router.post('/find-by-password', (req, res) => {
    try {
        if (req.body && req.body != null) {
            accountService.findAllByPassword(req.body).then(acc => {
                res.end(JSON.stringify(acc));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method create acc and user
 * Return
 */
router.post('/signup', (req, res, next) => {
    try {
        if (req.body && req.body != null) {
            let new_info = req.body;
            accountService.createAccount(new_info).then(acc => {
                check_account_null = validCommon.check_obj_null(acc);
                if (check_account_null) {
                    new_info.account_id = acc.id;
                    userService.createUser(new_info).then(user => {
                        res.end(JSON.stringify(user));
                    });
                }
            });
        } else {
            res.end(accountMeg.MESS_ACC_OO1);
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method login using user and pass
 */
router.post('/login', (req, res, next) => {
    try {
        if (req.body && req.body != null) {
            let loginAccount = req.body;
            accountService.accountLogin(loginAccount).then(acc => {
                res.end(JSON.stringify(acc));
            });
        } else {
            res.end(accountMeg.MESS_ACC_OO1);
        }
    } catch (error) {
        console.log(error);
    }
});


/**
 * Method change passwords
 */
router.post('/update', (req, res, next) => {
    try {
        if (req.body && req.body != null) {
            let account_info = req.body;
            userService.editUser(account_info).then(_ => {
                res.end(JSON.stringify(_));
            });
        } else {
            res.end(accountMeg.MESS_ACC_OO1);
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method change passwords
 */
router.post('/update/account', (req, res, next) => {
    try {
        if (req.body && req.body != null) {
            let account_info = req.body;
            accountService.changePassword(account_info).then(_ => {
                res.end(JSON.stringify(_));
            });
        } else {
            res.end(accountMeg.MESS_ACC_OO1);
        }
    } catch (error) {
        console.log(error);
    }
});
/**
 * Method delete account infor
 */
router.delete('/:id', (req, res, next) => {
    try {
        let account_id = req.params['id'];
        let check_null_id = validCommon.check_id_null(account_id);
        if (check_null_id) {
            accountService.deleteAccount(account_id).then(acc => {
                res.end(JSON.stringify(acc));
            });
        }
    } catch (error) {
        console.log(error);
    }
});


module.exports = router;