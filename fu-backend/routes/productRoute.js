const express = require('express');
const router = express.Router();
const productService = require('../repo/productRepo');
const productMeg = require('../utils/message/productMessage');
const imagesService = require('../repo/imagesRepo');
const categoryMeg = require('../utils/message/categoryMessage');
const validCommon = require('../utils/common/valid');
const cateProductService = require('../repo/cateProductRepo');
const genderCateProductService = require('../repo/genderCateRepo');
/**
 * Method find all product
 * Return list data product
 */
router.get('/', (req, res, next) => {
    try {
        productService.findAllProduct().then(pro => {
            if (pro && pro != null) {
                res.end(JSON.stringify(pro));
            } else {
                res.end(productMeg.MESS_PRO_OO1);
            }
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find gender by product id
 */
router.get('/filter-gender-pid/:id', (req, res, next) => {
    try {
        let id_product = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_product);
        if (check_null_id) {
            genderCateProductService.findGenderByPId(id_product).then(pro => {
                res.end(JSON.stringify(pro));
            });
        }
    } catch (error) {
        console.log(error);
    }
});


/**
 * Method find gender by product id
 */
router.get('/filter-ids/:id', (req, res, next) => {
    try {
        let id_product = req.params['id'];
        productService.findAllListPid(id_product).then(pro => {
            res.end(JSON.stringify(pro));
        });
    } catch (error) {
        console.log(error);
    }
});



/**
 * Method find all product 4 new
 * Return list data product
 */
router.get('/top4/new', (req, res, next) => {
    try {
        productService.findAllProductByTop4New().then(pro => {
            if (pro && pro != null) {
                res.end(JSON.stringify(pro));
            } else {
                res.end(productMeg.MESS_PRO_OO1);
            }
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find product by ID
 * Return product
 */
router.get('/:id', (req, res, next) => {
    try {
        let id_product = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_product);
        if (check_null_id) {
            productService.findProductByID(id_product).then(pro => {
                res.end(JSON.stringify(pro));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find product by Category
 * Return products
 */
router.get('/type/:type_id', (req, res, next) => {
    try {
        let type_id = req.params['type_id'];
        let check_null_id = validCommon.check_id_null(type_id);
        if (check_null_id) {
            productService.findProductByTypeID(type_id).then(pro => {
                res.end(JSON.stringify(pro));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all by cate id
 */
router.get('/category/:cate_id/:gender_id', (req, res, next) => {
    try {
        let cate_id = req.params['cate_id'];
        let gender_id = req.params['gender_id'];
        let check_null_id = validCommon.check_id_null(cate_id);
        let check_null_id_gender = validCommon.check_id_null(gender_id);
        if (check_null_id && check_null_id_gender) {
            productService.findProductByCateId(cate_id, gender_id).then(pro => {
                res.end(JSON.stringify(pro));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all by cate id
 */
router.get('/gender/:gender_id', (req, res, next) => {
    try {
        let gender_id = req.params['gender_id'];
        let check_null_id = validCommon.check_id_null(gender_id);
        if (check_null_id) {
            productService.findCateID(gender_id).then(pro => {
                res.end(JSON.stringify(pro));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all by cate id
 */
router.get('/size/:size', (req, res, next) => {
    try {
        let size = req.params['size'];
        let check_null_size = validCommon.check_id_null(size);
        if (check_null_size) {
            productService.findProductBySize(size).then(pro => {
                res.end(JSON.stringify(pro));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all by cate id
 */
router.get('/update-inventory/:id/:quantity', (req, res, next) => {
    try {
        let id = req.params['id'];
        let quantity = req.params['quantity'];
        let check_null_id = validCommon.check_id_null(id);
        let check_null_quantity = validCommon.check_id_null(check_null_id);
        if (check_null_id && check_null_quantity) {
            productService.findProductByID(id).then(pro => {
                if (pro) {
                    let quantityTemp = pro.dataValues.inventory;
                    let quantityExit = quantityTemp - quantity;
                    productService.updateInventory(id, quantityExit).then(pro => {
                        res.end(JSON.stringify(pro));
                    });
                }
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method filter product
 * Return products
 */
router.post('/filter', (req, res, next) => {
    try {
        check_null_body = validCommon.check_obj_null(req.body)
        if (check_null_body) {
            let product = req.body;
            console.log(product);
            productService.filterProduct(product).then(products => {
                check_null_products = validCommon.check_obj_null(products)
                if (check_null_products) {
                    res.end(JSON.stringify(products))
                } else {
                    res.end("not found")
                }
            })
        } else {
            res.end(productMeg.MESS_PRO_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * Method add product
 */
router.post('/add', (req, res, next) => {
    try {
        if (req.body && req.body != null) {
            let new_product = req.body;
            productService.addnewProduct(new_product).then(new_pro => {
                let jsonBulkCreata = [];
                new_product.gender_id.forEach(element => {
                    let json = {
                        "categories_id": new_product.cat_id,
                        "product_id": new_pro.dataValues.id,
                        "gender_id": element
                    }
                    jsonBulkCreata.push(json);
                });
                // let jsonThum = {
                //     "product_id": new_pro.dataValues.id,
                //     "images": new_product.image
                // }
                // imagesService.createImagesThum(jsonThum).then(result => { console.log(result); })
                cateProductService.addNewCatePro(jsonBulkCreata).then(result => {
                    res.end(JSON.stringify(result));
                });
            });
        } else {
            res.end(productMeg.MESS_PRO_OO1);
        }
    } catch (error) {
        res.end(productMeg.MESS_PRO_OO5);
    }
});

/**
 * Method delete product
 */
router.delete('/:id', (req, res, next) => {
    try {
        let id_product = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_product);
        if (check_null_id) {
            productService.findProductByID(id_product).then(pro => {
                if (pro && pro != null) {
                    productService.deleteProduct(id_product).then(__ => {
                        res.end(JSON.stringify(__));
                    });
                }
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method update product
 */
router.post('/', (req, res, next) => {
    try {
        if (req.body && req.body != null) {
            let product = req.body;
            let jsonBulkCreata = [];
            productService.updateProduct(product).then(pro => {
                product.gender_id.forEach(element => {
                    let json = {
                        "categories_id": product.cat_id,
                        "product_id": product.id,
                        "gender_id": element
                    }
                    jsonBulkCreata.push(json);
                });
                cateProductService.deleteCateProByPId(product.id).then(_ => {
                    cateProductService.addNewCatePro(jsonBulkCreata).then(result => {
                        res.end(JSON.stringify(result));
                    });
                });
                res.end(JSON.stringify(pro));
            });
        } else {
            res.end(productMeg.MESS_PRO_OO1);
        }
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;