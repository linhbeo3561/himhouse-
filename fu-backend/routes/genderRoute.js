const express = require('express');
const router = express.Router();
const validCommon = require('../utils/common/valid');
const genderSerice = require('../repo/genderRepo');


/**
 * Method find all gender
 */
router.get('/', (req, res) => {
    try {
        genderSerice.findAllGender().then(gender => {
            res.end(JSON.stringify(gender));
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all gender
 */
router.get('/:gender_id', (req, res) => {
    try {
        let gender_id = req.params['gender_id'];
        let check_null_id = validCommon.check_id_null(gender_id);
        if (check_null_id) {
            genderSerice.findAllCategoriesByGenderID(gender_id).then(gender => {
                res.end(JSON.stringify(gender));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;