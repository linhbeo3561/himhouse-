const express = require('express');
const router = express.Router();
const validCommon = require('../utils/common/valid');
const orderMeg = require('../utils/message/orderMessage');
const orderService = require('../repo/orderRepo');
const productService = require('../repo/productRepo');
const orderDetailsService = require('../repo/orderDetailRepo');
const db = require('../migrate/db');

/*
 * Add order 
 */
router.post('/', (req, res) => {
    try {
        if (validCommon.check_obj_null(req.body)) {
            let newOder = req.body;
            console.log(newOder);
            orderService.createOrder(newOder).then(order => {
                res.end(JSON.stringify(order))
            });
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/*
 * Get order history by Custumer
 */
router.get('/customer/:order_id', (req, res, next) => {
    try {
        let order_id = req.params['order_id'];
        let check_null_id = validCommon.check_id_null(order_id);
        if (check_null_id) {
            let query_str = `SELECT
                                pr.NAME,
                                pr.ID,
                                pr.image,
                                pr.description,
                                od.color,
                                od.SIZE,
                                od.quantity,
                                od.status,
                                od.price_per_unit
                            FROM
                                order_details AS od
                                INNER JOIN products AS pr ON od.product_id = pr.ID 
                            WHERE
                                od.order_id = '${order_id}'
                            ORDER BY od."createdAt" DESC`;
            db.connection.query(query_str, {
                type: db.connection.QueryTypes.SELECT
            }).then(order => {
                res.end(JSON.stringify(order));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * 
 */
router.get('/:id', (req, res) => {
    try {
        let id_customer = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_customer);
        if (check_null_id) {
            orderService.getCustomerOrderHistory(id_customer).then(order => {
                res.end(JSON.stringify(order))
            })
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * 
 */
router.get('/info/:id', (req, res) => {
    try {
        let id = req.params['id'];
        let check_null_id = validCommon.check_id_null(id);
        if (check_null_id) {
            orderService.getOrderInfoById(id).then(order => {
                res.end(JSON.stringify(order))
            })
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * Method find all order
 */
router.get('/', (req, res) => {
    try {
        orderService.getOrderInfo().then(order => {
            res.end(JSON.stringify(order));
        });
    } catch (error) {
        console.log(error)
    }
});

/**
 * Method find all by status
 */
router.get('/status/:status', (req, res) => {
    try {
        let status = req.params['status'];
        let check_null_status = validCommon.check_id_null(status);
        if (check_null_status) {
            orderService.getOrderByStatus(status).then(order => {
                res.end(JSON.stringify(order))
            })
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * Method udate status by order id
 */
router.get('/update-status-by-order-id/:id/:status', (req, res) => {
    try {
        let id = req.params['id'];
        let status = req.params['status'];
        let check_null_id = validCommon.check_id_null(id);
        let check_null_status = validCommon.check_id_null(status);
        if (check_null_id && check_null_status) {
            orderService.updateOrderStatus(id, status).then(_res => {
                res.end(JSON.stringify(_res));
            });
            if (status == '5') {
<<<<<<< HEAD
=======
                console.log(id);
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
                orderDetailsService.getOrderdetailbyOrderId(id).then(order_detail => {
                    if (order_detail) {
                        let data = JSON.parse(JSON.stringify(order_detail));
                        data.forEach(element => {
                            productService.findProductByID(element.product_id).then(pro => {
                                if (pro) {
                                    let quantityTemp = pro.dataValues.inventory;
                                    let quantityExit = quantityTemp + element.quantity;
                                    productService.updateInventory(element.product_id, quantityExit).then(pro => {
                                        res.end(JSON.stringify(pro));
                                    });
                                }
                            });
                        });
                    }
                });
            }
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * Method update shipepr by order id and name
 */
router.get('/update-shipper-by-order-id/:id/:name', (req, res) => {
    try {
        let id = req.params['id'];
        let name = req.params['name'];
        let check_null_id = validCommon.check_id_null(id);
        let check_null_name = validCommon.check_id_null(name);
        if (check_null_id && check_null_name) {
            orderService.updateOrderShipperName(id, name).then(_res => {
                res.end(JSON.stringify(_res));
            });
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * Method find order by shipper name
 */
router.get('/find-order-by-shipper/:name', (req, res) => {
    try {
        let name = req.params['name'];
        let check_null_name = validCommon.check_id_null(name);
        if (check_null_name) {
            orderService.findOrderByShipperName(name).then(_res => {
                res.end(JSON.stringify(_res));
            });
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;