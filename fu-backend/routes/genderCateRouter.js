const express = require('express');
const router = express.Router();
const genderCateService = require('../repo/genderCateRepo');
const categoryMeg = require('../utils/message/categoryMessage');
const validCommon = require('../utils/common/valid');

/**
 * Method find all category
 * Return list data category
 */
router.get('/:id', (req, res, next) => {
    try {
        let id_category = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_category);
        if (check_null_id) {
            genderCateService.findByCateId(id_category).then(cat => {
                if (cat && cat != null) {
                    res.end(JSON.stringify(cat));
                } else {
                    res.end(categoryMeg.MESS_CAT_OO1);
                }
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method delete category
 */
router.delete('/:id', (req, res) => {
    let id_category = req.params['id'];
    let check_null_id = validCommon.check_id_null(id_category);
    if (check_null_id) {
        productService.findProductByCategoryID(id_category).then(result => {
            if (result == null) {
                categoryService.deleteCategory(id_category).then(_ => {
                    res.end(JSON.stringify(_));
                });
            } else {
                res.end(JSON.stringify(result));
            }
        });
    }
});

module.exports = router;