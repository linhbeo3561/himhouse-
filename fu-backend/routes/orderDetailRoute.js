const express = require('express');
const router = express.Router();
const orderDetailsService = require('../repo/orderDetailRepo');
const productService = require('../repo/productRepo');
const orderMeg = require('../utils/message/orderDetailsMessage');
const validCommon = require('../utils/common/valid');

/*
 * This router use for both order sell and rent detail
 */

/*
 * Add order sell detail
 */
router.post('/', (req, res) => {
    try {
        if (validCommon.check_obj_null(req.body)) {
            let newOder = req.body;
            orderDetailsService.addOrder(newOder).then(order => {
                res.end(JSON.stringify(order))
            });
            // console.log(newOder);
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * Method valid quantity
 */
router.get('/valid-quantity/:id', (req, res) => {
    try {
        let id_prods = req.params['id'];
        if (validCommon.check_id_null(id_prods)) {
            productService.validProds(id_prods).then(result => {
                if(result) {
                    res.end(JSON.stringify(result));
                }
            });
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});

/**
 * 
 */
router.get('/:id', (req, res) => {
    try {
        let id_customer = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_customer);
        if (check_null_id) {
            orderDetailsService.getOrderdetailbyOrderId(id_customer).then(order => {
                res.end(JSON.stringify(order))
            });
        } else {
            res.end(orderMeg.MESS_ORDER_OO1);
        }
    } catch (error) {
        console.log(error)
    }
});


module.exports = router;