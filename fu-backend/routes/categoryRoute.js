const express = require('express');
const router = express.Router();
const categoryService = require('../repo/categoryRepo');
const productService = require('../repo/productRepo');
const genderCateService = require('../repo/genderCateRepo');
const prodTypeService = require('../repo/productTypeRepo');
const categoryMeg = require('../utils/message/categoryMessage');
const validCommon = require('../utils/common/valid');

/**
 * Method find all category
 * Return list data category
 */
router.get('/', (req, res, next) => {
    try {
        categoryService.findAllCategory().then(cat => {
            if (cat && cat != null) {
                res.end(JSON.stringify(cat));
            } else {
                res.end(categoryMeg.MESS_CAT_OO1);
            }
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method find all category
 * Return list data category
 */
router.get('/:id', (req, res, next) => {
    try {
        let id_category = req.params['id'];
        let check_null_id = validCommon.check_id_null(id_category);
        if (check_null_id) {
            categoryService.findCategoryByID(id_category).then(cat => {
                if (cat && cat != null) {
                    res.end(JSON.stringify(cat));
                } else {
                    res.end(categoryMeg.MESS_CAT_OO1);
                }
            });
        }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Method delete category
 */
router.delete('/:id', (req, res) => {
    let id_category = req.params['id'];
    let check_null_id = validCommon.check_id_null(id_category);
    if (check_null_id) {
        productService.findProductByCategoryID(id_category).then(result => {
            if (result == null) {
                categoryService.deleteCategory(id_category).then(_ => {
                    res.end(JSON.stringify(_));
                });
                prodTypeService.deleteTypeByCateID(id_category).then(__ => {
                    res.end(JSON.stringify(__));
                })
            } else {
                res.end(JSON.stringify(result));
            }
        });
    }
});

/**
 * Method update categories
 */
router.post("/", (req, res) => {
    let categories = req.body;
    let check_null_body = validCommon.check_obj_null(categories);
    if (check_null_body) {
        categoryService.updateCategory(categories).then(_ => {
            if (_) {
                console.log(_);
                let jsonBulkCreata = [];
                categories.gender.forEach(element => {
                    let json = {
                        "categories_id": categories.id,
                        "gender_id": element
                    }
                    jsonBulkCreata.push(json);
                    genderCateService.deleteGenderCateById(categories.id).then(result => {
                        if (result) {
                            genderCateService.addnewGenderCate(jsonBulkCreata).then(result => {
                                if (result) {
                                    res.end(JSON.stringify(_));
                                }
                            });
                        }
                    });
                });
            }
        });
    }
});

/**
 * Method update categories
 */
router.post("/add", (req, res) => {
    let categories = req.body;
    let check_null_body = validCommon.check_obj_null(categories);
    if (check_null_body) {
        categoryService.addNewCategory(categories).then(_ => {
            if (_) {
                let jsonBulkCreata = [];
                categories.gender.forEach(element => {
                    let json = {
                        "categories_id": _.id,
                        "gender_id": element
                    }
                    jsonBulkCreata.push(json);
                });
                genderCateService.addnewGenderCate(jsonBulkCreata).then(result => {
                    if (result) {
                        res.end(JSON.stringify(_));
                    }
                });
            }
        });
    }
});

module.exports = router;