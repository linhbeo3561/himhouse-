const express = require('express');
const router = express.Router();
const validCommon = require('../utils/common/valid');
const imageService = require('../repo/imagesRepo');


/**
 * Method find all gender
 */
router.post('/', (req, res) => {
    try {
        let images = req.body;
        let check_null_body = validCommon.check_obj_null(images);
        if (check_null_body) {
            imageService.createImagesThum(images).then(_ => {
                res.end(JSON.stringify(_));
            });
        }
    } catch (error) {
        console.log(error);
    }
});


/**
 * Method find all gender
 */
router.get('/:product_id', (req, res) => {
    try {
        let product_id = req.params['product_id'];
        let check_null_id = validCommon.check_id_null(product_id);
        if (check_null_id) {
            imageService.deleteByProductId(product_id).then(images => {
                res.end(JSON.stringify(images));
            });
        }
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;