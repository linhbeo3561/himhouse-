var express = require('express');
const db = require('../migrate/db');
var router = express.Router();

router.get('/price-interest', (req, res) => {
    try {
        let query = `SELECT
                        CONCAT('Tháng ', TO_CHAR(od."createdAt", 'MM')) as label,
                        SUM (
                            (
<<<<<<< HEAD
                                od .price_per_unit - od .old_price
=======
                                P .price_per_unit - P .old_price
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
                            ) * od.quantity
                        ) AS y
                    FROM
                        order_details od
                    INNER JOIN orders o ON od.order_id = o. ID
                    LEFT JOIN products P ON od.product_id = P . ID
                    WHERE
                        1 = 1 AND o.status = 6 `;
        // find param in URL
        // find param in URL
        let start_date = req.query.start_date;
        let end_date = req.query.end_date;
        if (start_date == "undefined" || start_date == "") {
            start_date = undefined;
        }
        if (end_date == "undefined" || end_date == "") {
            end_date = undefined;
        }
        // Check undefined
        if (start_date == undefined && end_date != undefined) {
            query += ` AND od."createdAt" <= '${end_date}' `;
        } else if (start_date != undefined && end_date == undefined) {
            query += ` AND od."createdAt" >= '${start_date}' `;
        } else if (start_date != undefined && end_date != undefined) {
            query += ` AND od."createdAt" BETWEEN '${start_date}' 
            AND '${end_date}' `;
        }
        query += " GROUP BY 1";
        // Find in DB
        db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        }).then(kpi_rent => {
            console.log(kpi_rent);
            res.end(JSON.stringify(kpi_rent));
        });
    } catch (error) {
        console.log(error);
    }
});
/*
 * Get all kpi rent
 */
router.get('/quantity', (req, res, next) => {
    try {
        let query = `SELECT
                        SUM (od.quantity) AS y,
                        prd. NAME,
                        'exploded: true' AS exploded
                    FROM
                        order_details od
                    INNER JOIN products prd ON product_id = prd. ID
                    INNER JOIN orders o ON od.order_id = o. ID
                    WHERE
                        1 = 1
                    AND o.status = 6`;
        // find param in URL
        // find param in URL
        let start_date = req.query.start_date;
        let end_date = req.query.end_date;
        if (start_date == "undefined" || start_date == "") {
            start_date = undefined;
        }
        if (end_date == "undefined" || end_date == "") {
            end_date = undefined;
        }
        // Check undefined
        if (start_date == undefined && end_date != undefined) {
            query += ` AND od."createdAt" <= '${end_date}' `;
        } else if (start_date != undefined && end_date == undefined) {
            query += ` AND od."createdAt" >= '${start_date}' `;
        } else if (start_date != undefined && end_date != undefined) {
            query += ` AND od."createdAt" BETWEEN '${start_date}' 
            AND '${end_date}' `;
        }
        query += " GROUP BY od.product_id, prd.NAME";
        // Find in DB
        db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        }).then(kpi_rent => {
            res.end(JSON.stringify(kpi_rent));
        });
    } catch (error) {
        console.log(error);
    }
});

/*
 * Get all kpi rent
 */
router.get('/price', (req, res, next) => {
    try {
        let query = `SELECT
                        od. NAME,
                        SUM (od.total_money) AS y,
                        'exploded: true' AS exploded
                    FROM
                        orders od
                    WHERE
                        1 = 1
                    AND od.status = 6`;
        // find param in URL
        // find param in URL
        let start_date = req.query.start_date;
        let end_date = req.query.end_date;
        if (start_date == "undefined" || start_date == "") {
            start_date = undefined;
        }
        if (end_date == "undefined" || end_date == "") {
            end_date = undefined;
        }
        // Check undefined
        if (start_date == undefined && end_date != undefined) {
            query += ` AND od."createdAt" <= '${end_date}' `;
        } else if (start_date != undefined && end_date == undefined) {
            query += ` AND od."createdAt" >= '${start_date}' `;
        } else if (start_date != undefined && end_date != undefined) {
            query += ` AND od."createdAt" BETWEEN '${start_date}' 
            AND '${end_date}' `;
        }
        query += " GROUP BY od.NAME";
        // Find in DB
        db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        }).then(kpi_rent => {
            res.end(JSON.stringify(kpi_rent));
        });
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;