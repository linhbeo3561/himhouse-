const express = require('express');
const multer = require('multer');
const router = express.Router();
const uuidv1 = require('uuid/v1');

const DIR = './public/images/home';

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, DIR);
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})

var upload = multer({
  storage: storage
}).single('photo');

router.get('/', (req, res, next) => {
  res.end('file catcher example');
});

router.post('/', (req, res, next) => {
  var path = '';
  upload(req, res, (err) => {

    if (err) {
      console.log(err);
      return res.status(422).send("an Error occured")
    }
    path = req.file.path;
    return res.send("Upload Completed for " + path);
  });
})

module.exports = router;