<<<<<<< HEAD
require("dotenv").config();
const Sequelize = require('sequelize');
const db = {}
/**
 * Config DB connect to postgres
 */
const connection = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: 'postgres',
    dialectOptions: {
        multipleStatements: true
    },
    pool: {
        max: 20,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

//----------------------------------------------------------------------------------------
db.Sequelize = Sequelize;
db.connection = connection;

// Tạo bảng khi run code
//----------------------------------------------------------------------------------------
db.db_acc = require('./table/db_account')(connection, Sequelize);
db.db_role = require('./table/db_role')(connection, Sequelize);
db.db_category = require('./table/db_category')(connection, Sequelize);
db.db_order = require('./table/db_orders')(connection, Sequelize);
db.db_order_details = require('./table/db_order_details')(connection, Sequelize);
db.db_products = require('./table/db_products')(connection, Sequelize);
db.db_product_type = require('./table/db_product_type')(connection, Sequelize);
db.db_product_optional = require('./table/db_product_optional')(connection, Sequelize);
db.db_user = require('./table/db_users')(connection, Sequelize);
db.db_status = require('./table/db_status')(connection, Sequelize);
db.db_gender = require('./table/db_gender')(connection, Sequelize);
db.cate_product = require('./table/db_cate_product')(connection, Sequelize);
db.gender_cate = require('./table/db_gender_cate')(connection, Sequelize);
db.db_images = require('./table/db_images')(connection, Sequelize);

//----------------------------------------------------------------------------------------
// Tao quan he
db.db_category.hasMany(db.db_product_type, { foreignKey: 'categories_id' });
db.db_acc.hasMany(db.db_user, { foreignKey: 'account_id' });
db.db_order.hasMany(db.db_order_details, { foreignKey: 'order_id' });
db.db_gender.hasMany(db.db_category, { foreignKey: 'gender_id' });
db.db_products.hasMany(db.db_images, { foreignKey: 'product_id' });

connection.authenticate()
    .then(() => {
        console.log('Connection successfully.');
    })
    .catch(err => {
        console.error('ERROR:', err);
    });

=======
require("dotenv").config();
const Sequelize = require('sequelize');
const db = {}
/**
 * Config DB connect to postgres
 */
const connection = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: 'postgres',
    dialectOptions: {
        multipleStatements: true
    },
    pool: {
        max: 20,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

//----------------------------------------------------------------------------------------
db.Sequelize = Sequelize;
db.connection = connection;

// Tạo bảng khi run code
//----------------------------------------------------------------------------------------
db.db_acc = require('./table/db_account')(connection, Sequelize);
db.db_role = require('./table/db_role')(connection, Sequelize);
db.db_category = require('./table/db_category')(connection, Sequelize);
db.db_order = require('./table/db_orders')(connection, Sequelize);
db.db_order_details = require('./table/db_order_details')(connection, Sequelize);
db.db_products = require('./table/db_products')(connection, Sequelize);
db.db_product_type = require('./table/db_product_type')(connection, Sequelize);
db.db_product_optional = require('./table/db_product_optional')(connection, Sequelize);
db.db_user = require('./table/db_users')(connection, Sequelize);
db.db_status = require('./table/db_status')(connection, Sequelize);
db.db_gender = require('./table/db_gender')(connection, Sequelize);
db.cate_product = require('./table/db_cate_product')(connection, Sequelize);
db.gender_cate = require('./table/db_gender_cate')(connection, Sequelize);
db.db_images = require('./table/db_images')(connection, Sequelize);

//----------------------------------------------------------------------------------------
// Tao quan he
db.db_category.hasMany(db.db_product_type, { foreignKey: 'categories_id' });
db.db_acc.hasMany(db.db_user, { foreignKey: 'account_id' });
db.db_order.hasMany(db.db_order_details, { foreignKey: 'order_id' });
db.db_gender.hasMany(db.db_category, { foreignKey: 'gender_id' });
db.db_products.hasMany(db.db_images, { foreignKey: 'product_id' });

connection.authenticate()
    .then(() => {
        console.log('Connection successfully.');
    })
    .catch(err => {
        console.error('ERROR:', err);
    });

>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
module.exports = db;