module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Order = connection_db.define('order', {
        order_id: {
            type: Sequelize.STRING(),
        },
        cart_id: {
            type: Sequelize.STRING(11),
        },
        customer_id: {
            type: Sequelize.STRING(11),
        },
        name: {
            type: Sequelize.STRING()
        },
        address: {
            type: Sequelize.STRING()
        },
        email: {
            type: Sequelize.STRING()
        },
        phone_number: {
            type: Sequelize.STRING(11)
        },
        shipping: {
            type: Sequelize.STRING()
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
        total_money: {
            type: Sequelize.DOUBLE,
            defaultValue: 0
        },
        note: {
            type: Sequelize.STRING()
        },
        account_id: {
            type: Sequelize.STRING()
        },
        money_shipping: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
    });

    Order.sync();

    return Order;
};