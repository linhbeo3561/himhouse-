module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Category = connection_db.define('categories', {
        name: {
            type: Sequelize.STRING
        },
        gender_id: {
            type: Sequelize.INTEGER
        },
        description: {
            type: Sequelize.STRING(500)
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    Category.sync();

    return Category;
};