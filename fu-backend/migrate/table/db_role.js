module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Role = connection_db.define('role', {
        role_name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        }
    });

    Role.sync();

    return Role;
};