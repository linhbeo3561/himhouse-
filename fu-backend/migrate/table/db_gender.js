module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Gender = connection_db.define('gender', {
        gender: {
            type: Sequelize.STRING
        },
        image: {
            type: Sequelize.STRING
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    Gender.sync();

    return Gender;
};