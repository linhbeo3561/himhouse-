module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Status = connection_db.define('status', {
        status_name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        }
    });

    Status.sync();

    return Status;
};