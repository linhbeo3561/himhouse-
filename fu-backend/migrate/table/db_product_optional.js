module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table ProductOptional
     */
    const ProductOptional = connection_db.define('product_optional', {
        product_id:{
            type: Sequelize.STRING(11)
        },
        option_name:{
            type: Sequelize.STRING
        },
        inventory: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
        price_per_unit: {
            type: Sequelize.DOUBLE,
            allowNull: true
        },
        is_sale: {
            type: Sequelize.DOUBLE,
            defaultValue : 0
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    ProductOptional.sync();

    return ProductOptional;
};