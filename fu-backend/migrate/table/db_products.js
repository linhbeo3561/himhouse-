module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Product
     */
    const Product = connection_db.define('product', {
        cat_id: {
            type: Sequelize.STRING(11)
        },
        name: {
            type: Sequelize.STRING
        },
        inventory: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
        unit: {
            type: Sequelize.STRING
        },
        price_per_unit: {
            type: Sequelize.DOUBLE,
            allowNull: true
        },
        old_price: {
            type: Sequelize.DOUBLE,
            allowNull: true
        },
        image: {
            type: Sequelize.STRING
        },
        type_id: {
            type: Sequelize.INTEGER
        },
        description: {
            type: Sequelize.STRING(500)
        },
        size: {
            type: Sequelize.STRING(500)
        },
        color: {
            type: Sequelize.STRING(500)
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    Product.sync();

    return Product;
};