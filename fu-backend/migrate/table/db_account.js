module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Account = connection_db.define('account', {
        username: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        role_id: {
            type: Sequelize.INTEGER
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    Account.sync();

    return Account;
};