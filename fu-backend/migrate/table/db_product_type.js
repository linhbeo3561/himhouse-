module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Product Type
     */
    const ProductType = connection_db.define('product_type', {
        categories_id: {
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
    });

    ProductType.sync();

    return ProductType;
};