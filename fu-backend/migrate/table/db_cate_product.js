module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Cate_product = connection_db.define('cate_product', {
        categories_id: {
            type: Sequelize.INTEGER
        },
        product_id: {
            type: Sequelize.INTEGER
        },
        gender_id: {
            type: Sequelize.INTEGER
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    Cate_product.sync();

    return Cate_product;
};