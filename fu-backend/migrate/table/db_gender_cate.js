module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Gender_cate = connection_db.define('gender_cate', {
        categories_id: {
            type: Sequelize.INTEGER
        },
        gender_id: {
            type: Sequelize.INTEGER
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    Gender_cate.sync();

    return Gender_cate;
};