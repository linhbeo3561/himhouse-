module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Users
     */
    const Users = connection_db.define('user', {
        account_id: {
            type: Sequelize.INTEGER
        },
        fullname: {
            type: Sequelize.STRING
        },
        age: {
            type: Sequelize.INTEGER
        },
        address: {
            type: Sequelize.STRING(200)
        },
        phone_number: {
            type: Sequelize.STRING(13)
        },
        email: {
            type: Sequelize.STRING(200)
        },
        gennder: {
            type: Sequelize.BOOLEAN
        },
        is_delete: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });

    Users.sync();

    return Users;
};