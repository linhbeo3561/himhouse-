module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const Images = connection_db.define('images', {
        product_id: {
            type: Sequelize.INTEGER
        },
        images: {
            type: Sequelize.STRING
        }
    });

    Images.sync();

    return Images;
};