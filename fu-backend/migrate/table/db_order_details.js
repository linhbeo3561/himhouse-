module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Order
     */
    const OrderDetail = connection_db.define('order_detail', {
        order_id: {
            type: Sequelize.INTEGER
        },
        product_id: {
            type: Sequelize.INTEGER
        },
        color: {
            type: Sequelize.STRING()
        },
        price_per_unit: {
            type: Sequelize.DOUBLE,
            allowNull: true
        },
        old_price: {
            type: Sequelize.DOUBLE,
            allowNull: true
        },
        size: {
            type: Sequelize.STRING()
        },
        quantity: {
            type: Sequelize.INTEGER
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        }
    });

    OrderDetail.sync();

    return OrderDetail;
};