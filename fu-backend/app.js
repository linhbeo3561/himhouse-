var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var accountController = require('./routes/accountRoute');
var productController = require('./routes/productRoute');
var categoryController = require('./routes/categoryRoute');
var productTypeController = require('./routes/productTypeRoute');
var orderController = require('./routes/orderRoute');
var orderDetailsController = require('./routes/orderDetailRoute');
var kpiController = require('./routes/kpiRouter');
var uploadFile = require('./routes/uploadFile');
var gender = require('./routes/genderRoute');
var genderCate = require('./routes/genderCateRouter');
var imagesController = require('./routes/imagesRoute');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/v1/api/account', accountController);
app.use('/v1/api/product', productController);
app.use('/v1/api/category', categoryController);
app.use('/v1/api/type', productTypeController);
app.use('/v1/api/order', orderController);
app.use('/v1/api/order-details', orderDetailsController);
app.use('/v1/api/kpi', kpiController);
app.use('/v1/api/uploadfile', uploadFile);
app.use('/v1/api/gender', gender);
app.use('/v1/api/gender-cate', genderCate);
app.use('/v1/api/images', imagesController);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

module.exports = app;
