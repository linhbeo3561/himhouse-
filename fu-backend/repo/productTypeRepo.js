const db = require('../migrate/db');

module.exports = {

    /**
     * Method create new product type
     */
    insertProductType: (productType) => {
        return db.db_product_type.bulkCreate(productType);
    },
    /**
     * Method delete type by id
     */
    deleteTypeById: (id) => {
        return db.db_product_type.destroy({
            where: {
                id: id
            }
        });
    },
    /** 
     * Method delete by categories id 
    */
    deleteTypeByCateID: (id) => {
        return db.db_product_type.destroy({
            where: {
                categories_id: id
            }
        });
    },
    /**
     * Method find Product Type by id
     */
    findProductTypeByID: (id = null) => {
        return db.db_product_type.findOne({
            where: {
                id: id
            }
        });
    },


    /**
     * Method find Product Type by id
     */
    findProductTypeByCategoryId: (id = null) => {
        return db.db_product_type.findAll({
            where: {
                categories_id: id
            }
        });
    },

    /**
     * Method check exit prod type by name
     */
    checkExitProdName: (name = null) => {
        return db.db_product_type.findAll({
            where: {
                name: name
            }
        });
    },
    /**
     * Method find all Product Type
     */
    findAllProductType: (id = null) => {
        return db.db_product_type.findAll({
            order: [
                ['name', 'ASC'],
            ],
        });
    }
}