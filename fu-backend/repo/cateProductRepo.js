const db = require('../migrate/db');

module.exports = {

    /**
     * Method add new GenderCate
     */
    addNewCatePro: (catePro) => {
        return db.cate_product.bulkCreate(catePro);
    },

    /**
     * Method delete gender cate by id
     */
    deleteCateProById: (cateId) => {
        return db.cate_product.destroy({
            where: {
                categories_id: cateId
            }
        });
    },

    /**
     * Method delete gender cate by id
     */
    deleteCateProByPId: (pId) => {
        return db.cate_product.destroy({
            where: {
                product_id: pId
            }
        });
    },

    /**
     * Method find by cate id
     */
    findByCateId: (cateId) => {
        return db.cate_product.findAll({
            where: {
                is_delete: false,
                categories_id: cateId
            }
        });
    }
}