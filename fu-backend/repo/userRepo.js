const db = require('../migrate/db');

module.exports = {
    findAllUser: () => {
        return db.db_user.findAll({
            where: {
                is_delete: false
            }
        });
    },
    findUserByID: (user_id) => {
        return db.db_user.findOne({
            where: {
                account_id: user_id
            }
        });
    },
    findUserByAccount: (account_id) => {
        return db.db_user.findOne({
            where: {
                account_id: account_id
            }
        });
    },
    createUser: (user_info) => {
        return db.db_user.create({
            account_id: user_info.account_id,
            fullname: user_info.fullname,
            age: user_info.age,
            address: user_info.address,
            phone_number: user_info.phone_number,
            email: user_info.email,
            gennder: user_info.gennder
        });
    },
    /**
     * Method edit user
     */
    editUser: (user) => {
        return db.db_user.update(user, {
            where: {
                id: user.id
            }
        })
    },
    /**
     * Method delete user
     */
    deleteUser: (id_user) => {
        return db.db_user.update({
            is_delete: true
        }, {
                where: {
                    id: id_user
                }
            })
    }
}