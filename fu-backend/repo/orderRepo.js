const db = require('../migrate/db');
const uuidv1 = require('uuid/v1');

module.exports = {
    /**
     * Add new order details
     */
    createOrder: (order) => {
        return db.db_order.create({
            order_id: uuidv1(),
            customer_id: order.customer_id,
            name: order.name,
            address: order.address,
            email: order.email,
            phone_number: order.phone_number,
            shipping: order.shipping,
            total_money: order.total_money,
            money_shipping: order.money_shipping
        });
    },

    getOrderInfo: () => {
        return db.db_order.findAll({
            order: [
                ['createdAt', 'DESC'],
            ],
        });
    },

     /**
     * Method delete gender cate by id
     */
    deleteOrderById: (id) => {
        return db.db_order.destroy({
            where: {
                id: id
            }
        });
    },
    /**
    * Get info order by ID
    */
    getOrderByStatus: (status) => {
        return db.db_order.findAll({
            where: {
                status: status
            }
        });
    },

    /**
     * Get info order by ID
     */
    getOrderInfoById: (id) => {
        return db.db_order.findAll({
            where: {
                id: id
            }
        });
    },

    /**
     * Get info order by Customer ID
     */
    getCustomerOrderHistory: (customer_id) => {
        return db.db_order.findAll({
            where: {
                customer_id: customer_id
            },
            include: [db.db_order_details],
            order: [
                ['createdAt', 'DESC'],
            ],
        });
    },
    /**
     * Method update status 
     */
    updateOrderStatus: (id, status) => {
        return db.db_order.update({
            status: status
        }, {
                where: {
                    id: id
                }
            });
    },
    /**
     * Method update status 
     */
    updateOrderShipperName: (id, name) => {
        return db.db_order.update({
            account_id: name
        }, {
                where: {
                    id: id
                }
            });
    },
    /**
     * Method update status 
     */
    findOrderByShipperName: (name) => {
        return db.db_order.findAll({
            where: {
                account_id: name
            },order: [
                ['createdAt', 'DESC'],
            ],
        });
    }
};