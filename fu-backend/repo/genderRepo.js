const db = require('../migrate/db');

module.exports = {
    /**
     * Find all gender
     */
    findAllGender: () => {
        return db.db_gender.findAll(
            {
                where: {
                    is_delete: false
                },
                include: [db.db_category],
                // include: [db.db_gender],
                order: [[db.db_category, 'name', 'DESC']]
                // [db.db_gender, 'id', 'DESC']]
            }
        );
    },


    /**
     * Method find category by genderid
     */
    findAllCategoriesByGenderID: (gender_id) => {
        let query = `SELECT
                        c.id, c.name
                        FROM
                            categories C
                        LEFT JOIN gender_cates gc ON C . ID = gc.categories_id
                        WHERE
                            gc.gender_id = ${gender_id}
                            AND c.is_delete = false
                        ORDER BY
                            C ."name" DESC;`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    }
}