const db = require('../migrate/db');

module.exports = {
    /**
     * Find all gender
     */
    createImagesThum: (images) => {
        return db.db_images.create(images);
    },

    deleteByProductId: (product_id) => {
        return db.db_images.destroy({
            where: {
                product_id: product_id
            }
        });
    }
}