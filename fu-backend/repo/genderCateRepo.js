const db = require('../migrate/db');

module.exports = {

    /**
     * Method add new GenderCate
     */
    addnewGenderCate: (genderCate) => {
        return db.gender_cate.bulkCreate(genderCate);
    },

    /**
     * Method delete gender cate by id
     */
    deleteGenderCateById: (cateId) => {
        return db.gender_cate.destroy({
            where: {
                categories_id: cateId
            }
        });
    },

    /**
     * Method find by cate id
     */
    findByCateId: (cateId) => {
        return db.gender_cate.findAll({
            where: {
                is_delete: false,
                categories_id: cateId
            }
        });
    },
     /**
     * Method find by cate id
     */
    findGenderByPId: (pid) => {
        return db.cate_product.findAll({
            where: {
                is_delete: false,
                product_id: pid
            }
        });
    }
}