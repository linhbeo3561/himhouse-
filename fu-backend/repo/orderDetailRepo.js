const db = require('../migrate/db');

module.exports = {
    /**
     * Method add order rent to DB
     */
    addOrder: (order) => {
        return db.db_order_details.bulkCreate(order);
    },

    /**
     * Method find by id
     */
    getOrderdetailbyOrderId: (order_id) => {
        return db.db_order_details.findAll({
            where: {
                order_id: order_id
            }
        });
    },


}