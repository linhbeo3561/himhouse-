const db = require('../migrate/db');
const md5 = require('md5')

module.exports = {

    /**
     * Method create new account
     */
    createAccount: (new_account) => {
        let password_MD5 = md5(new_account.password);
        // Create tạo mới 1 object (SEQ support)
        return db.db_acc.create({
            // id: new_account.id,
            username: new_account.username,
            password: password_MD5,
            role_id: new_account.role_id
        });
    },
    /**
     * Method login ussing account
     */
    accountLogin: (account) => {
        // select * from acc where 
        let password_MD5 = md5(account.password);
        return db.db_acc.findOne({
            where: {
                username: account.username,
                password: password_MD5,
                // is_delete true
                is_delete: false
            },
            // INNER JOIN
            include: [db.db_user]
        });
    },

    /**
     * Method find account by id
     */
    findAccountByID: (account_id) => {
        return db.db_acc.findOne({
            where: {
                id: account_id,
                // is_delete: false
            },
            include: [db.db_user]
        });
    },

    /**
     * Method find account by username
     */
    findAccountByUsername: (username) => {
        return db.db_acc.findOne({
            where: {
                username: username
            }
        });
    },
    /**
     * Method change password
     */

    changePassword: (account) => {
        return db.db_acc.update({
            password: md5(account.password)
        }, {
                where: {
                    id: account.id
                }
            });
    },



    /**
     * Method delete account
     */
    deleteAccount: (id_account, active) => {
        return db.db_acc.update({
            is_delete: active
        }, {
                where: {
                    id: id_account
                }
            });
    },
    /**
     * Method find all acc
     */

    findAllByRoleId: (role_id) => {
        return db.db_acc.findAll({
            include: [db.db_user],
            order: [
                ['createdAt', 'ASC'],
            ],
        });
    },


    /**
     * Method find all acc
     */
    findAllByUsername: (username) => {
        return db.db_acc.findAll({
            where: {
                username: username
            }
        });
    },
    /**
     * Method find all acc
     */
    findAllByRole: (role_id) => {
        return db.db_acc.findAll({
            where: {
                // is_delete: false,
                role_id: role_id
            },
            include: [db.db_user]
        });
    },

    /**
     * Method find all acc
     */
    findAllByPassword: (user) => {
        return db.db_acc.findAll({
            where: {
                is_delete: false,
                id: user.id,
                password: md5(user.password)
            }
        });
    },
    /**
     * Update account by id
     */

    changeRole: (account_id, role_id) => {
        return db.db_acc.update({
            role_id: role_id
        }, {
                where: {
                    id: account_id
                }
            });
    }
    
}