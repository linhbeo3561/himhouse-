const db = require('../migrate/db');

module.exports = {

    /**
     * Method delete cate
     */
    deleteCategory: (categoryId) => {
        return db.db_category.update({
            is_delete: true
        }, {
                where: {
                    id: categoryId
                }
            });
    },


    /**
     * Method find category by id
     */
    findCategoryByID: (id = null) => {
        return db.db_category.findOne({
            where: {
                id: id
            }
        });
    },

    /**
     * Method find all category
     */
    findAllCategory: (id = null) => {
        return db.db_category.findAll({
            where: {
                is_delete: false
            },
            include: [db.db_product_type],
            order: [
                ['createdAt', 'desc'],
            ],
        });
    },

    /**
     * Method add new category
     */
    addNewCategory: (new_category) => {
        return db.db_category.create(new_category);
    },

    /**
     * Method update category
     */
    updateCategory: (category) => {
        return db.db_category.update(category, {
            where: {
                id: category.id
            }
        });
    },
}