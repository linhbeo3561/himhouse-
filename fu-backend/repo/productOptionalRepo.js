const db = require('../migrate/db');
const Sequelize = require('sequelize')
const Op = Sequelize.Op
module.exports = {

    /**
     * Method find all product by is_delete default is false
     */
    find_Product_Option_By_Product_ID: (productID) => {
        return db.db_product_optional.findAll({
            where: {
                product_id: productID,
                is_delete: false
            }
        });
    },

    /**
     * Method add option for froduct
     */
    add_new_Product_Option: (product_option) => {
        return db.db_products.create({
            // id: product_option.id,
            product_id: product_option.product_id,
            option_name: product_option.option_name,
            inventory: product_option.inventory,
            price_per_unit: product_option.price_per_unit,
            rent_per_month:product_option.rent_per_month,
        });
    },
}