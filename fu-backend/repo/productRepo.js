const db = require('../migrate/db');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = {

    /**
     * Method find all product by is_delete default is false
     */
    findAllProduct: (is_delete = false) => {
        return db.db_products.findAll({
            where: {
                is_delete: false
            },
            include: [db.db_images],
            order: [
<<<<<<< HEAD
                ['createdAt', 'DESC'],
=======
                ['createdAt', 'ASC'],
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
            ],
        });
    },

    /**
     * Method valid 
     */
    validProds: (ids) => {
        let query = `SELECT id, inventory FROM products WHERE id IN (${ids})`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },
    /**
     * Method find all product by is_delete default is false
     */
    findAllProductByTop4New: (is_delete = false) => {
        return db.db_products.findAll({
            where: {
                is_delete: is_delete,
                id: {
                    [Op.between]: [1, 4]
                }
            }, order: [
                ['updatedAt', 'DESC'],
            ],
        });
    },

    /**
     * Method find product by id
     */
    findProductByID: (id = null) => {
        return db.db_products.findOne({
            where: {
                id: id
            },
            include: [db.db_images],
        });
    },

    /**
     * Method find product by id
     */
    findProductByCategory: (id = null) => {
        return db.db_products.findAll({
            where: {
                cat_id: id
            }
        });
    },

    /**
     * Method find product by id
     */
    findProductByCategoryID: (cat_id = null) => {
        return db.db_products.findOne({
            where: {
                cat_id: cat_id,
                is_delete: false
            }
        });
    },

    /**
     * Method find product by id
     */
    findProductByTypeID: (type_id = null) => {
        return db.db_products.findAll({
            where: {
                type_id: type_id,
                is_delete: false
            }
        });
    },

    /**
     * Method find product by id
     */
    findCateID: (gender_id = null) => {
        let query = `SELECT
                        C . ID
                    FROM
                        categories C
                    LEFT JOIN gender_cates gc ON C . ID = gc.categories_id
                    WHERE
                        gc.gender_id = ${gender_id}`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },


    /**
     * Method find product by id
     */
    findAllListPid: (pids = null) => {
        let query = `SELECT * FROM products WHERE id IN (${pids})`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * Method find product by id
     */
    findProductByCateId: (cate_id = null, gender_id = null) => {
        let query = `SELECT DISTINCT
                        p.*
                    FROM
                        products P
                    LEFT JOIN cate_products cp ON P . ID = cp.product_id
                    WHERE
                        cp.categories_id IN ( ${cate_id} ) AND p.is_delete = FALSE `;
        if (!(gender_id == null || gender_id == '0')) {
            query += `AND cp.gender_id = ${gender_id}`;
        }
        query += ` ORDER BY p."createdAt" DESC`;
        console.log(query);
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * Method find product by category
     */
    findProductByCategory: (category_id = null) => {
        return db.db_products.findAll({
            where: {
                cat_id: category_id,
                is_delete: false
            }
        });
    },

    /**
     * Method add new product
     */
    addnewProduct: (product) => {
        return db.db_products.create(product);
    },

    /**
     * Method delete product
     */
    deleteProduct: (product_ID) => {
        return db.db_products.update({
            is_delete: true
        }, {
                where: {
                    id: product_ID
                }
            });
    },

    /**
     * Method update product
     */
    updateProduct: (product) => {
        return db.db_products.update(product, {
            where: {
                id: product.id
            }
        });
    },

    /**
     * Method filter product
     */
    filterProduct: (product) => {
        return db.db_products.findAll({
            where: {
                name: {
                    [Op.iLike]: '%' + product.name + '%'
                },
                is_delete: false
            }
        });
    },

    /**
     * Method filter product
     */
    findProductBySize: (size) => {
        return db.db_products.findAll({
            where: {
                is_delete: false,
                size: {
                    [Op.iLike]: '%' + size + '%'
                }
            }
        });
    },
    /**
     * Method update inventory
     */
    updateInventory: (id, quantity) => {
        return db.db_products.update({
            inventory: quantity
        }, {
                where: {
                    id: id
                }
            });
    }

}