/*
Navicat PGSQL Data Transfer

Source Server         : fu
Source Server Version : 90614
Source Host           : localhost:5432
Source Database       : fu_gruation
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90614
File Encoding         : 65001

Date: 2019-08-21 13:05:13
*/


-- ----------------------------
-- Sequence structure for accounts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "accounts_id_seq";
CREATE SEQUENCE "accounts_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 10
 CACHE 1;
SELECT setval('"public"."accounts_id_seq"', 10, true);

-- ----------------------------
-- Sequence structure for accounts_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "accounts_id_seq1";
CREATE SEQUENCE "accounts_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 12
 CACHE 1;
SELECT setval('"public"."accounts_id_seq1"', 12, true);

-- ----------------------------
-- Sequence structure for cate_products_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "cate_products_id_seq";
CREATE SEQUENCE "cate_products_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 105
 CACHE 1;
SELECT setval('"public"."cate_products_id_seq"', 105, true);

-- ----------------------------
-- Sequence structure for categories_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "categories_id_seq";
CREATE SEQUENCE "categories_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;
SELECT setval('"public"."categories_id_seq"', 7, true);

-- ----------------------------
-- Sequence structure for categories_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "categories_id_seq1";
CREATE SEQUENCE "categories_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 20
 CACHE 1;
SELECT setval('"public"."categories_id_seq1"', 20, true);

-- ----------------------------
-- Sequence structure for gender_cates_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "gender_cates_id_seq";
CREATE SEQUENCE "gender_cates_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 12
 CACHE 1;
SELECT setval('"public"."gender_cates_id_seq"', 12, true);

-- ----------------------------
-- Sequence structure for genders_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "genders_id_seq";
CREATE SEQUENCE "genders_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."genders_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for images_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "images_id_seq";
CREATE SEQUENCE "images_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 79
 CACHE 1;
SELECT setval('"public"."images_id_seq"', 79, true);

-- ----------------------------
-- Sequence structure for order_details_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "order_details_id_seq";
CREATE SEQUENCE "order_details_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 10
 CACHE 1;
SELECT setval('"public"."order_details_id_seq"', 10, true);

-- ----------------------------
-- Sequence structure for order_details_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "order_details_id_seq1";
CREATE SEQUENCE "order_details_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 17
 CACHE 1;
SELECT setval('"public"."order_details_id_seq1"', 17, true);

-- ----------------------------
-- Sequence structure for orders_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "orders_id_seq";
CREATE SEQUENCE "orders_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 12
 CACHE 1;
SELECT setval('"public"."orders_id_seq"', 12, true);

-- ----------------------------
-- Sequence structure for orders_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "orders_id_seq1";
CREATE SEQUENCE "orders_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 13
 CACHE 1;
SELECT setval('"public"."orders_id_seq1"', 13, true);

-- ----------------------------
-- Sequence structure for product_optionals_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "product_optionals_id_seq";
CREATE SEQUENCE "product_optionals_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_optionals_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "product_optionals_id_seq1";
CREATE SEQUENCE "product_optionals_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_types_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "product_types_id_seq";
CREATE SEQUENCE "product_types_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_types_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "product_types_id_seq1";
CREATE SEQUENCE "product_types_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 11
 CACHE 1;
SELECT setval('"public"."product_types_id_seq1"', 11, true);

-- ----------------------------
-- Sequence structure for products_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "products_id_seq";
CREATE SEQUENCE "products_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."products_id_seq"', 8, true);

-- ----------------------------
-- Sequence structure for products_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "products_id_seq1";
CREATE SEQUENCE "products_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 62
 CACHE 1;
SELECT setval('"public"."products_id_seq1"', 62, true);

-- ----------------------------
-- Sequence structure for roles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "roles_id_seq";
CREATE SEQUENCE "roles_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for statuses_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "statuses_id_seq";
CREATE SEQUENCE "statuses_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "users_id_seq";
CREATE SEQUENCE "users_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 9
 CACHE 1;
SELECT setval('"public"."users_id_seq"', 9, true);

-- ----------------------------
-- Sequence structure for users_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "users_id_seq1";
CREATE SEQUENCE "users_id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 12
 CACHE 1;
SELECT setval('"public"."users_id_seq1"', 12, true);

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS "accounts";
CREATE TABLE "accounts" (
"id" int4 DEFAULT nextval('accounts_id_seq1'::regclass) NOT NULL,
"username" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"role_id" int4,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of accounts
-- ----------------------------
BEGIN;
INSERT INTO "accounts" VALUES ('1', 'manager', '21232f297a57a5a743894a0e4a801fc3', '2', 'f', '2019-08-06 21:45:44.713+07', '2019-08-06 21:45:44.713+07');
INSERT INTO "accounts" VALUES ('2', 'hoang', 'f82e62d7c3ea69cc12b5cdb8d621dab6', '0', 'f', '2019-08-06 21:49:01.904+07', '2019-08-19 18:37:58.388+07');
INSERT INTO "accounts" VALUES ('3', 'admin', '21232f297a57a5a743894a0e4a801fc3', '3', 'f', '2019-08-07 22:33:08.244+07', '2019-08-07 22:33:08.244+07');
INSERT INTO "accounts" VALUES ('4', 'shipper', '21232f297a57a5a743894a0e4a801fc3', '-1', 'f', '2019-08-07 22:33:42.265+07', '2019-08-07 22:34:44.842+07');
INSERT INTO "accounts" VALUES ('9', 'staff', '21232f297a57a5a743894a0e4a801fc3', '1', 'f', '2019-08-14 11:21:05.204+07', '2019-08-14 17:58:50.256+07');
INSERT INTO "accounts" VALUES ('10', 'son', '21232f297a57a5a743894a0e4a801fc3', '-1', 'f', '2019-08-14 11:35:18.957+07', '2019-08-14 18:20:25.842+07');
INSERT INTO "accounts" VALUES ('11', 'hoanghoang', '21232f297a57a5a743894a0e4a801fc3', '0', 'f', '2019-08-18 20:36:20.291+07', '2019-08-19 18:37:34.788+07');
INSERT INTO "accounts" VALUES ('12', 'hoanghoang', '21232f297a57a5a743894a0e4a801fc3', '0', 'f', '2019-08-18 20:36:24.951+07', '2019-08-19 18:37:12.79+07');
COMMIT;

-- ----------------------------
-- Table structure for cate_products
-- ----------------------------
DROP TABLE IF EXISTS "cate_products";
CREATE TABLE "cate_products" (
"id" int4 DEFAULT nextval('cate_products_id_seq'::regclass) NOT NULL,
"categories_id" int4,
"product_id" int4,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL,
"gender_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cate_products
-- ----------------------------
BEGIN;
INSERT INTO "cate_products" VALUES ('75', '1', '51', 'f', '2019-08-17 23:33:33.813+07', '2019-08-17 23:33:33.813+07', '1');
INSERT INTO "cate_products" VALUES ('76', '1', '51', 'f', '2019-08-17 23:33:33.813+07', '2019-08-17 23:33:33.813+07', '3');
INSERT INTO "cate_products" VALUES ('77', '1', '51', 'f', '2019-08-17 23:33:33.813+07', '2019-08-17 23:33:33.813+07', '2');
INSERT INTO "cate_products" VALUES ('78', '4', '52', 'f', '2019-08-18 04:05:17.806+07', '2019-08-18 04:05:17.806+07', '1');
INSERT INTO "cate_products" VALUES ('79', '4', '52', 'f', '2019-08-18 04:05:17.806+07', '2019-08-18 04:05:17.806+07', '2');
INSERT INTO "cate_products" VALUES ('80', '4', '52', 'f', '2019-08-18 04:05:17.806+07', '2019-08-18 04:05:17.806+07', '3');
INSERT INTO "cate_products" VALUES ('81', '1', '53', 'f', '2019-08-18 04:11:28.105+07', '2019-08-18 04:11:28.105+07', '2');
INSERT INTO "cate_products" VALUES ('82', '1', '53', 'f', '2019-08-18 04:11:28.105+07', '2019-08-18 04:11:28.105+07', '1');
INSERT INTO "cate_products" VALUES ('83', '1', '53', 'f', '2019-08-18 04:11:28.105+07', '2019-08-18 04:11:28.105+07', '3');
INSERT INTO "cate_products" VALUES ('84', '1', '54', 'f', '2019-08-18 04:15:17.153+07', '2019-08-18 04:15:17.153+07', '1');
INSERT INTO "cate_products" VALUES ('85', '1', '54', 'f', '2019-08-18 04:15:17.153+07', '2019-08-18 04:15:17.153+07', '2');
INSERT INTO "cate_products" VALUES ('86', '1', '54', 'f', '2019-08-18 04:15:17.153+07', '2019-08-18 04:15:17.153+07', '3');
INSERT INTO "cate_products" VALUES ('87', '3', '55', 'f', '2019-08-18 04:17:36.243+07', '2019-08-18 04:17:36.243+07', '1');
INSERT INTO "cate_products" VALUES ('88', '3', '56', 'f', '2019-08-18 04:19:50.396+07', '2019-08-18 04:19:50.396+07', '1');
INSERT INTO "cate_products" VALUES ('89', '1', '57', 'f', '2019-08-18 04:22:26.936+07', '2019-08-18 04:22:26.936+07', '1');
INSERT INTO "cate_products" VALUES ('90', '1', '57', 'f', '2019-08-18 04:22:26.936+07', '2019-08-18 04:22:26.936+07', '2');
INSERT INTO "cate_products" VALUES ('91', '1', '57', 'f', '2019-08-18 04:22:26.936+07', '2019-08-18 04:22:26.936+07', '3');
INSERT INTO "cate_products" VALUES ('92', '4', '58', 'f', '2019-08-18 04:27:17.105+07', '2019-08-18 04:27:17.105+07', '1');
INSERT INTO "cate_products" VALUES ('93', '4', '58', 'f', '2019-08-18 04:27:17.105+07', '2019-08-18 04:27:17.105+07', '2');
INSERT INTO "cate_products" VALUES ('94', '4', '58', 'f', '2019-08-18 04:27:17.105+07', '2019-08-18 04:27:17.105+07', '3');
INSERT INTO "cate_products" VALUES ('95', '2', '59', 'f', '2019-08-18 04:29:02.63+07', '2019-08-18 04:29:02.63+07', '1');
INSERT INTO "cate_products" VALUES ('96', '2', '59', 'f', '2019-08-18 04:29:02.63+07', '2019-08-18 04:29:02.63+07', '2');
INSERT INTO "cate_products" VALUES ('97', '2', '59', 'f', '2019-08-18 04:29:02.63+07', '2019-08-18 04:29:02.63+07', '3');
INSERT INTO "cate_products" VALUES ('98', '4', '60', 'f', '2019-08-18 04:35:37.918+07', '2019-08-18 04:35:37.918+07', '1');
INSERT INTO "cate_products" VALUES ('99', '4', '60', 'f', '2019-08-18 04:35:37.918+07', '2019-08-18 04:35:37.918+07', '2');
INSERT INTO "cate_products" VALUES ('100', '4', '60', 'f', '2019-08-18 04:35:37.918+07', '2019-08-18 04:35:37.918+07', '3');
INSERT INTO "cate_products" VALUES ('101', '1', '61', 'f', '2019-08-18 04:38:04.706+07', '2019-08-18 04:38:04.706+07', '1');
INSERT INTO "cate_products" VALUES ('102', '1', '61', 'f', '2019-08-18 04:38:04.706+07', '2019-08-18 04:38:04.706+07', '2');
INSERT INTO "cate_products" VALUES ('103', '1', '61', 'f', '2019-08-18 04:38:04.706+07', '2019-08-18 04:38:04.706+07', '3');
INSERT INTO "cate_products" VALUES ('104', '4', '62', 'f', '2019-08-18 04:48:54.252+07', '2019-08-18 04:48:54.252+07', '2');
INSERT INTO "cate_products" VALUES ('105', '4', '62', 'f', '2019-08-18 04:48:54.252+07', '2019-08-18 04:48:54.252+07', '1');
COMMIT;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS "categories";
CREATE TABLE "categories" (
"id" int4 DEFAULT nextval('categories_id_seq1'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default",
"gender_id" int4,
"description" varchar(500) COLLATE "default",
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of categories
-- ----------------------------
BEGIN;
INSERT INTO "categories" VALUES ('1', 'Originals', null, null, 'f', '2019-08-05 21:24:44+07', '2019-08-05 21:24:46+07');
INSERT INTO "categories" VALUES ('2', 'Lifestyle', null, null, 'f', '2019-08-05 21:25:27+07', '2019-08-05 21:25:29+07');
INSERT INTO "categories" VALUES ('3', 'Giày bóng rổ', null, null, 'f', '2019-08-05 21:26:41+07', '2019-08-05 21:26:43+07');
INSERT INTO "categories" VALUES ('4', 'Giày chạy', null, null, 'f', '2019-08-05 21:26:06+07', '2019-08-05 21:26:08+07');
INSERT INTO "categories" VALUES ('20', 'Bánh cuốn', null, 'nóng', 't', '2019-08-14 15:31:57.381+07', '2019-08-14 17:44:13.995+07');
COMMIT;

-- ----------------------------
-- Table structure for gender_cates
-- ----------------------------
DROP TABLE IF EXISTS "gender_cates";
CREATE TABLE "gender_cates" (
"id" int4 DEFAULT nextval('gender_cates_id_seq'::regclass) NOT NULL,
"categories_id" int4,
"gender_id" int4,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of gender_cates
-- ----------------------------
BEGIN;
INSERT INTO "gender_cates" VALUES ('1', '1', '1', 'f', '2019-08-06 22:41:59+07', '2019-08-06 22:41:59+07');
INSERT INTO "gender_cates" VALUES ('2', '1', '2', 'f', '2019-08-06 22:41:59+07', '2019-08-06 22:41:59+07');
INSERT INTO "gender_cates" VALUES ('3', '1', '3', 'f', '2019-08-06 22:41:59+07', '2019-08-06 22:41:59+07');
INSERT INTO "gender_cates" VALUES ('4', '2', '1', 'f', '2019-08-06 23:47:33+07', '2019-08-06 23:47:36+07');
INSERT INTO "gender_cates" VALUES ('5', '2', '2', 'f', '2019-08-06 23:55:31+07', '2019-08-06 23:55:33+07');
INSERT INTO "gender_cates" VALUES ('6', '2', '3', 'f', '2019-08-06 23:55:40+07', '2019-08-06 23:55:41+07');
INSERT INTO "gender_cates" VALUES ('7', '3', '1', 'f', '2019-08-06 23:55:54+07', '2019-08-06 23:55:55+07');
INSERT INTO "gender_cates" VALUES ('8', '3', '2', 'f', '2019-08-06 23:58:18+07', '2019-08-06 23:58:20+07');
INSERT INTO "gender_cates" VALUES ('9', '4', '1', 'f', '2019-08-06 23:58:27+07', '2019-08-06 23:58:29+07');
INSERT INTO "gender_cates" VALUES ('10', '4', '2', 'f', '2019-08-14 10:54:31+07', '2019-08-14 10:54:33+07');
INSERT INTO "gender_cates" VALUES ('11', '4', '3', 'f', '2019-08-14 10:54:49+07', '2019-08-14 10:54:51+07');
INSERT INTO "gender_cates" VALUES ('12', '20', '1', 'f', '2019-08-14 15:31:57.472+07', '2019-08-14 15:31:57.472+07');
COMMIT;

-- ----------------------------
-- Table structure for genders
-- ----------------------------
DROP TABLE IF EXISTS "genders";
CREATE TABLE "genders" (
"id" int4 DEFAULT nextval('genders_id_seq'::regclass) NOT NULL,
"gender" varchar(255) COLLATE "default",
"image" varchar(255) COLLATE "default",
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of genders
-- ----------------------------
BEGIN;
INSERT INTO "genders" VALUES ('1', 'Nam', 'men.jpg', 'f', '2019-08-05 21:22:13+07', '2019-08-05 21:22:15+07');
INSERT INTO "genders" VALUES ('2', 'Nữ', 'women.jpg', 'f', '2019-08-05 21:23:23+07', '2019-08-05 21:23:26+07');
INSERT INTO "genders" VALUES ('3', 'Trẻ em', 'kid.jpg', 'f', '2019-08-05 21:22:38+07', '2019-08-05 21:22:40+07');
COMMIT;

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS "images";
CREATE TABLE "images" (
"id" int4 DEFAULT nextval('images_id_seq'::regclass) NOT NULL,
"product_id" int4,
"images" varchar(255) COLLATE "default",
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of images
-- ----------------------------
BEGIN;
INSERT INTO "images" VALUES ('40', '50', '16f0922a-d49b-4974-9cd7-5fb0e3b64d7e_0e3b18a9-45a3-42ac-84a2-1aa2366bafa9AF1.jpg', '2019-08-17 23:24:40.71+07', '2019-08-17 23:24:40.71+07');
INSERT INTO "images" VALUES ('41', '50', 'a00f686d-c1c8-4fae-bb32-e0f5281f4420_02c3e5b2-e329-428c-a506-c4f64b862cbaab.jpg', '2019-08-17 23:24:40.724+07', '2019-08-17 23:24:40.724+07');
INSERT INTO "images" VALUES ('42', '50', '294bcb7b-63aa-4ff9-91d4-14bbccad44c9_3cba443b-369a-47c1-a3e5-e6815c65804efalcon.jpg', '2019-08-17 23:24:40.733+07', '2019-08-17 23:24:40.733+07');
INSERT INTO "images" VALUES ('43', '51', 'be49a026-1365-4e5f-acc1-89f771ef2721_02c3e5b2-e329-428c-a506-c4f64b862cbaab.jpg', '2019-08-17 23:34:14.275+07', '2019-08-17 23:34:14.275+07');
INSERT INTO "images" VALUES ('44', '51', 'eb28b853-78a5-442f-bb75-181f27ca25ab_3cba443b-369a-47c1-a3e5-e6815c65804efalcon.jpg', '2019-08-17 23:34:14.304+07', '2019-08-17 23:34:14.304+07');
INSERT INTO "images" VALUES ('45', '51', '81c823d6-c524-4c76-b074-92b8f6d38900_am1.jpg', '2019-08-17 23:34:14.308+07', '2019-08-17 23:34:14.308+07');
INSERT INTO "images" VALUES ('46', '52', 'd1f94ff2-91fa-4da5-9f8d-4cf5955645ef_UB2.jpg', '2019-08-18 04:08:43.199+07', '2019-08-18 04:08:43.199+07');
INSERT INTO "images" VALUES ('47', '52', '2b01915b-3891-4c95-a4c1-ce15342c3966_ub22.jpg', '2019-08-18 04:08:43.211+07', '2019-08-18 04:08:43.211+07');
INSERT INTO "images" VALUES ('48', '52', 'e82d8169-e1f0-487a-bb93-abed8dc4149c_ub23.jpg', '2019-08-18 04:08:43.212+07', '2019-08-18 04:08:43.212+07');
INSERT INTO "images" VALUES ('49', '53', 'a14e29cd-7020-40e0-a901-f5b118f5bfc4_star2.jpg', '2019-08-18 04:12:19.329+07', '2019-08-18 04:12:19.329+07');
INSERT INTO "images" VALUES ('50', '53', '4d3f059a-5666-46a3-9e21-a18ecd15dca4_star3.jpg', '2019-08-18 04:12:19.343+07', '2019-08-18 04:12:19.343+07');
INSERT INTO "images" VALUES ('51', '53', '01f410f7-f388-4a09-a90e-26a3517143fe_star4.jpg', '2019-08-18 04:12:19.344+07', '2019-08-18 04:12:19.344+07');
INSERT INTO "images" VALUES ('52', '54', 'f737c101-e420-49d4-b35b-3f230fac849c_stan2.jpg', '2019-08-18 04:15:40.331+07', '2019-08-18 04:15:40.331+07');
INSERT INTO "images" VALUES ('53', '54', '385cf14c-b9da-4471-9187-127de0906dee_stan3.jpg', '2019-08-18 04:15:40.332+07', '2019-08-18 04:15:40.332+07');
INSERT INTO "images" VALUES ('54', '54', 'e8a6db44-84f3-4056-aab5-bac6b43efd61_stan4.jpg', '2019-08-18 04:15:40.369+07', '2019-08-18 04:15:40.369+07');
INSERT INTO "images" VALUES ('55', '54', '3bbdacdf-7733-4138-bb4d-8463310fcfc1_stan5.jpg', '2019-08-18 04:15:40.37+07', '2019-08-18 04:15:40.37+07');
INSERT INTO "images" VALUES ('56', '55', 'a3cd5df9-5529-44a6-bc36-eb122f3d132b_lbr2.jpg', '2019-08-18 04:18:40.842+07', '2019-08-18 04:18:40.842+07');
INSERT INTO "images" VALUES ('57', '55', 'd88140fd-9068-4340-915e-f804dfc46ddc_lbr3.jpg', '2019-08-18 04:18:40.843+07', '2019-08-18 04:18:40.843+07');
INSERT INTO "images" VALUES ('58', '55', '1bdd8d83-141e-42d3-a44c-45228f82772b_lbr4.jpg', '2019-08-18 04:18:40.854+07', '2019-08-18 04:18:40.854+07');
INSERT INTO "images" VALUES ('59', '55', '4f6d8304-abef-4c34-a09c-de8c5ea90f26_lbr5.jpg', '2019-08-18 04:18:40.878+07', '2019-08-18 04:18:40.878+07');
INSERT INTO "images" VALUES ('60', '56', 'f65f3f2e-5bf2-4e5f-af68-813235d302e9_jrd2.jpg', '2019-08-18 04:20:30.092+07', '2019-08-18 04:20:30.092+07');
INSERT INTO "images" VALUES ('61', '56', '5c0bf63b-be94-4203-b7d4-02083f5ba864_jrd3.jpg', '2019-08-18 04:20:30.111+07', '2019-08-18 04:20:30.111+07');
INSERT INTO "images" VALUES ('62', '57', 'aee982cc-6147-4136-bcc7-af03b2b43c90_eqt1.jpg', '2019-08-18 04:23:10.415+07', '2019-08-18 04:23:10.415+07');
INSERT INTO "images" VALUES ('63', '57', '481631e9-1d2f-45a8-93c8-d1d2d00a739c_eqt3.jpg', '2019-08-18 04:23:10.431+07', '2019-08-18 04:23:10.431+07');
INSERT INTO "images" VALUES ('64', '57', 'b866680f-4c04-4df7-9820-617ae394c17f_eqt2.jpg', '2019-08-18 04:23:10.432+07', '2019-08-18 04:23:10.432+07');
INSERT INTO "images" VALUES ('65', '57', '745bc959-7d67-41b0-9e45-a2a6f14d6f8d_eqt4.jpg', '2019-08-18 04:23:10.452+07', '2019-08-18 04:23:10.452+07');
INSERT INTO "images" VALUES ('66', '58', '76dc9f07-3ce0-4f89-92b2-8482825db06b_apb1.jpg', '2019-08-18 04:27:34.492+07', '2019-08-18 04:27:34.492+07');
INSERT INTO "images" VALUES ('67', '58', '8427121f-510c-402f-851a-0cdda2d6fb8f_apb2.jpg', '2019-08-18 04:27:34.512+07', '2019-08-18 04:27:34.512+07');
INSERT INTO "images" VALUES ('68', '58', 'c04d8584-6ee5-4f46-a3c4-46fa1defa3c9_apb3.jpg', '2019-08-18 04:27:34.517+07', '2019-08-18 04:27:34.517+07');
INSERT INTO "images" VALUES ('69', '59', '76724eae-60d7-48a3-b295-e54e0fc4a813_af11.jpg', '2019-08-18 04:29:31.532+07', '2019-08-18 04:29:31.532+07');
INSERT INTO "images" VALUES ('70', '59', 'a412f4c0-b789-4f50-91bb-9d487d32a73a_af12.jpg', '2019-08-18 04:29:31.554+07', '2019-08-18 04:29:31.554+07');
INSERT INTO "images" VALUES ('71', '59', '64de2bd8-9c78-45d9-b7c2-f4bd5e3c52d1_af13.jpg', '2019-08-18 04:29:31.558+07', '2019-08-18 04:29:31.558+07');
INSERT INTO "images" VALUES ('72', '59', '58861144-b0bf-42d9-8b7c-ef1f1726f646_af14.jpg', '2019-08-18 04:29:31.563+07', '2019-08-18 04:29:31.563+07');
INSERT INTO "images" VALUES ('73', '59', 'fe80f572-5508-4905-bd69-e184e051f71c_af15.jpg', '2019-08-18 04:29:31.564+07', '2019-08-18 04:29:31.564+07');
INSERT INTO "images" VALUES ('74', '60', '8a3195b5-a02d-40a6-a29c-6809a813e479_UB4.01.jpg', '2019-08-18 04:36:26.22+07', '2019-08-18 04:36:26.22+07');
INSERT INTO "images" VALUES ('75', '60', '9781c49b-a668-4caa-9f23-7ee1c9230a50_UB4.03.jpg', '2019-08-18 04:36:26.238+07', '2019-08-18 04:36:26.238+07');
INSERT INTO "images" VALUES ('76', '60', '5195de07-258d-4309-a2d3-b10d234ad2ba_UB4.02.jpg', '2019-08-18 04:36:26.243+07', '2019-08-18 04:36:26.243+07');
INSERT INTO "images" VALUES ('77', '61', '4886c452-d88d-4cd0-b256-1f07a763ba87_falcon1.jpg', '2019-08-18 04:38:24.589+07', '2019-08-18 04:38:24.589+07');
INSERT INTO "images" VALUES ('78', '61', '259e169c-96cd-4760-b402-fd9cb5da1fb5_falcon2.jpg', '2019-08-18 04:38:24.608+07', '2019-08-18 04:38:24.608+07');
INSERT INTO "images" VALUES ('79', '61', '3b2c964d-29e3-416f-a8b1-33b67d495015_falcon3.jpg', '2019-08-18 04:38:24.618+07', '2019-08-18 04:38:24.618+07');
COMMIT;

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS "order_details";
CREATE TABLE "order_details" (
"id" int4 DEFAULT nextval('order_details_id_seq1'::regclass) NOT NULL,
"order_id" int4,
"product_id" int4,
"color" varchar(255) COLLATE "default",
"size" varchar(255) COLLATE "default",
"quantity" int4,
"status" int4 DEFAULT 1,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of order_details
-- ----------------------------
BEGIN;
INSERT INTO "order_details" VALUES ('1', '1', '4', 'trang', '35', '4', '1', '2019-08-06 21:49:55.9+07', '2019-08-06 21:49:55.9+07');
INSERT INTO "order_details" VALUES ('2', '2', '11', 'trang', '30', '2', '1', '2019-08-07 22:32:11.411+07', '2019-08-07 22:32:11.411+07');
INSERT INTO "order_details" VALUES ('3', '3', '9', 'den', '30', '3', '1', '2019-08-07 22:41:23.123+07', '2019-08-07 22:41:23.123+07');
INSERT INTO "order_details" VALUES ('4', '4', '9', 'den', '30', '1', '1', '2019-08-08 23:26:34.606+07', '2019-08-08 23:26:34.606+07');
INSERT INTO "order_details" VALUES ('5', '5', '9', 'den', '30', '1', '1', '2019-08-08 23:38:16.363+07', '2019-08-08 23:38:16.363+07');
INSERT INTO "order_details" VALUES ('6', '6', '18', 'trang', '40', '1', '1', '2019-08-08 23:40:28.606+07', '2019-08-08 23:40:28.606+07');
INSERT INTO "order_details" VALUES ('7', '7', '6', 'trang', '44', '1', '1', '2019-08-14 11:14:27.269+07', '2019-08-14 11:14:27.269+07');
INSERT INTO "order_details" VALUES ('8', '8', '10', 'den', '44', '3', '1', '2019-08-14 11:14:54.628+07', '2019-08-14 11:14:54.628+07');
INSERT INTO "order_details" VALUES ('9', '9', '9', 'xanh', '44', '2', '1', '2019-08-14 11:15:57.146+07', '2019-08-14 11:15:57.146+07');
INSERT INTO "order_details" VALUES ('10', '9', '11', 'trang', ' 45', '1', '1', '2019-08-14 11:15:57.146+07', '2019-08-14 11:15:57.146+07');
INSERT INTO "order_details" VALUES ('11', '9', '13', 'den', ' 46', '4', '1', '2019-08-14 11:15:57.146+07', '2019-08-14 11:15:57.146+07');
INSERT INTO "order_details" VALUES ('12', '10', '7', 'den', '45', '4', '1', '2019-08-14 11:46:13.595+07', '2019-08-14 11:46:13.595+07');
INSERT INTO "order_details" VALUES ('13', '10', '12', 'xam', '44', '3', '1', '2019-08-14 11:46:13.595+07', '2019-08-14 11:46:13.595+07');
INSERT INTO "order_details" VALUES ('14', '11', '9', 'xanh', '44', '2', '1', '2019-08-14 17:13:15.768+07', '2019-08-14 17:13:15.768+07');
INSERT INTO "order_details" VALUES ('15', '11', '9', 'xanh', '44', '2', '1', '2019-08-14 17:13:15.782+07', '2019-08-14 17:13:15.782+07');
INSERT INTO "order_details" VALUES ('16', '12', '9', 'xanh', '44', '2', '1', '2019-08-14 17:13:15.782+07', '2019-08-14 17:13:15.782+07');
INSERT INTO "order_details" VALUES ('17', '13', '53', 'Trắng', '38', '3', '1', '2019-08-18 21:10:52.633+07', '2019-08-18 21:10:52.633+07');
COMMIT;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS "orders";
CREATE TABLE "orders" (
"id" int4 DEFAULT nextval('orders_id_seq1'::regclass) NOT NULL,
"order_id" varchar(255) COLLATE "default",
"cart_id" varchar(11) COLLATE "default",
"customer_id" varchar(11) COLLATE "default",
"name" varchar(255) COLLATE "default",
"address" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"phone_number" varchar(11) COLLATE "default",
"shipping" varchar(255) COLLATE "default",
"status" int4 DEFAULT 0,
"total_money" float8 DEFAULT 0,
"note" varchar(255) COLLATE "default",
"account_id" varchar(255) COLLATE "default",
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of orders
-- ----------------------------
BEGIN;
INSERT INTO "orders" VALUES ('1', '748dc900-b859-11e9-89d1-f7c5d47a26f6', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '6', '800000', null, null, '2019-08-06 21:49:55.857+07', '2019-08-06 21:50:18.903+07');
INSERT INTO "orders" VALUES ('2', '863a0310-b928-11e9-839b-07ecece8799c', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '6', '200000', null, null, '2019-08-07 22:32:11.331+07', '2019-08-07 22:37:09.338+07');
INSERT INTO "orders" VALUES ('3', 'cf1246f0-b929-11e9-839b-07ecece8799c', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '0', '6', '300000', null, null, '2019-08-07 22:41:23.04+07', '2019-08-14 11:17:35.933+07');
INSERT INTO "orders" VALUES ('4', '49a89110-b9f9-11e9-a368-2d61d1a11f54', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '6', '100000', null, 'shipper', '2019-08-08 23:26:34.531+07', '2019-08-08 23:34:27.535+07');
INSERT INTO "orders" VALUES ('5', 'ebf49cb0-b9fa-11e9-a368-2d61d1a11f54', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '0', '6', '100000', null, null, '2019-08-08 23:38:16.316+07', '2019-08-14 11:17:56.283+07');
INSERT INTO "orders" VALUES ('6', '3acbdec0-b9fb-11e9-a368-2d61d1a11f54', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '6', '200000', null, 'shipper', '2019-08-08 23:40:28.589+07', '2019-08-14 11:17:01.92+07');
INSERT INTO "orders" VALUES ('7', '015d9b80-be4a-11e9-a7fd-87575935171d', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '2', '300000', null, 'shipper', '2019-08-14 11:14:27.13+07', '2019-08-14 11:18:09.591+07');
INSERT INTO "orders" VALUES ('8', '11bb5da0-be4a-11e9-a7fd-87575935171d', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '0', '6', '1200000', null, null, '2019-08-14 11:14:54.586+07', '2019-08-14 11:18:25.241+07');
INSERT INTO "orders" VALUES ('9', '36fd7b70-be4a-11e9-a7fd-87575935171d', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '6', '4100000', null, 'shipper', '2019-08-14 11:15:57.095+07', '2019-08-14 11:59:32.079+07');
INSERT INTO "orders" VALUES ('10', '71a11760-be4e-11e9-a7fd-87575935171d', null, '2', 'hoang', 'Đại học Fpt', 'hoang@gmail.com', '123456789', '1', '6', '1620000', null, 'son', '2019-08-14 11:46:13.462+07', '2019-08-14 11:50:54.716+07');
INSERT INTO "orders" VALUES ('11', '21665ed0-be7c-11e9-a5d3-4b6770b86018', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '6', '800000', null, null, '2019-08-14 17:13:15.711+07', '2019-08-18 15:14:22.187+07');
INSERT INTO "orders" VALUES ('12', '216c7950-be7c-11e9-a5d3-4b6770b86018', null, '2', 'hoang', 'hoang', 'hoang@gmail.com', '123456789', '1', '5', '800000', null, null, '2019-08-14 17:13:15.75+07', '2019-08-18 00:10:27.516+07');
INSERT INTO "orders" VALUES ('13', 'fcc77610-c1c1-11e9-9978-a3f93f85fa03', null, '11', 'Nguyễn Khắc Hoàng', 'Hải phòng', 'hoang@gmail.com', '123456789', '1', '0', '600000', null, null, '2019-08-18 21:10:52.531+07', '2019-08-18 21:10:52.531+07');
COMMIT;

-- ----------------------------
-- Table structure for product_optionals
-- ----------------------------
DROP TABLE IF EXISTS "product_optionals";
CREATE TABLE "product_optionals" (
"id" int4 DEFAULT nextval('product_optionals_id_seq1'::regclass) NOT NULL,
"product_id" varchar(11) COLLATE "default",
"option_name" varchar(255) COLLATE "default",
"inventory" int4 DEFAULT 0,
"price_per_unit" float8,
"is_sale" float8 DEFAULT 0,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_optionals
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS "product_types";
CREATE TABLE "product_types" (
"id" int4 DEFAULT nextval('product_types_id_seq1'::regclass) NOT NULL,
"categories_id" int4,
"name" varchar(255) COLLATE "default",
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_types
-- ----------------------------
BEGIN;
INSERT INTO "product_types" VALUES ('1', '1', 'Stan Smith', '2019-08-07 00:04:09+07', '2019-08-07 00:04:10+07');
INSERT INTO "product_types" VALUES ('2', '1', 'NMD', '2019-08-07 00:04:17+07', '2019-08-07 00:04:18+07');
INSERT INTO "product_types" VALUES ('3', '1', 'EQT', '2019-08-07 00:04:25+07', '2019-08-07 00:04:26+07');
INSERT INTO "product_types" VALUES ('4', '2', 'Nike Air', '2019-08-07 00:04:36+07', '2019-08-07 00:04:37+07');
INSERT INTO "product_types" VALUES ('5', '2', 'Nike Hurache', '2019-08-07 00:07:30+07', '2019-08-07 00:07:31+07');
INSERT INTO "product_types" VALUES ('6', '3', 'Jordan', '2019-08-07 00:07:51+07', '2019-08-07 00:07:53+07');
INSERT INTO "product_types" VALUES ('7', '3', 'LeBron', '2019-08-07 00:08:00+07', '2019-08-07 00:08:01+07');
INSERT INTO "product_types" VALUES ('8', '4', 'Ultra Boots', '2019-08-07 00:09:00+07', '2019-08-07 00:09:02+07');
INSERT INTO "product_types" VALUES ('9', '4', 'Alphaboune', '2019-08-07 00:10:50+07', '2019-08-07 00:10:54+07');
INSERT INTO "product_types" VALUES ('10', '1', 'Super Star', '2019-08-14 09:09:53+07', '2019-08-14 09:09:54+07');
INSERT INTO "product_types" VALUES ('11', '1', 'Falcon', '2019-08-07 22:49:59+07', '2019-08-07 22:50:01+07');
COMMIT;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS "products";
CREATE TABLE "products" (
"id" int4 DEFAULT nextval('products_id_seq1'::regclass) NOT NULL,
"cat_id" varchar(11) COLLATE "default",
"name" varchar(255) COLLATE "default",
"inventory" int4 DEFAULT 0,
"unit" varchar(255) COLLATE "default",
"price_per_unit" float8,
"old_price" float8,
"image" varchar(255) COLLATE "default",
"type_id" int4,
"description" varchar(10000) COLLATE "default",
"size" varchar(500) COLLATE "default",
"color" varchar(500) COLLATE "default",
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of products
-- ----------------------------
BEGIN;
INSERT INTO "products" VALUES ('51', '1', '1111', '1111', 'Đôi', '111', '1111', 'f6bbea6b-ec5e-43f5-acff-3d89f228d0fa_0e3b18a9-45a3-42ac-84a2-1aa2366bafa9AF1.jpg', '1', '1111', '111', '111', 't', '2019-08-17 23:33:33.755+07', '2019-08-18 04:01:34.505+07');
INSERT INTO "products" VALUES ('52', '4', 'ADIDAS ULTRABOOST 4.0 ORCA', '10', 'Đôi', '300000', '200000', '0256193f-6320-46b6-bbb3-d9873c99a6fb_ub21.jpg', '8', 'Đôi giày chạy này kết hợp sự thoải mái và hiệu năng cao cho cảm giác chạy tuyệt vời hơn bao giờ hết. Thân giày Primeknit có tính co giãn cao, thoáng khí, ôm khuôn chân khi bạn đang chạy. Đế giữa boost tăng cường và đế ngoài STRETCHWEB linh hoạt giúp bước chân linh hoạt và tràn đầy năng lượng.', '40, 41, 42 ,43', 'Đen, Trắng', 'f', '2019-08-18 04:05:17.714+07', '2019-08-18 04:05:17.714+07');
INSERT INTO "products" VALUES ('53', '1', 'ADIDAS SUPERSTAR TEM VÀNG', '7', 'Đôi', '200000', '100000', 'd8770894-45c6-4a8d-a572-3a28672ee5df_product2.jpg', '10', 'Cùng với giày adidas Samba hay adidas Stan Smith, giày adidas Superstar là dòng giày đặt nền móng đầu tiên cho sự phát triển không ngừng của hãng giày adidas ngày nay. Ra đời dành riêng cho bộ môn bóng rổ, sức hút của adidas Superstar thậm chí còn vượt xa hơn ngoài phạm vi các sân bóng đến nỗi hãng giày quyết định chuyển hướng đôi giày bóng rổ này sang mục đích thời trang. Đã được xuất hiện khá lâu và thiết kế của giày adidas Superstar gần như không có nhiều sự thay đổi cho đến ngày nay, đôi giày này vẫn luôn nhận được tình cảm yêu mến từ mọi người, đặc biệt là giới trẻ.', '38, 39, 40, 41, 42, 43', 'Trắng', 'f', '2019-08-18 04:11:28.054+07', '2019-08-18 21:10:52.663+07');
INSERT INTO "products" VALUES ('54', '1', 'ADIDAS STANSMITH GREEN', '10', 'Đôi', '200000', '100000', '2afc981b-23c9-45e9-9221-8b8b1edcaac5_product1.jpg', '1', 'Đôi giày này có thiết kế khá đơn giản, thường được làm từ da ở phần thân trên và lưỡi gà. Không giống như phần lớn các mẫu giày khác của Adidas, nó không có biểu tượng ba sọc ở bên ngoài. Thay vào đó, đôi giày có ba đường thẳng được đục lỗ ở cả hai bên.Một số phiên bản của mẫu giày này có thể in hình chân dung của tay vợt Stan Smith ở phần lưỡi giày. Một số khác lại có in hình biểu tượng Adidas rất nhỏ. Thiết kế và kiểu dáng của đôi giày gần như vẫn giữ nguyên kể từ khi ra mắt cho tới giờ, tuy nhiên rất nhiều các phiên bản với màu sắc khác nhau của mẫu giày Stan Smith vẫn được xuất hiện qua các năm.', '38, 39, 40, 41, 42, 43, 44', 'Trắng', 'f', '2019-08-18 04:15:17.045+07', '2019-08-18 04:15:17.045+07');
INSERT INTO "products" VALUES ('55', '3', 'Nike LeBron Soldier 11 ', '10', 'Đôi', '500000', '400000', '2d5187b3-b017-4af7-9d57-cb504526a0c7_leB1.jpg', '7', 'Một phối màu trắng và vàng của Nike LeBron Soldier 11 có trong sách phát hành ngày 3 tháng 6. Đây là giao diện PE Chung kết cổ điển, và dựa trên sự đối xử thuần túy của LeBron, đối với người Celt trong Trò chơi 1 của Chung kết Hội nghị Phục sinh, rõ ràng LBJ đang trên đường đến Chung kết NBA thứ bảy liên tiếp. LeBron Soldier 10 là một trong những sản phẩm bán chạy nhất của Nike Basketball, trong năm ngoái và nó chỉ trở nên tốt hơn vì đôi giày thực sự giảm giá xuống còn 130 đô la. Hình ảnh chính thức đầy đủ chỉ là ở phía trước', '43, 44, 45', 'Trắng xanh', 'f', '2019-08-18 04:17:36.151+07', '2019-08-18 04:17:36.151+07');
INSERT INTO "products" VALUES ('56', '3', 'Jordan retro 4s Military', '10', 'Đôi', '500000', '400000', '349e471c-1a2c-4bad-b74a-74d24928ff31_jrd1.jpg', '6', 'Năm 1989, AJ4 được ra mắt với giá retail là $100, với bốn phối màu O.G thường được gọi là “Bred”, “White Cement”, “Fire Red” và “Military Blues”. Trên đà thành công của AJ3, AJ4 tiếp tục tạo ra cơn bão shopping của các sneakerheads lúc bấy giờ. Một chi tiết khác khiến AJ4 có thêm giá trị về mặt lịch sử đó là trận cuối vòng đầu tiên của mùa playoffs 1989 giữa Chicago Bulls và Cleveland Cavaliers, Michael Jordan đã ghi điểm thứ 44 trong trận đấu cho bản thân, đồng thời đưa Bulls dẫn điểm 101-100 vào giây cuối cùng và mạng lại lại chiến thắng sát sao cho cả đội. Khoảnh khắc MJ ném rổ chính xác và sau đó nhảy lên ăn mừng với AJ 4 “Bred” dưới chân được xếp hạng 20 trong top 25 khoảnh khắc lịch sử của sneakers – theo complex. Nếu bạn nào quan tâm có thể tìm kiếm từ khóa “The Shot” ở Google, kết quả sẽ được hiện lên ngay đầu tiên không cần phải qua trang 2. Đối với người theo dõi NBA từ những năm 1985 và người hâm mộ MJ nói riêng, đây là khoảnh khắc không thể quên được.', '43, 44, 45', 'Trắng xanh', 'f', '2019-08-18 04:19:50.274+07', '2019-08-18 04:19:50.274+07');
INSERT INTO "products" VALUES ('57', '1', 'EQT SUPPORT ADV ALL WHITE', '10', 'Đôi', '300000', '200000', '931d035a-bc9d-4a4b-9db9-81dc94ef367a_eqt5.jpg', '3', 'Nhìn chung, EQT sở hữu ngôn ngữ thiết kế khá hiện đại, cứng cáp, mạnh mẽ, với những đường khối được vát góc mềm mại, tương tự như những gì nó đã thể hiện ở thời điểm trước đây mang lại giá trị lịch sử, ý nghĩa lớn cho lần trở lại này. Bên cạnh đó, một số điểm nhấn cá tính cũng làm cho đôi giày trở nên phá cách, không còn tuân thủ, gò bó theo những xu hướng thời trang nhất thời mà thay vào đó, người ta tập trung hơn vào đôi giày, vào công năng và chất lượng của nó.', '40, 41, 42, 43', 'Trắng', 'f', '2019-08-18 04:22:26.898+07', '2019-08-18 04:22:26.898+07');
INSERT INTO "products" VALUES ('58', '4', 'ALPHABOUNCE BEYOND', '10', 'Đôi', '300000', '200000', '476089e5-4642-47cf-88ff-c452dae6359d_apb.jpg', '9', 'Không sở hữu cả Primeknit và BOOST, tuy nhiên đôi giày vẫn nhận được sự đón nhận nồng nhiệt của các sneakerhead trên toàn thế giới nhờ vào form giày khoẻ khoắn, upper bằng lưới thoải mái cũng như bộ đệm BOUNCE khá tốt.
So với phiên bản Adidas AlphaBounce Beyond 2017, adidas đã phát triển dòng giày tiềm năng AlphaBOUNCE Beyond 2018 bằng nhiều phiên bản cải tiến hơn. Nói đơn giản, đây là một phiên bản cổ lửng của dòng AlphaBOUNCE với thân giày bằng lưới quen thuộc, lưỡi gà dính liền với thân, đi kèm với phần cổ cao hơn bình thường, mặt đế Continental chắc chắn và midsole cũng dày hơn. Với phần thân một mảnh bằng lưới làm bằng công nghệ Aramis cực kỳ hiện đại, phần đế Bounce được gia cố và nới rộng để giúp co giãn và hỗ trợ chân lúc tập luyện nặng và liên tục tốt hơn, và mặt đế cao su Continental cao cấp, phiên bản Beyond thật sự là đôi giày tốt nhất để tập Gym tính tới thời điểm hiện tại ở Việt Nam.
', '37, 38, 39, 40, 41, 42, 43', 'Đen', 'f', '2019-08-18 04:27:17.031+07', '2019-08-18 04:27:17.031+07');
INSERT INTO "products" VALUES ('59', '2', 'AIR FORCE 1 LOW', '10', 'Đôi', '300000', '200000', '4f194637-e0f6-43db-af8f-4fc6247a8273_AF1.jpg', '4', 'Đôi giày làm nên 1 phần lịch sử của Nike và đánh dấu một kỷ nguyên mới trong ngành công nghiệp sneakers.
Được sinh ra vào năm 1982 bởi nhà thiết kế Bruce Kilgore, ngay lập tức đôi giày đã trở thành một ‘hit’ mạnh trên khắp thế giới, ‘sold out’ ngay trong ngày đầu tiên ra mắt. Tại sao ư? Như ở trên đầu bài có nói, đây là một đôi mang tính cách mạng trong thế giới sneakers, khi mà các nhà thiết kế kết hợp với các nhà khoa học cho ra các mẫu giày có công nghệ. AF1 là đôi giày đầu tiên được tích hợp công nghệ ‘air’, một túi khí ở gót chân để đệm và hổ trợ. Air được gắn vào đôi giày với mục đích ban đầu là dùng trong môn bóng rổ, nhưng hiện nay rất ít người mang nó trên sân bóng rổ và thường được dùng với mục đích thời trang. Sau công nghệ Air, Nike cũng đã cho thiết kế thêm Nike với công nghệ VAC Tech vừa ra mắt cách đây vài tháng.
Cái tên Air Force One lấy ý tưởng từ chiếc chuyên cơ cùng tên chuyên dùng chở tổng thống Mỹ. AF1 có 3 style: low-mid-top. Với các style Mid-Top chúng ta dễ dàng nhận thấy một cọng dây đeo có khoá hoặc dán tạo vẻ chắc chắn cho đôi giày và có thể dịch chuyển theo tuỳ phiên bản. Đây cũng là một sự đặc biệt của đôi sneaker này so với các đôi giày khác cùng thời. Một điểm nhận dạng khác của các AF1 là một huy hiệu nhỏ ở giữa dây giày được làm bằng thiếc (ngoài ra có các phiên bản được làm bằng nhựa và bạc) trên đó có khắc dòng chữ ‘AF1′ và con số ‘ “82 ‘ năm sinh của nó.
Air Force 1 có hơn 1.700 phiên bản với nhiều màu khác nhau và ngày càng tăng lên. Nhưng 2 màu cơ bản white-on-white và black-on-black vẫn là hai phiên bản thành công nhất và có số lượng sản phẩm bán ra chạy nhất của dòng sản phẩm này. 12 triệu là số lượng giày được bán ra trong thời kì đỉnh cao của nó vào năm 2005. Con số phản ảnh sự phổ biến của nó trên khắp thế giới. Air Force 1 thu về hơn 800 triệu USD mỗi năm cho Nike. Sự tồn tại của đôi giày này trong hơn 25 năm qua cho ta thấy sự bền vững của AF1 trong trái tim người tiêu dùng.
Đơn giản mà ‘chất’ là đánh giá của nhiều bạn trẻ khi nhắc đến đôi sneakers này. Nhờ vậy mà AF1 đã tồn tại được trong thế giới giày cực kỳ khó tính này. So với mẫu giày kinh điển khác của Nike như Jordan, Air Yeezy,… thì AF1 có mức giá phù hợp với tất cả các đối tượng khách hàng, có những đôi chỉ 50 USD nhưng cũng có những đôi có giá trị cả ngàn USD. Do đó nó trở thành một món hàng sưu tập lý tưởng của các Sneakerhead trên thế giới
', '40, 41, 42 ', 'Trắng', 'f', '2019-08-18 04:29:02.525+07', '2019-08-18 04:29:02.525+07');
INSERT INTO "products" VALUES ('60', '4', 'ADIDAS ULTRABOOST 4.0 CORE BLACK', '10', 'Đôi', '300000', '200000', 'bfcf6ac0-36e7-4298-b161-e8176936f2c4_UB_coreBlack.jpg', '8', '', '41, 42, 43, 44', 'Đen', 'f', '2019-08-18 04:35:37.796+07', '2019-08-18 04:35:37.796+07');
INSERT INTO "products" VALUES ('61', '1', 'ADIDAS FALCON ', '10', 'Đôi', '200000', '150000', '8b00ad96-ec67-4101-b500-608c715d0866_falcon.jpg', '11', 'Dòng giày sneaker adidas Original Falcon được biết đến như mẫu giày thể thao kết hợp sneaker nam được yêu thích nhất từ khi được giới thiệu 2016. adidas Original vừa làm mới thiết kế adidas Falcon bằng đế giày chunky và phối màu sneaker nữ cực chất.
Chất liệu da lộn trên upper giày sneaker adidas Original Falcon W được sử dụng khéo léo với vải giày thoáng hơi, đúng chất thời trang retro cho các bạn nữ năng động.
Bản phối hồng tím, xám, đen và xanh colorway gợi nhớ những mẫu giày sneaker vintage của những nam 80, đi kèm công nghệ mới và cực dễ phối đồ.
', '41, 42, 43', 'Trắng xanh', 'f', '2019-08-18 04:38:04.593+07', '2019-08-18 04:38:04.593+07');
INSERT INTO "products" VALUES ('62', '4', '111', '11', 'Đôi', '11', '11', '', '9', '11', '1', '11', 't', '2019-08-18 04:48:54.192+07', '2019-08-18 04:49:28.018+07');
COMMIT;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "roles";
CREATE TABLE "roles" (
"id" int4 DEFAULT nextval('roles_id_seq'::regclass) NOT NULL,
"role_name" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO "roles" VALUES ('0', 'GUEST', null, '2019-06-20 22:00:07+07', '2019-06-20 22:00:07+07');
INSERT INTO "roles" VALUES ('1', 'STAFF', null, '2019-06-20 22:00:07+07', '2019-06-20 22:00:07+07');
INSERT INTO "roles" VALUES ('2', 'MANAGER', null, '2019-06-20 22:00:07+07', '2019-06-20 22:00:07+07');
INSERT INTO "roles" VALUES ('3', 'ADMIN', null, '2019-06-20 22:00:07+07', '2019-06-20 22:00:07+07');
COMMIT;

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS "statuses";
CREATE TABLE "statuses" (
"id" int4 DEFAULT nextval('statuses_id_seq'::regclass) NOT NULL,
"status_name" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of statuses
-- ----------------------------
BEGIN;
INSERT INTO "statuses" VALUES ('0', 'Đang chờ xử lý', null, '2019-07-23 21:45:36+07', '2019-07-23 21:45:36+07');
INSERT INTO "statuses" VALUES ('1', 'Đã xử lý', null, '2019-07-23 21:45:36+07', '2019-07-23 21:45:36+07');
INSERT INTO "statuses" VALUES ('2', 'Đang ship', null, '2019-07-23 21:45:36+07', '2019-07-23 21:45:36+07');
INSERT INTO "statuses" VALUES ('3', 'Đơn hàng đã thanh toán', null, '2019-07-23 21:45:36+07', '2019-07-23 21:45:36+07');
INSERT INTO "statuses" VALUES ('4', 'Đơn hàng đã thanh toán 50% giá trị', null, '2019-07-23 21:45:36+07', '2019-07-23 21:45:36+07');
INSERT INTO "statuses" VALUES ('5', 'Hủy đơn hàng', null, '2019-07-24 10:04:54+07', '2019-07-24 10:04:54+07');
INSERT INTO "statuses" VALUES ('6', 'Đơn hàng đã hoàn thành', null, '2019-07-24 10:04:54+07', '2019-07-24 10:04:54+07');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "users";
CREATE TABLE "users" (
"id" int4 DEFAULT nextval('users_id_seq1'::regclass) NOT NULL,
"account_id" int4,
"fullname" varchar(255) COLLATE "default",
"age" int4,
"address" varchar(200) COLLATE "default",
"phone_number" varchar(13) COLLATE "default",
"email" varchar(200) COLLATE "default",
"gennder" bool,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "users" VALUES ('1', '1', 'manager', null, 'manager', '123456789', 'manager@gmail.com', null, 'f', '2019-08-06 21:45:44.861+07', '2019-08-06 21:45:44.861+07');
INSERT INTO "users" VALUES ('2', '2', 'hoang', null, 'hoang', '123456789', 'hoang@gmail.com', null, 'f', '2019-08-06 21:49:02.022+07', '2019-08-17 21:15:17.387+07');
INSERT INTO "users" VALUES ('3', '3', 'admin', null, 'admin', '123456789', 'admin@gmail.com', null, 'f', '2019-08-07 22:33:08.36+07', '2019-08-07 22:33:08.36+07');
INSERT INTO "users" VALUES ('4', '4', 'shipper', null, 'shipper', '123456789', 'shipper@gmail.com', null, 'f', '2019-08-07 22:33:42.343+07', '2019-08-07 22:33:42.343+07');
INSERT INTO "users" VALUES ('9', '9', 'staff', null, 'staff', '123456789', 'staff@gmail.com', null, 'f', '2019-08-14 11:21:05.299+07', '2019-08-14 11:21:05.299+07');
INSERT INTO "users" VALUES ('10', '10', 'son ', null, 'son ', '09123333', 'son@gmail.com', null, 'f', '2019-08-14 11:35:19.051+07', '2019-08-14 11:35:19.051+07');
INSERT INTO "users" VALUES ('11', '11', 'Nguyễn Khắc Hoàng', null, 'Hải phòng', '123456789', 'hoang@gmail.com', null, 'f', '2019-08-18 20:36:20.393+07', '2019-08-18 20:36:20.393+07');
INSERT INTO "users" VALUES ('12', '12', 'Nguyễn Khắc Hoàng', null, 'Hải phòng', '123456789', 'hoang@gmail.com', null, 'f', '2019-08-18 20:36:24.956+07', '2019-08-18 20:36:24.956+07');
COMMIT;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "accounts_id_seq1" OWNED BY "accounts"."id";
ALTER SEQUENCE "cate_products_id_seq" OWNED BY "cate_products"."id";
ALTER SEQUENCE "categories_id_seq1" OWNED BY "categories"."id";
ALTER SEQUENCE "gender_cates_id_seq" OWNED BY "gender_cates"."id";
ALTER SEQUENCE "genders_id_seq" OWNED BY "genders"."id";
ALTER SEQUENCE "images_id_seq" OWNED BY "images"."id";
ALTER SEQUENCE "order_details_id_seq1" OWNED BY "order_details"."id";
ALTER SEQUENCE "orders_id_seq1" OWNED BY "orders"."id";
ALTER SEQUENCE "product_optionals_id_seq1" OWNED BY "product_optionals"."id";
ALTER SEQUENCE "product_types_id_seq1" OWNED BY "product_types"."id";
ALTER SEQUENCE "products_id_seq1" OWNED BY "products"."id";
ALTER SEQUENCE "roles_id_seq" OWNED BY "roles"."id";
ALTER SEQUENCE "statuses_id_seq" OWNED BY "statuses"."id";
ALTER SEQUENCE "users_id_seq1" OWNED BY "users"."id";

-- ----------------------------
-- Primary Key structure for table accounts
-- ----------------------------
ALTER TABLE "accounts" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table cate_products
-- ----------------------------
ALTER TABLE "cate_products" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table categories
-- ----------------------------
ALTER TABLE "categories" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table gender_cates
-- ----------------------------
ALTER TABLE "gender_cates" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table genders
-- ----------------------------
ALTER TABLE "genders" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table images
-- ----------------------------
ALTER TABLE "images" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table order_details
-- ----------------------------
ALTER TABLE "order_details" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table orders
-- ----------------------------
ALTER TABLE "orders" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product_optionals
-- ----------------------------
ALTER TABLE "product_optionals" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product_types
-- ----------------------------
ALTER TABLE "product_types" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table products
-- ----------------------------
ALTER TABLE "products" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "roles" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table statuses
-- ----------------------------
ALTER TABLE "statuses" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "categories"
-- ----------------------------
ALTER TABLE "categories" ADD FOREIGN KEY ("gender_id") REFERENCES "genders" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "order_details"
-- ----------------------------
ALTER TABLE "order_details" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "product_types"
-- ----------------------------
ALTER TABLE "product_types" ADD FOREIGN KEY ("categories_id") REFERENCES "categories" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "users"
-- ----------------------------
ALTER TABLE "users" ADD FOREIGN KEY ("account_id") REFERENCES "accounts" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
