/*
Navicat PGSQL Data Transfer

Source Server         : localhost
Source Server Version : 90508
Source Host           : localhost:5432
Source Database       : fu_gruation
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90508
File Encoding         : 65001

Date: 2019-06-18 16:23:02
*/


-- ----------------------------
-- Sequence structure for accounts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."accounts_id_seq";
CREATE SEQUENCE "public"."accounts_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 5
 CACHE 1;
SELECT setval('"public"."accounts_id_seq"', 5, true);

-- ----------------------------
-- Sequence structure for categories_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."categories_id_seq";
CREATE SEQUENCE "public"."categories_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for order_details_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_details_id_seq";
CREATE SEQUENCE "public"."order_details_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for orders_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."orders_id_seq";
CREATE SEQUENCE "public"."orders_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_optionals_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_optionals_id_seq";
CREATE SEQUENCE "public"."product_optionals_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_types_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_types_id_seq";
CREATE SEQUENCE "public"."product_types_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for products_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."products_id_seq";
CREATE SEQUENCE "public"."products_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."users_id_seq"', 4, true);

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS "public"."accounts";
CREATE TABLE "public"."accounts" (
"id" int4 DEFAULT nextval('accounts_id_seq'::regclass) NOT NULL,
"username" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"role_id" int4,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO "public"."accounts" VALUES ('1', 'bangnx1', '8f1c053eb0621e5997e5886bf82bffef', '1', 't', '2019-06-16 15:27:33+07', '2019-06-16 15:27:33+07');
INSERT INTO "public"."accounts" VALUES ('4', 'admin', '21232f297a57a5a743894a0e4a801fc3', null, 'f', '2019-06-17 18:31:56.404+07', '2019-06-17 18:31:56.404+07');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS "public"."categories";
CREATE TABLE "public"."categories" (
"id" int4 DEFAULT nextval('categories_id_seq'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default",
"description" varchar(500) COLLATE "default",
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO "public"."categories" VALUES ('1', 'Giày chạy', null, 'f', '2019-06-16 16:06:46+07', '2019-06-16 16:06:51+07');
INSERT INTO "public"."categories" VALUES ('2', 'Lifestyle', null, 'f', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."categories" VALUES ('3', 'Giày bóng rổ', null, 'f', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."categories" VALUES ('4', 'Originals', null, 'f', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_details";
CREATE TABLE "public"."order_details" (
"id" int4 DEFAULT nextval('order_details_id_seq'::regclass) NOT NULL,
"order_id" varchar(255) COLLATE "default",
"product_id" varchar(11) COLLATE "default",
"quantity" int4,
"status" int4 DEFAULT 1,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of order_details
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS "public"."orders";
CREATE TABLE "public"."orders" (
"id" int4 DEFAULT nextval('orders_id_seq'::regclass) NOT NULL,
"order_id" varchar(255) COLLATE "default",
"cart_id" varchar(11) COLLATE "default",
"customer_id" varchar(11) COLLATE "default",
"name" varchar(255) COLLATE "default",
"address" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"phone_number" varchar(11) COLLATE "default",
"shipping" bool DEFAULT true,
"status" int4 DEFAULT 0,
"total_money" float8 DEFAULT 0,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for product_optionals
-- ----------------------------
DROP TABLE IF EXISTS "public"."product_optionals";
CREATE TABLE "public"."product_optionals" (
"id" int4 DEFAULT nextval('product_optionals_id_seq'::regclass) NOT NULL,
"product_id" varchar(11) COLLATE "default",
"option_name" varchar(255) COLLATE "default",
"inventory" int4 DEFAULT 0,
"price_per_unit" float8,
"is_sale" float8 DEFAULT 0,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_optionals
-- ----------------------------

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS "public"."product_types";
CREATE TABLE "public"."product_types" (
"id" int4 DEFAULT nextval('product_types_id_seq'::regclass) NOT NULL,
"categories_id" int4,
"name" varchar(255) COLLATE "default",
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_types
-- ----------------------------
INSERT INTO "public"."product_types" VALUES ('1', '1', 'Ultra Boost', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('2', '2', 'Stan Smith', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('3', '1', 'Supper Star', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('4', '2', 'Yeezy', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('5', '2', 'NMD', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('6', '3', 'Nite Jogger', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('7', '3', 'Dame', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('8', '4', 'Solar Boost', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."product_types" VALUES ('9', '4', 'Lười', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS "public"."products";
CREATE TABLE "public"."products" (
"id" int4 DEFAULT nextval('products_id_seq'::regclass) NOT NULL,
"cat_id" varchar(11) COLLATE "default",
"name" varchar(255) COLLATE "default",
"inventory" int4 DEFAULT 0,
"unit" varchar(255) COLLATE "default",
"price_per_unit" float8,
"image" varchar(255) COLLATE "default",
"type_id" int4,
"description" varchar(500) COLLATE "default",
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO "public"."products" VALUES ('1', '1', 'Adidas Nam', '10', 'đôi', '100000', 'product1.jpg', '1', 'Đây là đôi giày bán chạy nhất thế giới.', 'f', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."products" VALUES ('2', '2', 'Adidas Nam', '10', 'đôi', '100000', 'product2.jpg', '2', 'Đây là đôi giày bán chạy nhất thế giới.', 'f', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."products" VALUES ('3', '3', 'Adidas Nam', '10', 'đôi', '100000', 'product3.jpg', '3', 'Đây là đôi giày bán chạy nhất thế giới.', 'f', '2019-06-10 16:08:59+07', '2019-06-10 16:08:59+07');
INSERT INTO "public"."products" VALUES ('4', '4', 'Adidas M', '10', 'đôi', '100000', 'product4.jpg', '4', 'Đây là đôi giày bán chạy nhất thế giới.', 'f', '2019-06-17 16:08:59+07', '2019-06-10 16:08:59+07');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."roles";
CREATE TABLE "public"."roles" (
"id" int4 NOT NULL,
"role_name" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of roles
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
"id" int4 DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
"account_id" int4,
"fullname" varchar(255) COLLATE "default",
"age" int4 DEFAULT 0,
"address" varchar(200) COLLATE "default",
"phone_number" varchar(13) COLLATE "default",
"email" varchar(200) COLLATE "default",
"gennder" bool,
"is_delete" bool DEFAULT false,
"createdAt" timestamptz(6) NOT NULL,
"updatedAt" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES ('1', '1', 'Nguyễn Xuân Bằng', '20', '232 Phạm Văn Đồng', '0966189407', 'bangnx1@msb.com.vn', 'f', 'f', '2019-06-17 15:32:44.147+07', '2019-06-17 15:32:44.147+07');
INSERT INTO "public"."users" VALUES ('3', '4', 'Nguyễn Văn Admin', null, '232 Phạm Văn Đồng', '0966189407', 'admin@gmail.com', null, 'f', '2019-06-17 18:31:56.516+07', '2019-06-17 18:31:56.516+07');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."accounts_id_seq" OWNED BY "accounts"."id";
ALTER SEQUENCE "public"."categories_id_seq" OWNED BY "categories"."id";
ALTER SEQUENCE "public"."order_details_id_seq" OWNED BY "order_details"."id";
ALTER SEQUENCE "public"."orders_id_seq" OWNED BY "orders"."id";
ALTER SEQUENCE "public"."product_optionals_id_seq" OWNED BY "product_optionals"."id";
ALTER SEQUENCE "public"."product_types_id_seq" OWNED BY "product_types"."id";
ALTER SEQUENCE "public"."products_id_seq" OWNED BY "products"."id";
ALTER SEQUENCE "public"."users_id_seq" OWNED BY "users"."id";

-- ----------------------------
-- Primary Key structure for table accounts
-- ----------------------------
ALTER TABLE "public"."accounts" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table categories
-- ----------------------------
ALTER TABLE "public"."categories" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table order_details
-- ----------------------------
ALTER TABLE "public"."order_details" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table orders
-- ----------------------------
ALTER TABLE "public"."orders" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product_optionals
-- ----------------------------
ALTER TABLE "public"."product_optionals" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product_types
-- ----------------------------
ALTER TABLE "public"."product_types" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table products
-- ----------------------------
ALTER TABLE "public"."products" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "public"."roles" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."product_types"
-- ----------------------------
ALTER TABLE "public"."product_types" ADD FOREIGN KEY ("categories_id") REFERENCES "public"."categories" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
