import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificeComponent } from './notifice.component';

describe('NotificeComponent', () => {
  let component: NotificeComponent;
  let fixture: ComponentFixture<NotificeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
