import { Component, OnInit } from '@angular/core';
import { FPT } from '../utils/config/service';

@Component({
  selector: 'app-notifice',
  templateUrl: './notifice.component.html',
  styleUrls: ['./notifice.component.css']
})
export class NotificeComponent implements OnInit {

  IMAGES_SERVICE = FPT.API_SERVICE;
  
  constructor() { }

  ngOnInit() {
  }

}
