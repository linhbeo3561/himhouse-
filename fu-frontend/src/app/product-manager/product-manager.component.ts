import { Component, OnInit } from '@angular/core';
import { Product } from '../module/product';
import { CoreService } from '../service/core.service';
import { PATH_URI } from '../utils/common/path';
import { Router } from '@angular/router';
import { FPT } from '../utils/config/service';
import { ToastrService } from 'ngx-toastr';
import { Category } from '../module/category';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import * as uuid from 'uuid';
import * as $ from "jquery";

@Component({
  selector: 'app-product-manager',
  templateUrl: './product-manager.component.html',
  styleUrls: ['./product-manager.component.css']
})
export class ProductManagerComponent implements OnInit {

  dataGender: any;
  dataProductById: Product;
  listProduct: Product[];
  config: any;
  accontInfo: any;
  name = "";
  inventory: any;
<<<<<<< HEAD
  price_per_unit: any;
=======
  price_per_unit = "";
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
  size = "";
  color = "";
  listDataCategory: Category[];
  listDataType: any;
  categorySelected: any;
  genderSelected: any;
  categorySelectedTemp: any;
  typeSelected: any;
  des: any;
  product_id: string;
  message = "";
  productImages: any;
  typeSelectedTemp: any;
  image: any;
  list_image: any = [];
  gender = [];
  listGenderCate: any;
  old_price: any;
  isFlatCreateProduct = false;
  isFlatAddThu = false;
  filePreviewPath: string | SafeUrl;
  IMAGES_SERVICE = FPT.API_SERVICE;
  IMAGES = FPT.API_SERVICE + "/images/home/";
  API = FPT.API_SERVICE + FPT.API_EXTENSION;
  public uploader: FileUploader = new FileUploader({ url: `${this.API}uploadfile/`, itemAlias: 'photo' });

  /**
   * Method upload thum
   */
  uploadThum() {
    for (let i = 0; i < this.uploader.queue.length; i++) {
      let fileItem = this.uploader.queue[i]._file;
      if (fileItem.size > 10000000) {
        alert("Vui lòng chọn file <= 10 MB.");
        return;
      }
    }
    this.uploader.uploadAll();
    if (this.list_image) {
      this.iService.findByID("images", this.product_id).subscribe(result => { });
      this.list_image.forEach(element => {
        this.saveImagesThumToDB(element);
      });
    }
    alert("Bạn đã thêm hình ảnh phụ thành công.");
    window.location.reload();
  }


  /**
   * Method valid number 
   */
  validNumber() {
    if (this.inventory <= 0) {
      this.iMessage.warning("Số lượng sản phẩm > 0", "E-Shopper", { timeOut: 3000 });
    }
  }

  /**
   * Method valid number 
   */
<<<<<<< HEAD
  validNumberMoney() {
    if (this.old_price <= 0) {
      this.iMessage.warning("Giá sản phẩm > 0", "E-Shopper", { timeOut: 3000 });
    }
  }

  /**
   * Method valid number 
   */
  validNumberMoneyPrice() {
    if (this.price_per_unit <= 0) {
      this.iMessage.warning("Giá sản phẩm > 0", "E-Shopper", { timeOut: 3000 });
    }
  }

    /**
=======
  validNumber() {
    if (this.inventory <= 0) {
      this.iMessage.warning("Số lượng sản phẩm > 0", "E-Shopper", { timeOut: 3000 });
    }
  }
  /**
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
   * Method save images thum to db
   */
  saveImagesThumToDB(file_name: any) {
    // this.uploader.uploadAll();
    let json = {
      "product_id": this.product_id,
      "images": file_name
    }
    this.iService.insertObject("images", json).subscribe(result => {
      if (result) {
        this.list_image = [];
      }
    });
  }


  /**
  * Config pagging
  */
  configPagging() {
    this.config = {
      itemsPerPage: 100,
      currentPage: 1,
      totalItems: this.listProduct
    };
  }

  /**
   * Method change color
   */
  methodChangeColor(quantity: any) {
    if (quantity <= 3) {
      return { "background-color": "yellow", "color": "black" }
    }
  }

  /**
   * Method check null
   * @param data 
   */
  isCheckNull(data: any) {
    let isFlat = false;
    if (data == null || data == "" || data == undefined) {
      isFlat = true;
    }
    return isFlat;
  }

  /**
   * Method update product
   */
  updateProduct() {
    if (!this.validNull(this.name) || !this.validNull(this.inventory) || !this.validNull(this.old_price) || !this.validNull(this.price_per_unit) || !this.validNull(this.size) || !this.validNull(this.color)) {
      this.iMessage.warning("Vui lòng điền đầy đủ thông tin sản phẩm.", "E-Shopper", { timeOut: 3000 });
    } else if (this.inventory <= 0) {
      this.iMessage.warning("Số lượng sản phẩm > 0", "E-Shopper", { timeOut: 3000 });
    } else if (this.old_price < 0 || this.price_per_unit < 0) {
      this.iMessage.warning("Vui lòng nhập số tiền > 0", "E-Shopper", { timeOut: 3000 });
    } else {
      let json = this.createJsonProduct(this.dataProductById.id);
      this.iService.insertObject(PATH_URI.PRODUCT, json).subscribe(
        result => {
          if (result) {
            this.isFlatAddThu = true;
            this.iMessage.success("Bạn đã lưu sản phẩm thành công.", "E-Shopper", { timeOut: 3000 });
            this.uploader.uploadAll();
            this.findAllProduct();
          }
        }, err => {
          this.iMessage.error("Có lỗi xẩy ra trong quá trình xử lý.", "E-Shopper", { timeOut: 3000 });
        }
      );
    }
  }

  /**
   * Method create json product update
   */
  createJsonProduct(id) {
    let json = {
      "id": id,
      "name": this.name,
      "inventory": this.inventory,
      "old_price": this.old_price,
      "price_per_unit": this.price_per_unit,
      "size": this.size,
      "color": this.color,
      "cat_id": this.categorySelected,
      "type_id": this.typeSelected,
      "gender_id": this.gender,
      "description": this.des,
      "image": this.image,
      "unit": "Đôi"
    };
    return json;
  }
  /**
   * 
   * @param event 
   */
  pageChanged(event) {
    this.config.currentPage = event;
  }

  constructor(private iService: CoreService,
    private iMessage: ToastrService,
    public sanitizer: DomSanitizer,
    private router: Router) {
    this.hideVChat();
    this.configPagging();
  }

  hideVChat() {
    $("#embed_circle").hide();
    $("#embed_fill").hide();
    $("#embed_vchat").hide();
  }

  /**
   * Method find all data product
   */
  findAllProduct() {
    this.iService.findAll(PATH_URI.PRODUCT).subscribe(
      result => {
        if (result) {
          this.listProduct = result;
          result.forEach(element => {
            this.iService.findByID(PATH_URI.TYPE, element.type_id).subscribe(
              data => {
                if (data) {
                  element.prod_type = data;
                }
              }
            );
          });
        }
      }
    );
  }

  /**
   * Method find all product
  */
  findAllCaregory() {
    this.iService.findAll(PATH_URI.CATEGORY).subscribe(
      category => {
        if (category != null) {
          this.listDataCategory = category;
          this.categorySelected = this.listDataCategory[0].id;
        }
      });
  }

  /**
   * Method delete product by id
   */
  deleteProduct(product: any) {
    if (confirm("Bạn có chắc muốn xóa sản phẩm : " + product.name)) {
      this.iService.deleteByID(PATH_URI.PRODUCT, product.id).subscribe(
        result => {
          if (result) {
            alert("Bạn đã xóa sản phẩm thành công!");
            this.init();
          }
        }, err => {
          if (err) {
            alert("Có lỗi xẩy ra trong quá trình xử lý!");
          }
        }
      );
    }
  }

  /**
   * Method edit acc
   * @param acc 
   */
  editProduct(product: any) {
    console.log(product);
    this.productImages = product.images;
    this.product_id = product.id;
    this.image = product.image;
    this.filePreviewPath = this.IMAGES + product.image;
    this.isFlatCreateProduct = false;
    this.dataProductById = product;
    this.name = product.name;
    this.inventory = product.inventory;
    this.old_price = product.old_price;
    this.price_per_unit = product.price_per_unit;
    this.size = product.size;
    this.color = product.color;
    this.categorySelected = product.cat_id;
    this.typeSelected = product.prod_type.id;
    this.des = product.description;
    this.setDataCombobox(product.cat_id);
    this.findGenderByPId(product.id);
  }

  /**
   * Method add product
   */
  addProduct() {
    this.isFlatCreateProduct = true;
    this.name = "";
    this.inventory = 1;
    this.price_per_unit = "";
    this.old_price = "";
    this.size = "";
    this.color = "";
    this.categorySelected = "";
    this.typeSelected = "";
    this.genderSelected = "";
    this.des = "";
    this.image = "";
    this.dataProductById = new Product();
  }

  /**
   * Method close 
   */
  closePoupup() {
    // window.location.reload();
  }
  /**
   * Method create product
   */
  createProduct() {
    if (!this.validNull(this.name) || !this.validNull(this.inventory) || !this.validNull(this.old_price) || !this.validNull(this.price_per_unit) || !this.validNull(this.size) || !this.validNull(this.color)) {
      this.iMessage.warning("Vui lòng điền đầy đủ thông tin sản phẩm.", "E-Shopper", { timeOut: 3000 });
    } else if (this.inventory <= 0) {
      this.iMessage.warning("Số lượng sản phẩm > 0", "E-Shopper", { timeOut: 3000 });
<<<<<<< HEAD
    } else if (this.old_price < 0 || this.price_per_unit < 0) {
      this.iMessage.warning("Vui lòng nhập số tiền > 0", "E-Shopper", { timeOut: 3000 });
=======
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
    } else {
      let json = this.createJsonProduct(null);
      this.iService.insertObject(PATH_URI.CREATE_PRODUCT, json).subscribe(
        result => {
          if (result) {
            alert("Bạn đã thêm sản phẩm thành công!");
            this.uploader.uploadAll();
            window.location.reload();
          }
        }, err => {
          this.iMessage.error("Có lỗi xẩy ra trong quá trình xử lý.", "E-Shpper", { timeOut: 3000 });
        }
      );
    }
  }

  /**
   * Method check null
   */
  validNull(data: any) {
    let isFlat = true;
    if (data == "" || data == undefined || data == null) {
      isFlat = false;
    }
    return isFlat;
  }

  /**
   * 
   * @param id 
   */
  setDataCombobox(id: any) {
    this.listDataCategory.forEach(element => {
      if (element.id == id) {
        this.categorySelected = element.id;
        this.listDataType = element.product_types;
        this.typeSelected = this.listDataType[0].id;
        return;
      }
    });
  }

  /**
  * 
  * @param id 
  */
  updateGender($event) {
    let value = $event.target.defaultValue;
    if ($event.target.checked) {
      this.gender.push(value);
    } else {
      this.gender = this.arrayRemove(this.gender, value);
    }
    console.log(this.gender);
  }

  /**
   * Method find gender by pro id
   * @param id 
   */
  findGenderByPId(id) {
    $("#gender_1")[0].checked = false;
    $("#gender_2")[0].checked = false;
    $("#gender_3")[0].checked = false;
    this.gender = [];
    this.iService.findByID("product/filter-gender-pid", id).subscribe(result => {
      if (result) {
        this.dataGender = result;
        this.dataGender.forEach(item => {
          $("#gender_" + item.gender_id)[0].checked = true;
          this.gender.push(item.gender_id);
        });
      }
    });
  }

  /**
  * Remove
  * @param arr 
  * @param value 
  */
  arrayRemove(arr, value) {
    return arr.filter(function (ele) {
      return ele != value;
    });
  }

  /**
   * 
   * @param id 
   */
  findTypeProduct(id: any) {
    this.listDataType.array.forEach(element => {
      if (element.id == id) {
        this.typeSelectedTemp = element.name;
        return;
      }
    });
  }

  /**
   * 
   * @param $event 
   */
  onchangeCategory($event) {
    this.setDataCombobox($event.target.value);
  }

  /**
   * 
   * @param $event 
   */
  onchangeType($event) {
    this.typeSelected = $event.target.value;
  }

  /**
   * Method on change gender
   * @param $event 
   */
  onchangeGender($event) {
    this.genderSelected = $event.target.value;
  }
  /**
   * Method find token from localstorage
   */
  findToken() {
    let acc = localStorage.getItem("account_^((!");
    if (acc != null) {
      this.accontInfo = JSON.parse(acc);
      if (!(this.accontInfo.role_id == 2)) {
        this.login();
      }
    } else {
      this.login();
    }
  }

  /**
   * Method back to login
   */
  login() {
    this.router.navigate(['/login']);
  }

  /**
   * Method init
   */
  init() {
    this.addProduct();
    this.findToken();
    this.findAllProduct();
    this.findAllCaregory();
  }

  ngOnInit() {
    this.init();
    //test upload image
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
      let imgName = uuid.v4() + "_" + file.file.name;
      this.image = imgName;
      this.list_image.push(imgName);
      file.file.name = this.image;
      this.filePreviewPath = this.sanitizer.bypassSecurityTrustResourceUrl((window.URL.createObjectURL(file._file)))['changingThisBreaksApplicationSecurity'];
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.uploader.onCompleteAll();
      this.list_image = [];
    };
  }

}
