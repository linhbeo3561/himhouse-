import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Product } from '../module/product';
import { Category } from '../module/category';
import { Type } from '../module/productType';
import { CoreService } from "../service/core.service";
import { PATH_URI } from "../utils/common/path";
import { ProductService } from '../service/product.service';
import { FPT } from '../utils/config/service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  // Create variable
  IMAGES_SERVICE = FPT.API_SERVICE;
  listDataProduct: Product[];
  listDataProductTemp: Product[];
  listDataProductTop4: Product[];
  listDataCategory: Category[];
  listDataType: any;
  listDateSize: any;
  config: any;
  message = "";
  name: string;
  isClickShowMoreSize = false;
  isClickShowMoreProductType = false;
  titleShowPage = "Gợi ý cho hôm nay";
  nameSearchFilter: string;

  constructor(
    private iService: CoreService,
    private router: ActivatedRoute,
    private routerRidect: Router,
    private productService: ProductService
  ) {
    window.scrollTo(0, 0);
    this.configPagging();
  }

  /**
 * Method redirect
 * @param id 
 */
  redirect(id) {
    let pathRedirect = "/product/" + id;
    this.routerRidect.navigate([pathRedirect]);
  }

  /**
   * Find id in URL
   */
  findIdInURL() {
    let name = this.router.snapshot.paramMap.get('name');
    this.name = name;
    this.findByName(name);
  }

  /**
   * Method find all cate by id
   */
  findByName($event) {
    this.titleShowPage = "Gợi ý cho hôm nay";
    this.message = "";
    if ($event != null && $event != "") {
      this.productService.filter($event).subscribe(
        result => {
          this.titleShowPage = "Sản phẩm  '" + $event + "' ";
          this.listDataProduct = result;
          if (this.listDataProduct.length == 0) {
            this.message = "Chúng tôi không tìm thấy sản phẩm '" + $event + "' nào.";
          }
          this.configPagging();
        }
      );
    } else {
      this.titleShowPage = "Gợi ý cho hôm nay";
      this.findAllProduct(null, null);
    }
  }

  /**
   * Config pagging
   */
  configPagging() {
    this.config = {
      itemsPerPage: 9,
      currentPage: 1,
      totalItems: this.listDataProduct
    };
  }

  /**
   * Filter by parent
   * @param $event
   */
  filterParent($event) {
    this.titleShowPage = "Gợi ý cho hôm nay";
    this.message = "";
    if ($event != null && $event != "") {
      this.productService.filter($event).subscribe(
        result => {
          this.titleShowPage = "Sản phẩm  '" + $event + "' ";
          this.listDataProduct = result;
          if (this.listDataProduct.length == 0) {
            this.message = "Chúng tôi không tìm thấy sản phẩm '" + $event + "' nào.";
          }
          this.configPagging();
        }
      );
    } else {
      this.titleShowPage = "Gợi ý cho hôm nay";
      this.findAllProduct(null, null);
    }
  }
  /**
   * Method find by category
   */
  findByTypeId(id: any) {
    this.message = "";
    this.iService.findByID(PATH_URI.PRODUCT_TYPE, id).subscribe(
      product => {
        this.listDataProduct = product;
        if (this.listDataProduct.length == 0) {
          this.message = "Chúng tôi không tìm thấy sản phẩm nào.";
        }
      });
  }

  /**
   * 
   * @param event 
   */
  pageChanged(event) {
    this.config.currentPage = event;
  }
  /**
   * Method find all product
  */
  findAllProduct(start, end) {
    this.iService.findAll(PATH_URI.PRODUCT).subscribe(
      product => {
        if (product != null) {
          //Create dummy data
          this.findDataSize(product, start, end);
        }
      }, err => { });
  }

  /**
   * 
   * @param listDataProd 
   */
  findDataSize(listDataProd: any, start = null, end = null) {
    let dataSizeStr = "";
    listDataProd.forEach(dataPro => {
      dataSizeStr += dataPro.size + ",";
    });
    let size = JSON.parse("[" + dataSizeStr.substr(0, dataSizeStr.length - 1) + "]");
    let dataSizeArr = this.deduplicate(size);
    if (start != null && end != null) {
      this.listDateSize = dataSizeArr.splice(start, end);
    } else {
      this.listDateSize = dataSizeArr;
    }
  }

  /**
   * Method remove item duplicate
   * @param arr 
   */
  deduplicate(arr) {
    let isExist = (arr, x) => {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] === x) return true;
      }
      return false;
    }
    let ans = [];
    arr.forEach(element => {
      if (!isExist(ans, element)) ans.push(element);
    });
    return ans;
  }
  /**
   * Method find all product
  */
  findAllCaregory() {
    this.iService.findAll(PATH_URI.CATEGORY).subscribe(
      category => {
        if (category != null) {
          let cateTemp = this.deduplicateCate(category);
          this.listDataCategory = cateTemp;
        }
      });
  }

  /**
   * Method remove item duplicate
   * @param arr 
   */
  deduplicateCate(arr) {
    let isExist = (arr, x) => {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].name === x) return true;
      }
      return false;
    }
    let ans = [];
    arr.forEach(element => {
      if (!isExist(ans, element.name)) ans.push(element);
    });
    return ans;
  }
  /**
   * Method find all product
  */
  findAllProductBySize(size: any) {
    this.iService.findByID("product/size", size).subscribe(
      size => {
        if (size != null) {
          this.listDataProduct = size;
        }
      });
  }

  /**
   * Method show more size
   */
  showMoreSize(type: any) {
    if (type == 0) {
      this.isClickShowMoreProductType = true;
      this.findAllProducType(null, null);
    } else if (type == 1) {
      this.isClickShowMoreSize = true;
      this.findAllProduct(null, null);
    }
  }
  /**
   * Method find all prod type
   */
  findAllProducType(start, end) {
    this.iService.findAll("type").subscribe(
      result => {
        if (result) {
          if (start != null && end != null) {
            this.listDataType = result.splice(start, end);
          } else {
            this.listDataType = result;
          }
        }
      }
    );
  }

  /**
   * Method find all product
  */
  findAllProductTop4() {
    this.iService.findAll(PATH_URI.TOP_4).subscribe(
      product => {
        if (product != null) {
          this.listDataProductTop4 = product;
        }
      });
  }

  init() {
    this.findIdInURL();
    this.findAllCaregory();
    this.findAllProducType(0, 4);
    this.findAllProduct(0, 4);
  }

  ngOnInit() {
    this.init();
  }

}
