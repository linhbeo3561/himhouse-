import { Component, OnInit } from '@angular/core';
import { CoreService } from '../service/core.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Category } from '../module/category';
import { FPT } from '../utils/config/service';
import { PATH_URI } from '../utils/common/path';
import * as $ from 'jquery';

@Component({
  selector: 'app-category-product',
  templateUrl: './category-product.component.html',
  styleUrls: ['./category-product.component.css']
})
export class CategoryProductComponent implements OnInit {

  accontInfo: any;
  config: any;
  idCategoriesSelected: any;
  categoriesName: string;
  description: string;
  listDataCategory: Category[];
  categoryById: Category;
  gender = [];
  isFlatCreateCategory = false;
  listGenderCate: any;
  categorySelected: any;
  productTypeNew: any;
  listProductTypeByCate: any;
  IMAGES_SERVICE = FPT.API_SERVICE;

  /**
  * Config pagging
  */
  configPagging() {
    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.listDataCategory
    };
  }

  /**
   * Method find all 
   */
  findAllProductTypeByCateId(id) {
    this.idCategoriesSelected = id;
    this.iService.findByID("type/categories", id).subscribe(
      result => {
        if (result) {
          this.listProductTypeByCate = result;
        }
      }
    );
  }


  /**
   * Method delete prod type
   */
  deleteProdType(podType) {
    this.iService.findByID("product/type", podType.id).subscribe(
      result => {
        if (result) {
          if (result.length > 0) {
            this.iMessage.warning("Đang có sản phẩm sử dụng product type này!", "E-Shopper", { timeOut: 3000 });
          } else {
            this.iService.findByID("type/delete-prod-type", podType.id).subscribe(
              result => {
                if (result) {
                  this.findAllProductTypeByCateId(this.idCategoriesSelected);
                  this.iMessage.success("Bạn đã xóa thành công product type.", "E-Shopper", { timeOut: 3000 });
                }
              }
            );
          }
        }
      }
    );
  }

  /**
   * 
   * @param id 
   */
  updateGender($event) {
    let value = $event.target.defaultValue;
    if ($event.target.checked) {
      this.gender.push(value);
    } else {
      this.gender = this.arrayRemove(this.gender, value);
    }
    console.log(this.gender);
  }

  /**
   * Remove
   * @param arr 
   * @param value 
   */
  arrayRemove(arr, value) {
    return arr.filter(function (ele) {
      return ele != value;
    });
  }

  /**
   * Method check null
   * @param data 
   */
  isCheckNull(data: any) {
    let isFlat = false;
    if (data == null || data == "" || data == undefined) {
      isFlat = true;
    }
    return isFlat;
  }

  /**
   * 
   * @param event 
   */
  pageChanged(event) {
    this.config.currentPage = event;
  }

  constructor(private iService: CoreService,
    private iMessage: ToastrService,
    private router: Router) {
    this.configPagging();
  }

  /**
   * 
   * @param category 
   */
  editCategory(category) {
    this.isFlatCreateCategory = false;
    this.categoriesName = category.name;
    this.description = category.description;
    this.categoryById = category;
    this.findAllGenderCaregoryById(category.id);
    this.findAllProductTypeByCateId(category.id);
  }

  /**
   * Method delete type
   * @param type 
   */
  deleteType(type) {
    if (confirm("Bạn có chắc muốn xóa loại sản phẩm : " + type.name)) {

    }
  }
  /**
   * Method create product
   */
  createProduct() {
    if (this.isCheckNull(this.categoriesName) || this.isCheckNull(this.description) || this.gender.length == 0) {
      this.iMessage.warning("Vui lòng nhập đầy đủ thông tin.", "E-Shopper", { timeOut: 3000 });
    } else {
      let json = this.creaetJsonCategory(null);
      this.iService.insertObject(PATH_URI.CATEGORY_CREATE, json).subscribe(
        result => {
          if (result) {
            this.iMessage.success("Tạo mới thành công.", "E-Shopper", {
              timeOut: 3000
            });
            window.location.reload();
          }
          this.findAllCaregory();
        }
      );
    }
  }

  /**
   * 
   */
  createProductType() {
    if (this.isCheckNull(this.categorySelected) || this.isCheckNull(this.productTypeNew)) {
      this.iMessage.warning("Vui lòng nhập đầy đủ thông tin.", "E-Shopper", { timeOut: 3000 });
    } else {
      this.iService.findByID("type/is-exit-prod-name", this.productTypeNew).subscribe(
        result => {
          if (result) {
            if (result.length > 0) {
              this.iMessage.warning(this.productTypeNew + " đã tồn tại!", "E-Shopper", { timeOut: 3000 });
            } else {
              this.iService.insertObject("type/insert/prod-type", this.createJsonNewProdType()).subscribe(
                result => {
                  if (result) {
                    this.iMessage.success("Thêm mới product type thành công.", "E-Shopper", { timeOut: 3000 });
                    window.location.reload();
                  }
                }
              );
            }
          }
        }
      );
    }
  }

  createJsonNewProdType() {
    let json = {
      "categories_id": this.categorySelected,
      "name": this.productTypeNew
    }
    return json;
  }

  /**
   * Method add product
   */
  addProduct() {
    this.isFlatCreateCategory = true;
    this.categoriesName = "";
    this.description = "";
    this.categoryById = null;
  }

  /**
   * Method update category
   */
  updateCategory() {
    if (this.isCheckNull(this.categoriesName) || this.isCheckNull(this.description) || this.gender.length == 0) {
      this.iMessage.warning("Vui lòng nhập đầy đủ thông tin.", "E-Shopper", { timeOut: 3000 });
    } else {
      let json = this.creaetJsonCategory(this.categoryById.id);
      this.iService.insertObject(PATH_URI.CATEGORY, json).subscribe(
        result => {
          if (result) {
            this.iMessage.success("Lưu thành công.", "E-Shopper", {
              timeOut: 3000
            });
          }
          this.findAllCaregory();
        }
      );
    }
  }

  /**
   * 
   * @param category 
   */
  creaetJsonCategory(id) {
    let json = {
      "id": id,
      "name": this.categoriesName,
      "description": this.description,
      "gender": this.gender
    }
    return json;
  }


  /**
   * Method delete category
   * @param category 
   */
  deleteCategory(category) {
    if (confirm("Bạn có chắc muốn xóa loại sản phẩm : " + category.name)) {
      this.iService.deleteByID(PATH_URI.CATEGORY, category.id).subscribe(
        result => {
          if (result.id != undefined || result.id != null) {
            this.iMessage.warning("Đang có sản phẩm sử dụng loại sản phẩm này.", "E-Shopper", {
              timeOut: 3000
            });
          } else {
            this.iMessage.success("Bạn đã xóa thành công loại sản phẩm " + category.name + ".", "E-Shopper", {
              timeOut: 3000
            });
          }
          this.findAllCaregory();
        }
      );
    }
  }

  /**
   * Method find all product
  */
  findAllCaregory() {
    this.iService.findAll(PATH_URI.CATEGORY).subscribe(
      category => {
        if (category != null) {
          let cateTemp = this.deduplicateCate(category);
          this.listDataCategory = cateTemp;
        }
      });
  }

  /**
   * Method find all product
  */
  findAllGenderCaregoryById(cateId) {
    $("#gender_1")[0].checked = false;
    $("#gender_2")[0].checked = false;
    $("#gender_3")[0].checked = false;
    this.gender = [];
    this.iService.findByID(PATH_URI.GENDER_CATE, cateId).subscribe(
      genderCategory => {
        if (genderCategory != null) {
          this.listGenderCate = genderCategory;
          this.listGenderCate.forEach(element => {
            $("#gender_" + element.gender_id)[0].checked = true;
            this.gender.push(element.gender_id);
          });
        }
      });
  }

  /**
   * Method remove item duplicate
   * @param arr 
   */
  deduplicateCate(arr) {
    let isExist = (arr, x) => {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].name === x) return true;
      }
      return false;
    }
    let ans = [];
    arr.forEach(element => {
      if (!isExist(ans, element.name)) ans.push(element);
    });
    return ans;
  }
  /**
   * Method find token from localstorage
   */
  findToken() {
    let acc = localStorage.getItem("account_^((!");
    if (acc != null) {
      this.accontInfo = JSON.parse(acc);
      if (!(this.accontInfo.role_id == 2)) {
        this.login();
      }
    } else {
      this.login();
    }
  }

  /**
   * Method back to login
   */
  login() {
    this.router.navigate(['/login']);
  }

  /**
   * Method init
   */
  init() {
    this.findToken();
    this.findAllCaregory();
  }

  ngOnInit() {
    this.init();
  }
}
