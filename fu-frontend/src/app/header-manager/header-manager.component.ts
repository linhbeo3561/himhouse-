import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-header-manager',
  templateUrl: './header-manager.component.html',
  styleUrls: ['./header-manager.component.css']
})
export class HeaderManagerComponent implements OnInit {

  userRole = 0;
  accontInfo: any;
  role_id = 0;

  constructor(private router: Router) {
    $("#embed_vchat").hide();
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.findToken();
  }

  /**
   * Method find token from localstorage
   */
  findToken() {
    let acc = localStorage.getItem("account_^((!");
    if (acc != null) {
      this.accontInfo = JSON.parse(acc);
      this.role_id = JSON.parse(acc).role_id;
    } else {
      this.logout();
    }
  }
  /**
   * Method logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
