import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Account } from '../module/account';
import { Router } from '@angular/router';
import { Utils } from '../utils/common/EShop';
import { FPT } from '../utils/config/service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  IMAGES_SERVICE = FPT.API_SERVICE;
  accountName: string;
  isFlatLogin = false;
  @Output() nameSearchFilter = new EventEmitter();
  nameSearch: string = "";
  role_id = 0;
  id = "";
  pathManager = "product";
  orderLength = 0;

  /**
   * Method init
   */
  init() {
    let acc = localStorage.getItem("account_^((!");
    let accountName = localStorage.getItem("acc_name_!@!@");
    if (accountName != null) {
      this.isFlatLogin = true;
      this.accountName = accountName;
      this.role_id = JSON.parse(acc).role_id;
      if (JSON.parse(acc).role_id == '-1') {
        this.id = JSON.parse(acc).users[0].fullname;
      } else {
        this.id = JSON.parse(acc).id;
      }
      this.findRole(this.role_id);
    }
  }

  /**
   * Method find role and redirect
   */
  findRole(role: any) {
    let pathRedirect = 'product';
    switch (role) {
      case Utils.ROLE.ADMIN:
        pathRedirect = 'chart-manager'
        break;
      case Utils.ROLE.GUEST:
        pathRedirect = 'product';
        break;
      case Utils.ROLE.MANAGER:
        pathRedirect = 'product-manager'
        break;
      case Utils.ROLE.STAFF:
        pathRedirect = 'order-manager'
        break;
    }
    this.pathManager = pathRedirect;
  }

  /**
   * Filter by name
   */
  filterByName() {
    if (this.nameSearch != null) {
      this.nameSearchFilter.emit(this.nameSearch);
    }
  }

  /**
   * Method logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  constructor(private router: Router) { }

  ngOnInit() {
    this.init();
  }

}
