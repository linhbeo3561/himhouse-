import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utils } from '../utils/common/EShop';
import * as CanvasJS from '../utils/canvasjs.min.js';
import { KpiService } from '../service/kpi.service';

@Component({
  selector: 'app-chart-manager',
  templateUrl: './chart-manager.component.html',
  styleUrls: ['./chart-manager.component.css']
})
export class ChartManagerComponent implements OnInit {

  // Variable month or year
  start_date: string;
  end_date: string;
  type: number = 1;
  accontInfo: any;

  constructor(private router: Router, private kpiService: KpiService) { }

  /**
   * Method find KPI
   */
  find_KPI() {
    let channel = 'quantity';
    if (this.type == 1) {
      channel = 'price'
    } else if (this.type == 2) {
      channel = 'price-interest'
    }
    this.userChart(channel);
  }

  /**
   * Method user chart
   */
  userChart(channel: any) {
    this.kpiService.findKpi(this.start_date, this.end_date, channel).subscribe(
      result => {
        if (result) {
          if (channel == 'price-interest') {
            this.renderChartMoney(result);
          } else if (channel == 'quantity') {
            this.renderChart(result);
          } else {
            this.renderChart2(result);
          }
        }
      }
    );
  }

  /**
   * Method render chart money
   */
  renderChartMoney(data) {
    console.log(data);
    let chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2",
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: `Biểu Đồ Thống Kê Số Tiền Lãi. `
      },
      legend: {
        cursor: "pointer",
        itemclick: this.explodePie
      },
      data: [{
        type: "column",
        dataPoints: data
      }]
    });
    chart.render();
  }

  /**
   * Method render chart
   * @param data 
   */
  renderChart(data) {
    let chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2",
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: `Biểu Đồ Thống Kê Số Lượng Sản Phẩm.`
      },
      legend: {
        cursor: "pointer",
        itemclick: this.explodePie
      },
      data: [{
        type: "pie",
        showInLegend: true,
        toolTipContent: "<b>{name}</b>",
        indexLabel: "{name} - {y} (sản phẩm)",
        dataPoints: data,
      }]
    });
    chart.render();
  }

  /**
   * Method render chart
   * @param data 
   */
  renderChart2(data) {
    let chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2",
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: `Biểu Đồ Thống Kê Số Tiền.`
      },
      legend: {
        cursor: "pointer",
        itemclick: this.explodePie
      },
      data: [{
        type: "pie",
        showInLegend: true,
        toolTipContent: "<b>{name}</b>",
        indexLabel: "{name} - {y} (VND)",
        dataPoints: data,
      }]
    });
    chart.render();
  }

  explodePie(e) {
    if (typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
      e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
    } else {
      e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
    }
    e.chart.render();
  }

  /**
   * Method find token from localstorage
   */
  findToken() {
    let acc = localStorage.getItem("account_^((!");
    if (acc != null) {
      this.accontInfo = JSON.parse(acc);
      if (this.accontInfo.role_id != Utils.ROLE.MANAGER) {
        this.logout();
      }
    } else {
      this.logout();
    }
  }
  /**
   * Method logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  init() {
    this.findToken();
    this.find_KPI();
  }

  ngOnInit() {
    this.init();
  }
}
