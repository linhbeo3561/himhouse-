import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map } from 'rxjs/operators';
import { FPT } from '../utils/config/service';

const httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Cache-Control', 'no-cache');

let options = {
  headers: httpHeaders
};

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  // Variable service
  private API_URL = FPT.API_SERVICE + FPT.API_EXTENSION;

  constructor(private http: HttpClient) { }

  /**
  * Find list product data.
  */
  findAll(channel: string) {
    return this.http.get<any[]>(`${this.API_URL}${channel}/`).pipe(map(data => data));
  }

  /**
   * Method find by ID
   * 
   * @param id : ID can find
   */
  findByID(channel: string, id: any) {
    return this.http.get<any>(`${this.API_URL}${channel}/${id}`).pipe(map(data => data));
  }

  /**
   * Method delete by id
   * @param channel 
   * @param id 
   */
  deleteByID(channel: string, id: any) {
    return this.http.delete<any>(`${this.API_URL}${channel}/${id}`, options).pipe(map(data => data));
  }

  /**
   * Find list account data.
   */
  insertObject(channel: string, newData: any) {
    return this.http.post<any>(`${this.API_URL}${channel}`, newData, options);
  };
}
