import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FPT } from '../utils/config/service';

const httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Cache-Control', 'no-cache');

let options = {
  headers: httpHeaders
};

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  // Variable service
  private API_URL = FPT.API_SERVICE + FPT.API_EXTENSION;

  /**
   * Find list account data.
   */
  filter(username: string) {
    return this.http.post<any>(`${this.API_URL}product/filter/`,
      {
        "name": username
      }, options);
  };

  constructor(private http: HttpClient) { }
}
