import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../core.service';
import { Utils } from 'src/app/utils/common/EShop';
import { StatusBuy } from 'src/app/utils/common/StatusOrder';
import $ from "jquery";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  accontInfo: any;
  statusBuy: string;
  list_data_order: any;
  list_data_shipper: any;
  shipperId: any;
  list_data_order_details: any;
  config: any;
  id: string;
  STATUSBUYCOMMON = StatusBuy;
  total_buy: number = 0;
  status: number;
  isShipping = false;
  statusOrder = "";

  constructor(
    private router: Router,
    private iService: CoreService,
    private iNotifice: ToastrService,
    private routerActive: ActivatedRoute) {
    this.configPagging();
  }

  /**
   * Config pagging
   */
  configPagging() {
    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.list_data_order_details
    };
  }

  /**
   * Find all list shipper
   */
  findAllShipper() {
    this.iService.findByID("account/find-by-role", -1).subscribe(
      result => {
        if (result) {
          this.list_data_shipper = result;
        }
      }
    );
  }

  /**
   * Method find all by status
   * 
   * @param status 
   */
  findAllById(id: any) {
    this.iService.findByID("order/info", id).subscribe(
      result => {
        if (result) {
          this.list_data_order = result;
          // console.log(this.list_data_order);
          this.status = result[0]['status'];
          this.statusOrder = this.STATUSBUYCOMMON[this.status];
        }
      }
    );
  }

  /**
   * Method find order details by id
   */
  findOrderDetails(id: any) {
    this.total_buy = 0;
    this.iService.findByID("order/customer", id).subscribe(
      result => {
        if (result) {
          this.list_data_order_details = result;
          console.log(this.list_data_order_details);
          result.forEach(element => {
            this.total_buy += element.price_per_unit * element.quantity;
          });
        }
      }
    );
  }
  /**
  * Find id in url
  */
  findIdInURL() {
    let id = this.routerActive.snapshot.paramMap.get('id');
    if (id == null || id == '' || id == "") {
      this.router.navigate(['not-found']);
    } else {
      this.id = id;
      this.findAllById(id);
      this.findOrderDetails(id);
    }
  }

  /**
   * Method update status rent
   */
  changeStatus() {
    if (confirm("Bạn có chắc chắn muốn chuyển trạng thái đơn hàng thành: [" + this.STATUSBUYCOMMON[this.statusBuy] + "]")) {
      if (this.statusBuy == '2') {
        this.isShipping = true;
      }
      // Method update order status
      let id = this.list_data_order[0].id;
      let status = this.statusBuy;
      this.iService.findByID("order/update-status-by-order-id", id + "/" + status).subscribe(
        result => {
          if (result) {
            this.iNotifice.success("Chuyển trạng thái đơn hàng thành công.", "E-Shopper", {
              timeOut: 3000
            });
            this.statusOrder = this.STATUSBUYCOMMON[status];
            this.init();
          }
        }
      );
    }
  }

  /**
   * Method change shipper
   */
  changeShipper() {
    let id = this.list_data_order[0].id;
    let name = this.shipperId;
    this.iService.findByID("order/update-shipper-by-order-id", id + "/" + name).subscribe(
      result => {
        if (result) {
          this.iNotifice.success("Chọn người vận chuyển hàng thàng công.", "E-Shopper", {
            timeOut: 3000
          });
          this.init();
        }
      }
    );
  }

  /**
   * 
   * @param event 
   */
  pageChanged(event) {
    this.config.currentPage = event;
  }

  /**
   * Method find token from localstorage
   */
  findToken() {
    let acc = localStorage.getItem("account_^((!");
    if (acc != null) {
      this.accontInfo = JSON.parse(acc);
      if (this.accontInfo.role_id == Utils.ROLE.GUEST || this.accontInfo.role_id == Utils.ROLE.ADMIN) {
        this.logout();
      }
    } else {
      this.logout();
    }
  }
  /**
   * Method logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  init() {
    this.findToken();
    this.findIdInURL();
    this.findAllShipper();
  }

  ngOnInit() {
    this.init();
  }
}
