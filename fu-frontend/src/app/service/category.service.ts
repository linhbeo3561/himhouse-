import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map } from 'rxjs/operators';
import { FPT } from '../utils/config/service';

const httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Cache-Control', 'no-cache');

let options = {
  headers: httpHeaders
};

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  // Variable service
  private API_URL = FPT.API_SERVICE + FPT.API_EXTENSION;

  constructor(private http: HttpClient) { }
}
