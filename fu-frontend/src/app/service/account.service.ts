import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Account } from '../module/account';
import { Info } from '../module/info';
import { FPT } from '../utils/config/service';

const httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Cache-Control', 'no-cache');

let options = {
  headers: httpHeaders
};
@Injectable({
  providedIn: 'root'
})
export class AccountService {

  // Variable service
  private API_URL = FPT.API_SERVICE + FPT.API_EXTENSION;

  constructor(private http: HttpClient) { }

  /**
   * Find list account data.
   */
  doLogin(username: string, password: string) {
    return this.http.post<any>(`${this.API_URL}account/login/`,
      {
        "username": username,
        "password": password
      }, options);
  };

  /**
   * Find list account data.
   */
  accountSignup(info: Info) {
    return this.http.post<Account>(`${this.API_URL}account/signup/`,
      {
        "username": info.accounts.username,
        "password": info.accounts.password,
        "role_id": 0,
        "fullname": info.users.fullname,
        "age": info.users.age,
        "address": info.users.address,
        "phone_number": info.users.phone_number,
        "email": info.users.email,
        "gennder": info.users.gennder
      }, options);
  };
}
