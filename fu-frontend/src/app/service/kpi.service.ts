import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map } from 'rxjs/operators';
import { FPT } from '../utils/config/service';

const httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Cache-Control', 'no-cache');

let options = {
  headers: httpHeaders
};

@Injectable({
  providedIn: 'root'
})
export class KpiService {

  // Variable service
  private API_URL = FPT.API_SERVICE + FPT.API_EXTENSION;

  constructor(private http: HttpClient) { }

  /**
  * Find list find kpi
  */
  findKpi(start_date: string, end_date: string, channel: string) {
    return this.http.get<any[]>(`${this.API_URL}kpi/${channel}?start_date=${start_date}&end_date=${end_date}`).pipe(map(data => data));
  }

}
