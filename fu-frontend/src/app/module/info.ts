import { Account } from './account';
import { User } from './user';

export class Info {
    accounts: Account;
    users: User;
}