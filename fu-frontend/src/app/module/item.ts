import { Product } from './product';

export class Item {
    product: Product;
    quantity: number;
    site: string;
    color: string;
    date: string;
}