import { Type } from './productType';

export class Category {
    id: number;
    name: string;
    description: string;
    is_delete: boolean;
    product_types: Type;
}