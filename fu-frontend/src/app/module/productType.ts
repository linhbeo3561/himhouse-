export class Type {
    id: number;
    category_id: number;
    name: string;
}