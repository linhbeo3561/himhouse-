export class User {
    account_id: string;
    fullname: string;
    age: number;
    address: string;
    phone_number: string;
    email: string;
    gennder: boolean
}