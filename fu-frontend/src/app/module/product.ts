export class Product {
    id: string;
    cat_id: string;
    name: string;
    inventory: number;
    unit: string;
    old_price: string;
    price_per_unit: number;
    image: string;
    type_id: number;
    description: string;
    size: string;
    color: string;
    is_delete: boolean;
    createdAt: string;
    updatedAt: string;
}