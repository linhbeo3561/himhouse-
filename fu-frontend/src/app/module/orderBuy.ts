import { Order } from './order';
import { OrderDetails } from './orderDetais';

export class OrderBuy {
    order: Order;
    orderDetails: OrderDetails;
}