export class Account {
    id: string;
    username: string;
    password: string;
    role_id: number;
    is_delete: string;
}