export class OrderDetails {
    id: string;
    order_id: string;
    product_id: string;
    color: string;
    size: string;
    quantity: string;
    status: number;
    createdAt: string;
}