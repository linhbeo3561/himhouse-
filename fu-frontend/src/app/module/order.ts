export class Order {
    id: string;
    order_id: string;
    cart_id: string;
    customer_id: number;
    name: string;
    address: string;
    email: string;
    phone_number: string;
    shipping: string;
    status: string;
    total_money: number;
    createdAt: string;
}