import { Component, OnInit } from '@angular/core';
import { FPT } from '../utils/config/service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  IMAGES_SERVICE = FPT.API_SERVICE;

  constructor() { }

  ngOnInit() {
  }

}
