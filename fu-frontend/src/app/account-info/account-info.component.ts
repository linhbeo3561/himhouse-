import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../service/core.service';
import { Account } from '../module/account';
import { User } from '../module/user';
import { Info } from '../module/info';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../service/account.service';

@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.css']
})
export class AccountInfoComponent implements OnInit {

  accounts: Account = new Account();
  users: User = new User();
  info: Info = new Info();
  id: any;
  oldPassword: string;
  newPassword: string;
  isFlatChangePass = false;

  constructor(private iService: CoreService,
    private router: ActivatedRoute,
    private iMessage: ToastrService,
    private accountService: AccountService,
    private routerParam: Router) { }

  ngOnInit() {
    this.findId();
  }

  /**
   * Method check length uname
   * @param uname 
   */
  validNull(text) {
    if (text == "" || text == undefined || text == null) {
      return false;
    }
    return true;
  }

  /**
   * Method check phone number
   * @param phone 
   */
  validPhoneNumber(phone) {
    var phoneno = /(09|01[2|6|8|9])+([0-9]{8})\b/g;
    if (!phoneno.test(phone)) {
      this.iMessage.warning("Vui lòng nhập vào đúng số điện thoại.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }

  /**
   * Method valid email
   * @param email 
   */
  validEmail(email) {
    var patternEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!patternEmail.test(email)) {
      this.iMessage.warning("Vui lòng nhập vào địa chỉ email.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }

  /**
  * Method find id
  */
  findId() {
    let id = this.router.snapshot.paramMap.get('id');
    if (id == null || id == '' || id == "" || isNaN(+id)) {
    } else {
      this.id = id;
      this.findAccountByID(id);
    }
  }

  /**
   * Method change password
   */
  changeNewPassword() {
    if (this.newPassword == "" || this.newPassword == null) {
      this.iMessage.warning("Vui lòng nhập vào mật khẩu.", "E-Shopper", {
        timeOut: 3000
      });
    } else {
      let json = {
        "id": this.id,
        "password": this.newPassword
      }
      this.accountService.doLogin(this.accounts.username, this.oldPassword).subscribe(
        result => {
          if (result && result != null) {
            this.iService.insertObject("account/update/account", json).subscribe(result => {
              if (result) {
                this.iMessage.success("Đổi mật khẩu thành công.", "E-Shopper", {
                  timeOut: 3000
                });
              }
            });
          } else {
            this.iMessage.warning("Mật khẩu cũ không đúng, vui lòng nhập lại.", "E-Shopper", {
              timeOut: 3000
            });
          }
        }
      );
    }
  }

  /**
   * Method change pass
   * @param $event 
   */
  changePass($event) {
    this.isFlatChangePass = $event.target.checked;
  }
  /**
   * 
   * @param id 
   */
  findAccountByID(id) {
    this.iService.findByID("account/find-by-id", id).subscribe(result => {
      if (result) {
        this.accounts = result;
        this.users = result.users[0];
      }
    });
  }

  /**
   * Method create json account
   * @param id
   */
  createJsonAccount(id) {
    let json = {
      "username": this.accounts.username,
      "password": this.accounts.password
    }
    return json;
  }

  /**
   * Method create json user
   * @param id 
   */
  createJsonUser(id) {
    let json = {
      "id": id,
      "fullname": this.users.fullname,
      "address": this.users.address,
      "phone_number": this.users.phone_number,
      "email": this.users.email
    }
    return json;
  }

  /**
   * 
   * Method update account
   */
  updateAccount() {
    if (!this.validNull(this.users.fullname) || !this.validNull(this.users.address) || !this.validNull(this.users.email) || !this.validNull(this.users.phone_number)) {
      this.iMessage.warning("Vui lòng nhập vào đầy đủ thông tin.", "E-Shopper", { timeOut: 3000 });
    } else {
      if (!this.validEmail(this.users.email) || !this.validPhoneNumber(this.users.phone_number)) {

      } else {
        let jsonUser = this.createJsonUser(this.id);
        this.iService.insertObject("account/update", jsonUser).subscribe(result => {
          if (result) {
            this.iMessage.success("Lưu thành công.", "E-Shopper", {
              timeOut: 3000
            });
          }
        });
      }
    }
  }
}
