import { Component, OnInit } from '@angular/core';
import { Item } from '../module/item';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Utils } from '../utils/common/EShop';
import { Router, ActivatedRoute } from '@angular/router';
import { FPT } from '../utils/config/service';
import { CoreService } from '../service/core.service';
import { PATH_URI } from '../utils/common/path';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  // Create variable
  IMAGES_SERVICE = FPT.API_SERVICE;
  item: Item[] = [];
  totalBuy: number = 0;
  trans = Utils.TRANS;
  radioSelected: string;
  radioSelectedValue: string;
  isShowFeeShip = false;
  isPayments = false;
  isClickPayemnt = false;
  accounInfo: any;
  fullnameOrder: string;
  phoneOrder: string;
  emailOrder: string;
  adressOrder: string;
  message = "";
  jsonBuy: any = [];
  jsonOrderInfo: any;
  moneyShipper = 0;
  jsonIdAndQuantity: any = [];
  isFlatQuantity = false;

  /**
   * Method get selected item
   */
  getSelectedItem() {
    this.isShowFeeShip = false;
    let radioSel = this.trans.find(Item => Item.value === this.radioSelected);
    this.radioSelectedValue = JSON.stringify(radioSel);
    let payment = JSON.parse(this.radioSelectedValue);
    if (payment.value == 1) {
      this.isShowFeeShip = true;
      this.moneyShipper = 30000;
<<<<<<< HEAD
    }
    if (this.isFlatQuantity) {
      this.moneyShipper = 0;
=======
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
    }
  }

  /**
   * Method change area
   */
  changeArea($event) {
    this.moneyShipper = 30000;
    let order = JSON.parse(localStorage.getItem('cart_buy'));
    if (this.totalBuy >= 3000000 || order.length >= 2) {
      this.moneyShipper = 0;
      return;
    }
    let area = $event.target.value;
    if (area == '0') {
      this.moneyShipper = 30000;
    } else if (area == '1') {
      this.moneyShipper = 70000;
    }
    if(this.isFlatQuantity) {
      this.moneyShipper = 0;
    }
  }

  /**
   * Method payment order
   */
  paymentOrder() {
    this.findAccInfo();
    this.isClickPayemnt = true;
  }

  /**
   * Method order
   */
  order() {
    this.validQuantityAgain();
  }

  /**
   * Method valid list quantity of product 
   */
  validQuantityAgain() {
    let isFlatSave = false;
    // Case payment from store
    if (this.radioSelected == "0") {
      isFlatSave = true;
    }
    // Case payment from your home
    if (this.radioSelected == "1") {
      this.message = "";
      let isValid = this.validInstanceOrder();

      if (!isValid && this.changeArea) {
        this.message = "Vui lòng điền đầy đủ thông tin.";
      } else {
        isFlatSave = true;
      }
    }
    // Case payment online
    if (this.radioSelected == "2") {

    }
    // Save to DB
    if (isFlatSave) {
      // Check again quantity in DB
      let ids = "";
      let idAndQuantity = [];
      let isFlat = true;
      this.item.forEach(item => {
        ids += item.product.id + ",";
        idAndQuantity.push({
          "id": item.product.id,
          "quantity": item.quantity,
          "name": item.product.name
        });
      });
      ids = ids.substr(0, ids.length - 1);
      this.iService.findByID("order-details/valid-quantity", ids).subscribe(result => {
        if (result) {
          idAndQuantity.forEach(item => {
            result.forEach(element => {
              if (item.id == element.id) {
                if (item.quantity > element.inventory) {
                  isFlat = false;
                  this.toastr.error("Sản phẩm " + item.name + " không đủ trong kho, vui lòng thử lại!", "E-Shopper", { timeOut: 3000 });
                  return;
                }
              }
            });
          });
          if (isFlat) {
            this.createJsonOrder();
            this.insertOrderInfo(PATH_URI.ORDER, this.jsonOrderInfo);
          }
        }
      });
    }
  }
  /**
   * Method create itemF json
   * 
   * @param orderId 
   */
  createJsonOrderDetails(orderId: any) {
    if (this.item != null) {
      this.item.forEach(item => {
        this.jsonBuy.push({
          "order_id": orderId,
          "product_id": item.product.id,
          "color": item.color,
          "size": item.site,
          "price_per_unit": item.product.price_per_unit,
          "old_price": item.product.old_price,
          "quantity": item.quantity
        });
        this.jsonIdAndQuantity.push({
          "id": item.product.id,
          "quantity": item.quantity
        });
      });
    }
  }

  /**
   * Method insert
   */
  insertOrderInfo(path: any, data: any) {
    this.iService.insertObject(path, data).subscribe(
      result => {
        if (result) {
          this.insertOrderDetails(result.id);
        } else {
          this.message = "Có lỗi xẩy ra trong quá trình xử lý.";
        }
      }, err => {
        this.router.navigate(['not-found']);
      }
    );
  }

  /**
   * Method insert
   */
  insertOrderDetails(orderId: any) {
    this.createJsonOrderDetails(orderId);
    this.iService.insertObject(PATH_URI.ORDER_DETAILS, this.jsonBuy).subscribe(
      result => {
        if (result) {
          if (this.jsonIdAndQuantity != null) {
            this.jsonIdAndQuantity.forEach(element => {
              this.updateQuantity(element.id, element.quantity);
            });
          }
          localStorage.removeItem("cart_buy");
          this.router.navigate(['notification']);
        } else {
          this.message = "Có lỗi xẩy ra trong quá trình xử lý.";
        }
      }, err => {
        this.router.navigate(['not-found']);
      }
    );
  }

  /**
   * Method update quantity
   */
  updateQuantity(id, quantity) {
    this.iService.findByID("product/update-inventory", id + "/" + quantity).subscribe(result => { });
  }

  /**
   * Method create json order
   */
  createJsonOrder() {
    this.jsonOrderInfo = {
      "customer_id": this.accounInfo.id,
      "name": this.fullnameOrder,
      "address": this.adressOrder,
      "email": this.emailOrder,
      "phone_number": this.phoneOrder,
      "shipping": this.radioSelected,
      "total_money": this.totalBuy,
      "money_shipping": this.moneyShipper
    }
    console.log(this.moneyShipper);
  }

  /**
   * Method find acc info
   */
  findAccInfo() {
    localStorage.removeItem("curentURL");
    let acc = localStorage.getItem("account_^((!");
    if (acc == null) {
      localStorage.setItem("curentURL", "/cart");
      this.router.navigate(['/login']);
    } else {
      this.isPayments = true;
      this.accounInfo = JSON.parse(acc);
      this.cretateInstance(this.accounInfo);
    }
  }

  /**
   * Method create install
   * 
   * @param accounInfo 
   */
  cretateInstance(accounInfo: any) {
    this.fullnameOrder = accounInfo.users[0].fullname;
    this.phoneOrder = accounInfo.users[0].phone_number;
    this.emailOrder = accounInfo.users[0].email;
    this.adressOrder = accounInfo.users[0].address;
  }

  /**
   * Method valid data order
   */
  validInstanceOrder() {
    let isFlat = true;
    if (this.fullnameOrder == null || this.fullnameOrder == "" || this.phoneOrder == null || this.phoneOrder == ""
      || this.emailOrder == null || this.emailOrder == "" || this.adressOrder == null || this.adressOrder == "") {
      isFlat = false;
    }
    return isFlat;
  }

  /**
   * Method onchange
   * @param item 
   */
  onItemChange(item) {
    this.getSelectedItem();
  }
  /**
   * Method find all order
   */
  findAllOrder() {
    let order = JSON.parse(localStorage.getItem('cart_buy'));
    if (order != null) {
      this.loadCard(order);
    }
  }

  /**
   * Method load card
   * @param order 
   */
  loadCard(order: any) {
    for (var i = 0; i < order.length; i++) {
      let item = JSON.parse(order[i]);
      // Add to list json item
      if(item.quantity >= 2) {
        this.moneyShipper = 0;
        this.isFlatQuantity = true;
      }
      this.item.push({
        product: item.product,
        quantity: item.quantity,
        color: item.color,
        date: item.date,
        site: item.site
      });
      // Method count total money buy
      this.totalBuy += item.product.price_per_unit * item.quantity;
    }
  }

  /**
   * 
   * @param id 
   */
  remove(id: string, size: string, color: string): void {
    let cart: any = JSON.parse(localStorage.getItem("cart_buy"));
    for (var i = 0; i < cart.length; i++) {
      let item: Item = JSON.parse(cart[i]);
      if (item.product.id == id && item.site == size && item.color == color) {
        cart.splice(i, 1);
        break;
      }
    }
    localStorage.setItem("cart_buy", JSON.stringify(cart));
    this.showNotificeModel();
    window.location.reload();
  }

  /**
   * Method remove order
   * 
   * @param id 
   * @param size 
   * @param color 
   */
  removeOrder(id: string, size: string, color: string) {
    this.remove(id, size, color);
  }

  /**
   *  Show noticatice
   */
  showNotificeModel() {
    this.toastr.warning("Xóa sản phẩm khỏi giỏ hàng thành công.", "E-Shopper", {
      timeOut: 3000
    });
  }

  init() {
    localStorage.removeItem("curentURL");
    let acc = localStorage.getItem("account_^((!");
    if (acc == null) {
      this.toastr.info("Đăng nhập trước khi thanh toán!", "E-Shopper", {
        timeOut: 3000
      });
    } 
    this.radioSelected = "0";
    this.findAllOrder();
  }

  constructor(private iService: CoreService,
    private activeRouter: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private location: Location) { }

  ngOnInit() {
    console.log("aa");
    this.init();
  }

}
