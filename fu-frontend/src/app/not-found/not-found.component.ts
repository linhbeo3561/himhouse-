import { Component, OnInit } from '@angular/core';
import { FPT } from '../utils/config/service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  // Create variable
  IMAGES_SERVICE = FPT.API_SERVICE;
  
  constructor() { }

  ngOnInit() {
  }

}
