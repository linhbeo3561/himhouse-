import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaerV2Component } from './heaer-v2.component';

describe('HeaerV2Component', () => {
  let component: HeaerV2Component;
  let fixture: ComponentFixture<HeaerV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaerV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaerV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
