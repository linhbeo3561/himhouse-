import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Utils } from '../utils/common/EShop';
import { Router } from '@angular/router';
import { FPT } from '../utils/config/service';
import { CoreService } from '../service/core.service';
import { PATH_URI } from '../utils/common/path';

@Component({
  selector: 'app-heaer-v2',
  templateUrl: './heaer-v2.component.html',
  styleUrls: ['./heaer-v2.component.css']
})
export class HeaerV2Component implements OnInit {

  @Output() nameSearchFilter = new EventEmitter();
  IMAGES_SERVICE = FPT.API_SERVICE;
  accountName: string;
  isFlatLogin = false;
  role_id = Utils.ROLE.GUEST;
  pathManager = "product";
  orderLength = 0;
  id = "";
  nameSearch: string;
  listDataGender: any;
  listDataCategory: any;
  listDataProduct: any;

  /**
   * Filter by name
   */
  filterByName() {
    if (this.nameSearch != null) {
      this.nameSearchFilter.emit(this.nameSearch);
      this.router.navigate(['/search-result/' + this.nameSearch]);
    }
  }


  /**
 * Method redirect
 * @param id 
 */
  redirect(id) {
    this.iService.findByID("product/gender", id).subscribe(
      result => {
        if (result) {
          let arrId = "";
          result.forEach(element => {
            arrId += element.id + ",";
          });
          let pathRedirect = `/category/${arrId.substr(0, arrId.length - 1)}?gender=${id}`;
          // this.router.navigate([pathRedirect]);
          window.open(pathRedirect);
        }
      }
    );
  }

  /**
   * Method init
   */
  init() {
    let acc = localStorage.getItem("account_^((!");
    let accountName = localStorage.getItem("acc_name_!@!@");
    if (accountName != null) {
      this.isFlatLogin = true;
      this.accountName = accountName;
      this.role_id = JSON.parse(acc).role_id;
      if(this.role_id == -1) {
        this.accountName = JSON.parse(acc).users[0].fullname;
      }
      this.id = JSON.parse(acc).id;
      this.findRole(this.role_id);
    }
    this.findAllGenderAndCate();
    this.findAllProduct();
  }

  /**
   * Method find all cate and gender
   */
  findAllGenderAndCate() {
    this.iService.findAll("gender").subscribe(
      result => {
        if (result) {
          this.listDataGender = result;
          result.forEach(element => {
            this.iService.findByID("gender", element.id).subscribe(
              cate => {
                if (cate) {
                  element.categories = cate;
                }
              }
            );
          });
        }
      }
    );
  }

  /**
   * Method find all category by gender id
   * @param genderID 
   */
  findAllCategoriesByGenderID(genderID) {
    this.iService.findByID("gender", genderID).subscribe(
      result => {
        if (result) {
          this.listDataCategory = result;
        }
      }
    );
  }
  /**
   * Method find all product
  */
  findAllProduct() {
    this.iService.findAll(PATH_URI.PRODUCT).subscribe(
      product => {
        if (product != null) {
          this.listDataProduct = product;
        }
      }, err => { });
  }

  /**
   * Method find role and redirect
   */
  findRole(role: any) {
    let pathRedirect = 'product';
    switch (role) {
      case Utils.ROLE.ADMIN:
        pathRedirect = 'account-manager';
        break;
      case Utils.ROLE.GUEST:
        pathRedirect = 'product';
        break;
      case Utils.ROLE.MANAGER:
        pathRedirect = 'product-manager'
        break;
      case Utils.ROLE.STAFF:
        pathRedirect = 'order-manager'
        break;
    }
    this.pathManager = pathRedirect;
  }

  constructor(private router: Router, private iService: CoreService) { }

  /**
  * Method logout
  */
  logout() {
    localStorage.clear();
    // localStorage.removeItem("account_^((!");
    // localStorage.removeItem("acc_name_!@!@");
    this.router.navigate(['/login']);
  }

  ngOnInit() {
    this.init();
  }

}
