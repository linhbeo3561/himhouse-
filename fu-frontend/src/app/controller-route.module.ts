import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from "../app/product/product.component";
import { ProductDetailsComponent } from './product-details/product-details.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { CartComponent } from './cart/cart.component';
import { AccountManagerComponent } from './account-manager/account-manager.component';
import { OrderManagerComponent } from './order-manager/order-manager.component';
import { ProductManagerComponent } from './product-manager/product-manager.component';
import { ChartManagerComponent } from './chart-manager/chart-manager.component';
import { BuyProductComponent } from './buy-product/buy-product.component';
import { NotificeComponent } from './notifice/notifice.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { OrderDetailsComponent } from './service/order-details/order-details.component';
import { CategoryProductComponent } from './category-product/category-product.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProviderComponent } from './provider/provider.component';
import { SearchResultComponent } from './search-result/search-result.component';

// Variable config routers.
const router: Routes = [
  {
    path: '',
    redirectTo: '/homepage',
    pathMatch: 'full'
  },
  {
    path: 'category/:id',
    component: ProductComponent
  },
  {
    path: 'product/:id',
    component: ProductDetailsComponent
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'account-manager',
    component: AccountManagerComponent
  },
  {
    path: 'order-manager',
    component: OrderManagerComponent
  },
  {
    path: 'product-manager',
    component: ProductManagerComponent
  },
  {
    path: 'chart-manager',
    component: ChartManagerComponent
  },
  {
    path: 'buy-product',
    component: BuyProductComponent
  },
  {
    path: 'notification',
    component: NotificeComponent
  },
  {
    path: 'order-history/:id',
    component: OrderHistoryComponent
  },
  {
    path: 'order-manager/:id',
    component: OrderDetailsComponent
  },
  {
    path: 'categories-manager',
    component: CategoryProductComponent
  },
  {
    path: 'account-info/:id',
    component: AccountInfoComponent
  },
  {
    path: 'homepage',
    component: HomepageComponent
  },
  {
    path: 'provider',
    component: ProviderComponent
  },
  {
    path: 'search-result/:name',
    component: SearchResultComponent
  }
];

@NgModule({
  imports: [
    // CommonModule
    RouterModule.forRoot(router)
  ],
  // declarations: []
  exports: [RouterModule]
})

export class ControllerRouteModule { }
