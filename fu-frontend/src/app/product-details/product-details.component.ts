import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Product } from '../module/product';
import { Category } from '../module/category';
import { Type } from '../module/productType';
import { CoreService } from "../service/core.service";
import { PATH_URI } from "../utils/common/path";
import { Item } from '../module/item';
import { ToastrService } from 'ngx-toastr';
import { FPT } from '../utils/config/service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  // Create variable
  IMAGES_SERVICE = FPT.API_SERVICE;
  product: Product;
  listDataProductTop4: Product[];
  id: string;
  quantity: number;
  jsonBuy: any = [];
  itemsBuy: Item[] = [];
  size: string[] = [];
  color: string[] = [];
  sizeSelected = "-1";
  colorSelected = "-1";
  isSelectedSize = true;
  isSelectedColor = true;
  notificeModel = false;
  imagePreview: string;
  firstImage: string;

  constructor(
    private iService: CoreService,
    private router: ActivatedRoute,
    private routerParam: Router,
    private toastr: ToastrService
  ) {
    window.scrollTo(0, 0);
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  /**
   * Method create rent details
   */
  createJsonBuyDetails(orderId: string) {
    if (this.itemsBuy != null) {
      this.itemsBuy.forEach(item => {
        this.jsonBuy.push({
          "order_id": orderId,
          "product_id": item.product.id,
          "quantity": item.quantity
        });
      });
    }
  }

  /**
   * Method do valid combobox
   * @param data 
   */
  doValidCombobox(data: any) {
    let isFlat = true;
    if (data == null || data == "" || data == NaN || data == undefined || data == "-1") {
      isFlat = false;
    }
    return isFlat;
  }

  /**
   * Method find student by ID
   */
  addToCard(id: any, payment: string) {
    this.isSelectedSize = this.doValidCombobox(this.sizeSelected);
    this.isSelectedColor = this.doValidCombobox(this.colorSelected);
    if (this.isSelectedColor && this.isSelectedSize) {
      if (payment != 'payment') {
        this.iService.findByID(PATH_URI.PRODUCT, id).subscribe(
          product_result => {
            if (product_result && product_result != null) {
              var item: Item = {
                product: product_result,
                quantity: 0 + this.quantity,
                site: this.sizeSelected,
                color: this.colorSelected,
                date: new Date().toDateString()
              };
              this.product = product_result;
              // Buy
              if (localStorage.getItem('cart_buy') == null) {
                let cart: any = [];
                cart.push(JSON.stringify(item));
                localStorage.setItem('cart_buy', JSON.stringify(cart));
              } else {
                let cart: any = JSON.parse(localStorage.getItem('cart_buy'));
                let index: number = -1;
                for (var i = 0; i < cart.length; i++) {
                  let item: Item = JSON.parse(cart[i]);
                  // Check exit in localStorage
                  if (item.product.id == id && item.site == this.sizeSelected && item.color == this.colorSelected) {
                    index = i;
                    break;
                  }
                }
                if (index == -1) {
                  cart.push(JSON.stringify(item));
                  localStorage.setItem('cart_buy', JSON.stringify(cart));
                } else {
                  let item: Item = JSON.parse(cart[index]);
                  // let quantity_buy = +item.quantity;
                  item.quantity = this.quantity;
                  cart[index] = JSON.stringify(item);
                  localStorage.setItem("cart_buy", JSON.stringify(cart));
                }
              }
            }
            this.showNotificeModel();
          }
        );
      } else
        if (payment == 'payment') {
          localStorage.removeItem("cart_product");
          this.iService.findByID(PATH_URI.PRODUCT, id).subscribe(
            product_result => {
              if (product_result && product_result != null) {
                var item: Item = {
                  product: product_result,
                  quantity: 0 + this.quantity,
                  site: this.sizeSelected,
                  color: this.colorSelected,
                  date: new Date().toDateString()
                };
                let cart: any = [];
                cart.push(JSON.stringify(item));
                localStorage.setItem('cart_product', JSON.stringify(cart));
              }
            });
          this.routerParam.navigate(["buy-product"]);
        }
    } else {
      this.isSelectedSize = false;
      this.isSelectedColor = false;
    }

  }

  /**
   * Method find param from card
   */
  findParamFromCard() {
    let size = this.router.snapshot.queryParamMap.get('size');
    let color = this.router.snapshot.queryParamMap.get('color');
    let quantity = this.router.snapshot.queryParamMap.get('quantity');
    if (size != null) {
      this.sizeSelected = size;
    }
    if (color != null) {
      this.colorSelected = color;
    }
    if (quantity != null) {
      this.quantity = +quantity;
    }
  }

  /**
   *  Show noticatice
   */
  showNotificeModel() {
    this.toastr.success("Thêm vào giỏ hàng thành công.", "E-Shopper", {
      timeOut: 3000
    });
  }


  /**
   * Valid quantity
   */
  validQuantity() {
    if (this.quantity <= 0) {
      this.quantity = 1;
    }
    if (this.quantity > this.product.inventory) {
      this.toastr.warning("Số lượng sản phẩm còn lại trong kho không đủ. ", "E-Shopper", {
        timeOut: 3000
      })
      this.quantity = this.product.inventory;
    }
  }

  /**
   * Find id in url
   */
  findIdInURL() {
    let id = this.router.snapshot.paramMap.get('id');
    if (id == null || id == '' || id == "" || isNaN(+id)) {
      this.routerParam['not-found'];
    } else {
      this.id = id;
      this.findProductByID(id);
    }
  }

  /**
   * Method find product by id
   * @param id
   */
  findProductByID(id: any) {
    this.iService.findByID(PATH_URI.PRODUCT, id).subscribe(
      product => {
        this.product = product;
        this.imagePreview = product.image;
        this.firstImage = this.imagePreview;
        let size = this.product.size;
        let color = this.product.color;
        if (size.length > 0) {
          this.size = size.split(",");
        }
        if (color.length > 0) {
          this.color = color.split(",");
        }
      });
  }

  /**
   * 
   * @param path 
   */
  updateImage(path) {
    this.imagePreview = path;
  }
  /**
  * Method find all product
  */
  findAllProduct() {
    this.iService.findAll(PATH_URI.PRODUCT).subscribe(
      product => {
        if (product != null) {
          this.listDataProductTop4 = product.slice(1, 5);
        }
      });
  }

  /**
   * Method init
   */
  init() {
    this.quantity = 1;
    this.findIdInURL();
    this.findAllProduct();
    this.findParamFromCard();
  }

  /**
   * Method change color
   * @param $event 
   */
  onChangeColor($event) {
    console.log($event.target.value);
  }

  ngOnInit() {
    this.init();
  }

}
