import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utils } from '../utils/common/EShop';
import { StatusBuy } from '../utils/common/StatusOrder';
import { CoreService } from '../service/core.service';

@Component({
  selector: 'app-order-manager',
  templateUrl: './order-manager.component.html',
  styleUrls: ['./order-manager.component.css']
})
export class OrderManagerComponent implements OnInit {

  list_data_shipper: any;
  accontInfo: any;
  status_buy: string;
  list_data_order: any;
  STATUSBUYCOMMON = StatusBuy;
  config: any;
  shipperName: any;

  constructor(private router: Router, private iService: CoreService) {
    this.configPagging()
  }

  /**
   * Config pagging
   */
  configPagging() {
    this.config = {
      itemsPerPage: 7,
      currentPage: 1,
      totalItems: this.list_data_order
    };
  }

  /**
   * Find all list shipper
   */
  findAllShipper() {
    this.iService.findByID("account/find-by-role", -1).subscribe(
      result => {
        if (result) {
<<<<<<< HEAD
          console.log(result);
=======
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
          this.list_data_shipper = result;
        }
      }
    );
  }
  /**
   * Method find all
   */
  findAll() {
    this.iService.findAll("order/").subscribe(
      result => {
        if (result) {
          this.list_data_order = result;
        }
      }
    );
  }

  /**
   * Method find all by status
   * 
   * @param status 
   */
  findAllByStatus(status: any) {
    this.iService.findByID("order/status", status).subscribe(
      result => {
        if (result) {
          this.list_data_order = result;
        }
      }
    );
  }

  /**
   * Method search by status
   */
  searchByShipper() {
    let searchName = this.shipperName;
    if (searchName == undefined || searchName == "-1") {
      this.findAll();
    } else {
      this.findAllByShipperName(searchName);
    }
  }

  /**
  * Method find all by status
  * 
  * @param status 
  */
  findAllByShipperName(name: any) {
    this.iService.findByID("order/find-order-by-shipper", name).subscribe(
      result => {
        if (result) {
          this.list_data_order = result;
        }
      }
    );
  }

  /**
   * Function change status
   */
  searchByStatus() {
    let search_buy = this.status_buy;
    if (search_buy == undefined || search_buy == "-1") {
      this.findAll();
    } else {
      this.findAllByStatus(search_buy);
    }

  }

  /**
   * 
   * @param event 
   */
  pageChanged(event) {
    this.config.currentPage = event;
  }

  /**
   * Method get color by status
   * @param stat 
   */
  getColorStatusBuy(stat) {
    if (stat == 0) {
      return { "background-color": "green", "color": "white" }
    }
    else if (stat == 1) {
      return { "background-color": "blue", "color": "white" }
    }
    else if (stat == 2) {
      return { "background-color": "red", "color": "white" }
    }
    else if (stat == 3) {
      return { "background-color": "green", "color": "white" }
    }
    else if (stat == null) {
      return { "background-color": "darkgrey" }
    }
  }

  /**
   * Method find token from localstorage
   */
  findToken() {
    let acc = localStorage.getItem("account_^((!");
    if (acc != null) {
      this.accontInfo = JSON.parse(acc);
      if (this.accontInfo.role_id == Utils.ROLE.GUEST || this.accontInfo.role_id == Utils.ROLE.ADMIN) {
        this.logout();
      }
    } else {
      this.logout();
    }
  }
  /**
   * Method logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  init() {
    this.findToken();
    this.findAll();
    this.findAllShipper();
  }

  ngOnInit() {
    this.init();
  }

}
