export const PATH_URI = {
    PRODUCT: "product",
    CREATE_PRODUCT: "product/add",
    PRODUCT_TYPE: "product/type",
    TOP_4: "product/top4/new",
    CATEGORY: "category",
    GENDER_CATE: "gender-cate",
    CATEGORY_CREATE: "category/add",
    ACCOUNT: "account",
    SUOC: "/",
    TYPE: "type",
    FIND_ORDER_SHIPPER: "order/find-order-by-shipper",
    ORDER: "order",
    ORDER_DETAILS: "order-details"
};
