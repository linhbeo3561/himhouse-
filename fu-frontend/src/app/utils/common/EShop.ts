export const Utils = {
    ROLE: {
        "ADMIN": 3,
        "MANAGER": 2,
        "STAFF": 1,
        "GUEST": 0,
        "SHIPPER": -1
    },

    TRANS: [
        {
            "name": "Thanh toán và nhận hàng tại Shop.",
            "value": "0"
        },
        {
            "name": "Thanh toán khi nhận hàng.",
            "value": "1"
        },
        {
            "name": "Thanh toán online.",
            "value": "2"
        }
    ]
}