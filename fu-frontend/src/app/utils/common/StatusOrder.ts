export const StatusBuy = {
    null: "Không đặt hàng mua.",
    0: "Đang chờ xử lý.",
    1: "Đã chấp nhận đơn hàng.",
    2: "Đang ship.",
    3: "Đơn hàng đã thanh toán",
    4: "Đơn hàng đã thanh toán 50% giá trị",
    5: "Hủy đơn hàng",
    6: "Đơn hàng đã hoàn thành"
};