import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Product } from '../module/product';
import { Category } from '../module/category';
import { Type } from '../module/productType';
import { CoreService } from "../service/core.service";
import { PATH_URI } from "../utils/common/path";
import { FPT } from '../utils/config/service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  // Create variable
  IMAGES_SERVICE = FPT.API_SERVICE;
  listDataCategory: Category[];
  listDataType: Type[];

  constructor(
    private iService: CoreService
  ) { }

  /**
   * Method find all category
   */
  findAllCategory() {
    this.iService.findAll(PATH_URI.CATEGORY).subscribe(
      category => {
        if (category != null) {
          this.listDataCategory = category;
        }
      });
  }

  /**
   * Method find all category
   */
  findAllType() {
    this.iService.findAll(PATH_URI.TYPE).subscribe(
      type => {
        if (type != null) {
          this.listDataType = type;
        }
      });
  }

  init() {
    this.findAllCategory();
    this.findAllType();
  }

  ngOnInit() {
    this.init();
  }

}
