import { Component, OnInit } from '@angular/core';
import { CoreService } from '../service/core.service';
import { OrderBuy } from '../module/orderBuy';
import { PATH_URI } from '../utils/common/path';
import { Router, ActivatedRoute } from '@angular/router';
import { StatusBuy } from '../utils/common/StatusOrder';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  orderIdSelected: string;
  order: OrderBuy[];
  details: any;
  STATUSBUYCOMMON = StatusBuy;
  total_buy = 0;
  list_data_order_details: any;

  constructor(
    private iService: CoreService,
    private router: ActivatedRoute,
    private iMessage: ToastrService,
    private routerParam: Router) { }

  /**
   * Method chagne status order
   * 
   * @param item 
   */
  changeStatusOrder(item) {
    if (confirm("Bạn có chắc hủy order")) {
      this.iService.findByID("order/update-status-by-order-id", item.id + "/5").subscribe(result => {
        this.iMessage.success("Bạn đã hủy đơn hàng thành công", "E-Shopee", { timeOut: 30000 });
        this.findId();
      });
    }
  }
  /**
   * Method find all by cus id
   * 
   * @param id 
   */
  findByCusId(id: any) {
    this.iService.findByID(PATH_URI.ORDER, id).subscribe(
      result => {
        if (result) {
          this.order = result;
          console.log(this.order['order_details']);
        }
      }, err => {
        console.log(err);
      }
    );
  }

  /**
   * Method find all order by shipper name
   * @param name 
   */
  findOrderByShipperName(name: any) {
    this.iService.findByID(PATH_URI.FIND_ORDER_SHIPPER, name).subscribe(
      result => {
        if (result) {
          this.order = result;
        }
      }, err => {
        console.log(err);
      }
    );
  }

  /**
   * Method get color by status
   * @param stat 
   */
  getColorStatusBuy(status: any) {
    if (status == 0) {
      return { "background-color": "green", "color": "white" }
    }
    else if (status == 1) {
      return { "background-color": "blue", "color": "white" }
    }
    else if (status == 2) {
      return { "background-color": "red", "color": "white" }
    }
    else if (status == 3) {
      return { "background-color": "green", "color": "white" }
    }
    else if (status == null) {
      return { "background-color": "darkgrey" }
    }
  }

  /**
   * Method find order details by id
   */
  findDetails(id: any) {
    this.total_buy = 0;
    this.iService.findByID("order/customer", id).subscribe(
      result => {
        if (result) {
          this.list_data_order_details = result;
          console.log(this.list_data_order_details);
          result.forEach(element => {
            this.total_buy += element.price_per_unit * element.quantity;
          });
        }
      }
    );
  }
  /**
   * Method find id
   */
  findId() {
    let id = this.router.snapshot.paramMap.get('id');
    if (id == null || id == '' || id == "" || isNaN(+id)) {
      this.findOrderByShipperName(id);
    } else {
      this.findByCusId(id);
    }
  }

  init() {
    this.findId();
  }

  ngOnInit() {
    this.init();
  }

}
