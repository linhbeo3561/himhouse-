import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { HeaerV2Component } from './heaer-v2/heaer-v2.component';
import { ControllerRouteModule } from './controller-route.module';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { CartComponent } from './cart/cart.component';
import { HeaderManagerComponent } from './header-manager/header-manager.component';
import { AccountManagerComponent } from './account-manager/account-manager.component';
import { OrderManagerComponent } from './order-manager/order-manager.component';
import { ProductManagerComponent } from './product-manager/product-manager.component';
import { ChartManagerComponent } from './chart-manager/chart-manager.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BuyProductComponent } from './buy-product/buy-product.component';
import { NotificeComponent } from './notifice/notifice.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { OrderDetailsComponent } from './service/order-details/order-details.component';
import { FileUploadModule } from 'ng2-file-upload';
import { CategoryProductComponent } from './category-product/category-product.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProviderComponent } from './provider/provider.component';
import { SearchResultComponent } from './search-result/search-result.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    ProductDetailsComponent,
    NotFoundComponent,
    LoginComponent,
    HeaerV2Component,
    CartComponent,
    HeaderManagerComponent,
    AccountManagerComponent,
    OrderManagerComponent,
    ProductManagerComponent,
    ChartManagerComponent,
    BuyProductComponent,
    NotificeComponent,
    OrderHistoryComponent,
    OrderDetailsComponent,
    CategoryProductComponent,
    AccountInfoComponent,
    HomepageComponent,
    ProviderComponent,
    SearchResultComponent
  ],
  imports: [
    BrowserModule,
    ControllerRouteModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    FileUploadModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
