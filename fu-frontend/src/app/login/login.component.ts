<<<<<<< HEAD
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CoreService } from "../service/core.service";
import { NgForm } from '@angular/forms';
import { AccountService } from '../service/account.service';
import { Info } from '../module/info';
import { Account } from '../module/account';
import { User } from '../module/user';
import { isEmpty } from 'rxjs/operators';
import * as EShop from '../utils/common/EShop';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Create variable acc
  username: string;
  password: string;
  message: string;
  messageReg: string;
  confirmPassword: string;
  accounts: Account = new Account();
  users: User = new User();
  info: Info = new Info();

  constructor(
    private accountService: AccountService,
    private router: Router,
    private iMess: ToastrService,
    private iService: CoreService
  ) {
    window.scrollTo(0, 0);
  }

  /**
   * Create data login
   */
  sigInAccount() {
    if (this.username == null || this.username == '' || this.username == "" || this.password == null || this.password == '' || this.password == "") {
      // this.message = "Vui lòng nhập vào tên đăng nhập hoặc mật khẩu.";
      this.iMess.warning("Vui lòng nhập vào tên đăng nhập hoặc mật khẩu.", "E-Shopper", { timeOut: 3000 });
    } else {
      this.doLogin();
    }
  }

  /**
   * Valid null account
   * @param acc 
   */
  validNullAccount(acc: Account) {
    let isFlat = true;
    if (acc.username == "" || acc.username == null || acc.password == "" || acc.password == null) {
      isFlat = false;
    }
    return isFlat;
  }

  /**
   * Valid user
   * @param user 
   */
  validNullUser(user: User) {
    console.log(user);
    let isFlat = true;
    if (user.fullname == "" || user.fullname == null || user.address == "" || user.address == null || user.phone_number == "" || user.phone_number == null || user.email == "" || user.email == null) {
      isFlat = false;
    }
    return isFlat;
  }

  regesterAccount() {
    if (!this.validNullAccount(this.accounts) || !this.validNullUser(this.users)) {
      this.iMess.warning("Bạn vui lòng nhập đầy đủ thông tin.", "E-Shopper", { timeOut: 3000 });
    } else {
      if (this.confirmPassword != this.accounts.password) {
        this.iMess.warning("Vui lòng nhập đúng nhập lại mật khẩu.", "E-Shopper", { timeOut: 3000 });
      } else {
        this.iService.findByID("account/find-by-uname", this.accounts.username).subscribe(
          result => {
            if (result && result.length > 0) {
              this.iMess.warning("Tên đăng nhập đã tồn tại.", "E-Shopper", { timeOut: 3000 });
            } else {
              if (!this.validUname(this.accounts.username) || !this.validPhoneNumber(this.users.phone_number) || !this.validEmail(this.users.email)) { }
              else {
                this.info.accounts = this.accounts;
                this.info.users = this.users;
                this.accountService.accountSignup(this.info).subscribe(
                  result => {
                    if (result) {
                      this.iMess.success("Bạn đã đăng ký thành công.", "E-Shopper", { timeOut: 3000 });
                    }
                  }, err => {
                    this.iMess.error("Đã có lỗi xẩy ra trong quá trình xử lý.", "E-Shopper", { timeOut: 3000 });
                  }
                );
              }
            }
          }
        );
      }
    }
  }

  /**
   * Method check length uname
   * @param uname 
   */
  validUname(uname) {
    if (uname.length < 6 || uname.length > 20) {
      this.iMess.warning("Vui lòng nhập vào tên đăng nhập từ 6 - 20 ký tự.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }

  /**
   * Method check phone number
   * @param phone 
   */
  validPhoneNumber(phone) {
    var phoneno = /(09|01[2|6|8|9])+([0-9]{8})\b/g;
    if (!phoneno.test(phone)) {
      this.iMess.warning("Vui lòng nhập vào đúng số điện thoại.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }

  /**
   * Method valid email
   * @param email 
   */
  validEmail(email) {
    var patternEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!patternEmail.test(email)) {
      this.iMess.warning("Vui lòng nhập vào địa chỉ email.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }
  /**
   *  Method do login sucessful
   */
  doLoginSuccess(acc: any) {
    localStorage.setItem("account_^((!", JSON.stringify(acc));
    localStorage.setItem("acc_name_!@!@", acc.username);
    let curentURL = localStorage.getItem("curentURL");
    if (curentURL != null) {
      localStorage.removeItem("curentURL");
      this.router.navigate([curentURL]);
    } else {
      this.findRole(acc.role_id);
    }
  }

  /**
   * Method find role and redirect
   */
  findRole(role: any) {
    let pathRedirect = 'homepage';
    switch (role) {
      case EShop.Utils.ROLE.ADMIN:
        pathRedirect = 'account-manager'
        break;
      case EShop.Utils.ROLE.GUEST:
        pathRedirect = 'homepage';
        break;
      case EShop.Utils.ROLE.MANAGER:
        pathRedirect = 'product-manager'
        break;
      case EShop.Utils.ROLE.STAFF:
        pathRedirect = 'order-manager'
        break;
    }
    this.router.navigate([pathRedirect]);
  }

  /**
* Method do login
*/
  doLogin() {
    this.accountService.doLogin(this.username, this.password).subscribe(
      reuslt => {
        if (reuslt) {
          this.doLoginSuccess(reuslt);
        } else {
          this.message = "Tên tài khoản hoặc mật khẩu không đúng?";
        }
      }, err => {

      }
    );
  }

  ngOnInit() {
  }

}
=======
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CoreService } from "../service/core.service";
import { NgForm } from '@angular/forms';
import { AccountService } from '../service/account.service';
import { Info } from '../module/info';
import { Account } from '../module/account';
import { User } from '../module/user';
import { isEmpty } from 'rxjs/operators';
import * as EShop from '../utils/common/EShop';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Create variable acc
  username: string;
  password: string;
  message: string;
  messageReg: string;
  confirmPassword: string;
  accounts: Account = new Account();
  users: User = new User();
  info: Info = new Info();

  constructor(
    private accountService: AccountService,
    private router: Router,
    private iMess: ToastrService,
    private iService: CoreService
  ) {
    window.scrollTo(0, 0);
  }

  /**
   * Create data login
   */
  sigInAccount() {
    if (this.username == null || this.username == '' || this.username == "" || this.password == null || this.password == '' || this.password == "") {
      // this.message = "Vui lòng nhập vào tên đăng nhập hoặc mật khẩu.";
      this.iMess.warning("Vui lòng nhập vào tên đăng nhập hoặc mật khẩu.", "E-Shopper", { timeOut: 3000 });
    } else {
      this.doLogin();
    }
  }

  /**
   * Valid null account
   * @param acc 
   */
  validNullAccount(acc: Account) {
    let isFlat = true;
    if (acc.username == "" || acc.username == null || acc.password == "" || acc.password == null) {
      isFlat = false;
    }
    return isFlat;
  }

  /**
   * Valid user
   * @param user 
   */
  validNullUser(user: User) {
    console.log(user);
    let isFlat = true;
    if (user.fullname == "" || user.fullname == null || user.address == "" || user.address == null || user.phone_number == "" || user.phone_number == null || user.email == "" || user.email == null) {
      isFlat = false;
    }
    return isFlat;
  }

  regesterAccount() {
    if (!this.validNullAccount(this.accounts) || !this.validNullUser(this.users)) {
      this.iMess.warning("Bạn vui lòng nhập đầy đủ thông tin.", "E-Shopper", { timeOut: 3000 });
    } else {
      if (this.confirmPassword != this.accounts.password) {
        this.iMess.warning("Vui lòng nhập đúng nhập lại mật khẩu.", "E-Shopper", { timeOut: 3000 });
      } else {
        this.iService.findByID("account/find-by-uname", this.accounts.username).subscribe(
          result => {
            if (result && result.length > 0) {
              this.iMess.warning("Tên đăng nhập đã tồn tại.", "E-Shopper", { timeOut: 3000 });
            } else {
              if (!this.validUname(this.accounts.username) || !this.validPhoneNumber(this.users.phone_number) || !this.validEmail(this.users.email)) { }
              else {
                this.info.accounts = this.accounts;
                this.info.users = this.users;
                this.accountService.accountSignup(this.info).subscribe(
                  result => {
                    if (result) {
                      this.iMess.success("Bạn đã đăng ký thành công.", "E-Shopper", { timeOut: 3000 });
                    }
                  }, err => {
                    this.iMess.error("Đã có lỗi xẩy ra trong quá trình xử lý.", "E-Shopper", { timeOut: 3000 });
                  }
                );
              }
            }
          }
        );
      }
    }
  }

  /**
   * Method check length uname
   * @param uname 
   */
  validUname(uname) {
    if (uname.length < 6 || uname.length > 20) {
      this.iMess.warning("Vui lòng nhập vào tên đăng nhập từ 6 - 20 ký tự.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }

  /**
   * Method check phone number
   * @param phone 
   */
  validPhoneNumber(phone) {
    var phoneno = /(09|01[2|6|8|9])+([0-9]{8})\b/g;
    if (!phoneno.test(phone)) {
      this.iMess.warning("Vui lòng nhập vào đúng số điện thoại.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }

  /**
   * Method valid email
   * @param email 
   */
  validEmail(email) {
    var patternEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!patternEmail.test(email)) {
      this.iMess.warning("Vui lòng nhập vào địa chỉ email.", "E-Shopper", { timeOut: 3000 });
      return false;
    }
    return true;
  }
  /**
   *  Method do login sucessful
   */
  doLoginSuccess(acc: any) {
    localStorage.setItem("account_^((!", JSON.stringify(acc));
    localStorage.setItem("acc_name_!@!@", acc.username);
    let curentURL = localStorage.getItem("curentURL");
    if (curentURL != null) {
      localStorage.removeItem("curentURL");
      this.router.navigate([curentURL]);
    } else {
      this.findRole(acc.role_id);
    }
  }

  /**
   * Method find role and redirect
   */
  findRole(role: any) {
    let pathRedirect = 'homepage';
    switch (role) {
      case EShop.Utils.ROLE.ADMIN:
        pathRedirect = 'account-manager'
        break;
      case EShop.Utils.ROLE.GUEST:
        pathRedirect = 'homepage';
        break;
      case EShop.Utils.ROLE.MANAGER:
        pathRedirect = 'product-manager'
        break;
      case EShop.Utils.ROLE.STAFF:
        pathRedirect = 'order-manager'
        break;
    }
    this.router.navigate([pathRedirect]);
  }

  /**
* Method do login
*/
  doLogin() {
    this.accountService.doLogin(this.username, this.password).subscribe(
      reuslt => {
        if (reuslt) {
          this.doLoginSuccess(reuslt);
        } else {
          this.message = "Tên tài khoản hoặc mật khẩu không đúng?";
        }
      }, err => {

      }
    );
  }

  ngOnInit() {
  }

}
>>>>>>> a987dbf740fe88df325d6113c580ad6d23a704d7
