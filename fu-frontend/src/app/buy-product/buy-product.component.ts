import { Component, OnInit } from '@angular/core';
import { FPT } from '../utils/config/service';
import { Utils } from '../utils/common/EShop';
import { Item } from '../module/item';
import { ActivatedRoute, Router } from '@angular/router';
import { PATH_URI } from '../utils/common/path';
import { CoreService } from '../service/core.service';

@Component({
  selector: 'app-buy-product',
  templateUrl: './buy-product.component.html',
  styleUrls: ['./buy-product.component.css']
})
export class BuyProductComponent implements OnInit {

  // Create variable
  IMAGES_SERVICE = FPT.API_SERVICE;
  item: Item[] = [];
  totalBuy: number = 0;
  trans = Utils.TRANS;
  radioSelected: string;
  radioSelectedValue: string;
  isShowFeeShip = false;
  isPayments = false;
  isClickPayemnt = false;
  accounInfo: any;
  fullnameOrder: string;
  phoneOrder: string;
  emailOrder: string;
  adressOrder: string;
  message = "";
  jsonBuy: any = [];
  jsonOrderInfo: any;

  /**
   * Method get selected item
   */
  getSelectedItem() {
    this.isShowFeeShip = false;
    let radioSel = this.trans.find(Item => Item.value === this.radioSelected);
    this.radioSelectedValue = JSON.stringify(radioSel);
    let payment = JSON.parse(this.radioSelectedValue);
    if (payment.value == 1) {
      this.isShowFeeShip = true;
    }
  }

  /**
   * Method payment order
   */
  paymentOrder() {
    this.findAccInfo();
    this.isClickPayemnt = true;
  }

  /**
   * Method order
   */
  order() {
    let isFlatSave = false;
    // Case payment from store
    if (this.radioSelected == "0") {
      isFlatSave = true;
    }
    // Case payment from your home
    if (this.radioSelected == "1") {
      this.message = "";
      let isValid = this.validInstanceOrder();
      if (!isValid) {
        this.message = "Vui lòng điền đầy đủ thông tin.";
      } else {
        isFlatSave = true;
      }
    }
    // Case payment online
    if (this.radioSelected == "2") {

    }
    // Save to DB
    if (isFlatSave) {
      this.createJsonOrder();
      this.insertOrderInfo(PATH_URI.ORDER, this.jsonOrderInfo);
    }
  }

  /**
   * Method create itemF json
   * 
   * @param orderId 
   */
  createJsonOrderDetails(orderId: any) {
    if (this.item != null) {
      this.item.forEach(item => {
        this.jsonBuy.push({
          "order_id": orderId,
          "product_id": item.product.id,
          "color": item.color,
          "size": item.site,
          "date_exp": item.date,
          "quantity": item.quantity
        });
      });
    }
  }

  /**
   * Method insert
   */
  insertOrderInfo(path: any, data: any) {
    this.iService.insertObject(path, data).subscribe(
      result => {
        if (result) {
          this.insertOrderDetails(result.id);
        } else {
          this.message = "Có lỗi xẩy ra trong quá trình xử lý.";
        }
      }, err => {
        this.router.navigate(['not-found']);
      }
    );
  }

  /**
   * Method insert
   */
  insertOrderDetails(orderId: any) {
    this.createJsonOrderDetails(orderId);
    this.iService.insertObject(PATH_URI.ORDER_DETAILS, this.jsonBuy).subscribe(
      result => {
        if (result) {
          localStorage.removeItem("cart_product");
          this.router.navigate(['notification']);
        } else {
          this.message = "Có lỗi xẩy ra trong quá trình xử lý.";
        }
      }, err => {
        this.router.navigate(['not-found']);
      }
    );
  }

  /**
   * Method create json order
   */
  createJsonOrder() {
    this.jsonOrderInfo = {
      "customer_id": this.accounInfo.id,
      "name": this.fullnameOrder,
      "address": this.adressOrder,
      "email": this.emailOrder,
      "phone_number": this.phoneOrder,
      "shipping": this.radioSelected,
      "total_money": this.totalBuy
    }
  }

  /**
   * Method find acc info
   */
  findAccInfo() {
    localStorage.removeItem("curentURL");
    let acc = localStorage.getItem("account_^((!");
    if (acc == null) {
      localStorage.setItem("curentURL", "/cart");
      this.router.navigate(['/login']);
    } else {
      this.isPayments = true;
      this.accounInfo = JSON.parse(acc);
      this.cretateInstance(this.accounInfo);
    }
  }

  /**
   * Method create install
   * 
   * @param accounInfo 
   */
  cretateInstance(accounInfo: any) {
    this.fullnameOrder = accounInfo.users[0].fullname;
    this.phoneOrder = accounInfo.users[0].phone_number;
    this.emailOrder = accounInfo.users[0].email;
    this.adressOrder = accounInfo.users[0].address;
  }

  /**
   * Method valid data order
   */
  validInstanceOrder() {
    let isFlat = true;
    if (this.fullnameOrder == null || this.fullnameOrder == "" || this.phoneOrder == null || this.phoneOrder == ""
      || this.emailOrder == null || this.emailOrder == "" || this.adressOrder == null || this.adressOrder == "") {
      isFlat = false;
    }
    return isFlat;
  }

  /**
   * Method onchange
   * @param item 
   */
  onItemChange(item) {
    this.getSelectedItem();
  }
  /**
   * Method find all order
   */
  findAllOrder() {
    let order = JSON.parse(localStorage.getItem('cart_product'));
    if (order != null) {
      this.loadCard(order);
    }
  }

  /**
   * Method load card
   * @param order 
   */
  loadCard(order: any) {
    for (var i = 0; i < order.length; i++) {
      let item = JSON.parse(order[i]);
      // Add to list json item
      this.item.push({
        product: item.product,
        quantity: item.quantity,
        color: item.color,
        date: item.date,
        site: item.site
      });
      // Method count total money buy
      this.totalBuy += item.product.price_per_unit * item.quantity;
    }
  }

  /**
   * 
   */
  init() {
    this.radioSelected = "0";
    this.findAllOrder();
  }

  constructor(private iService: CoreService,
    private activeRouter: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.init();
  }
}
