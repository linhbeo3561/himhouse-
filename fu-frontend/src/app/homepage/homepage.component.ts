import { Component, OnInit } from '@angular/core';
import { CoreService } from '../service/core.service';
import { Router } from '@angular/router';
import { PATH_URI } from '../utils/common/path';
import { FPT } from '../utils/config/service';
import { Utils } from '../utils/common/EShop';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  // Create variable
  IMAGES_SERVICE = FPT.API_SERVICE;
  listDataGender: any;
  listDataProduct: any;
  accountName: string;
  isFlatLogin = false;
  role_id = Utils.ROLE.GUEST;
  pathManager = "product";
  orderLength = 0;
  id = "";
  nameSearchFilter: string;

  constructor(
    private iService: CoreService,
    private router: Router,
    private productService: ProductService,
  ) {
    window.scrollTo(0, 0);
  }

  /**
   * Method redirect
   * @param id 
   */
  redirect(id) {
    this.iService.findByID("product/gender", id).subscribe(
      result => {
        if (result) {
          let arrId = "";
          result.forEach(element => {
            arrId += element.id + ",";
          });
          let pathRedirect = `/category/${arrId.substr(0, arrId.length - 1)}?gender=${id}`;
          this.router.navigate([pathRedirect]);
        }
      }
    );
  }
  /**
   * Method find all cate and gender
   */
  findAllGenderAndCate() {
    this.iService.findAll("gender").subscribe(
      result => {
        if (result) {
          this.listDataGender = result;
        }
      }
    );
  }

  /**
   * Filter by parent
   * @param $event
   */
  filterParent($event) {
    if ($event != null && $event != "") {
      this.productService.filter($event).subscribe(
        result => {
          this.listDataProduct = result;
        }
      );
    } else {
      this.findAllProduct();
    }
  }
  /**
   * Method find all product
  */
  findAllProduct() {
    this.iService.findAll(PATH_URI.PRODUCT).subscribe(
      product => {
        if (product != null) {
          this.listDataProduct = product;
        }
      }, err => { });
  }

  init() {
    let acc = localStorage.getItem("account_^((!");
    let accountName = localStorage.getItem("acc_name_!@!@");
    if (accountName != null) {
      this.isFlatLogin = true;
      this.accountName = accountName;
      this.role_id = JSON.parse(acc).role_id;
      this.id = JSON.parse(acc).id;
    }
    this.findAllGenderAndCate();
    // this.findAllProduct();
  }

  /**
   * Method logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }


  ngOnInit() {
    this.init();
  }

}
