import { Component, OnInit } from '@angular/core';
import { CoreService } from '../service/core.service';
import { Info } from '../module/info';
import { PATH_URI } from '../utils/common/path';
import { Utils } from '../utils/common/EShop';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import * as $ from "jquery";

@Component({
  selector: 'app-account-manager',
  templateUrl: './account-manager.component.html',
  styleUrls: ['./account-manager.component.css']
})
export class AccountManagerComponent implements OnInit {

  listAccount: Info[];
  config: any;
  dataAccountById: any;
  accontInfo: any;
  statusSelected: string;
  roleSelected: string;
  mess = "";

  /**
  * Config pagging
  */
  configPagging() {
    this.config = {
      itemsPerPage: 20,
      currentPage: 1,
      totalItems: this.listAccount
    };
  }

  constructor(private iService: CoreService,
    private router: Router,
    private location: Location,
    private iMessage: ToastrService) {
    this.hideVChat();
    this.configPagging();
  }

  hideVChat() {
    $("#embed_circle").hide();
    $("#embed_fill").hide();
    $("#embed_vchat").hide();
  }
  /**
   * Method do valid combobox
   * @param data 
   */
  doValidCombobox(data: any) {
    let isFlat = true;
    if (data == null || data == "" || data == NaN || data == undefined) {
      isFlat = false;
    }
    return isFlat;
  }


  /**
   * Method update account.
   */
  updateAccount() {
    if (this.dataAccountById != null) {
      let isFlatStaus = this.doValidCombobox(this.statusSelected);
      let isFlatRole = this.doValidCombobox(this.roleSelected);
      if (isFlatStaus || isFlatRole) {
        this.roleSelected == undefined ? (this.roleSelected = "-2") : this.roleSelected;
        this.statusSelected == undefined ? (this.statusSelected = "-1") : this.statusSelected;
        let extension = `?role=${this.roleSelected}&active=${this.statusSelected}`;
        this.iService.findByID("account/update-account-by-id", this.dataAccountById.id + extension).subscribe(
          result => {
            this.iMessage.success("Lưu thành công.", "E-Shopper", {
              timeOut: 3000,
            });
          }
        );
        window.location.reload();
      } else {
        this.iMessage.success("Lưu thất bại.", "E-Shopper", {
          timeOut: 3000,
        });
      }
    }
  }
  /**
   * 
   * @param event 
   */
  pageChanged(event) {
    this.config.currentPage = event;
  }

  /**
   * Method find all acc
   */
  findAllAcc() {
    let role_id = this.accontInfo.role_id - 1;
    this.iService.findAll(PATH_URI.ACCOUNT + PATH_URI.SUOC + role_id).subscribe(
      result => {
        if (result) {
          this.listAccount = result;
        }
      }
    );
  }

  /**
   * Method delete account by id
   */
  deleteAcc(acc: any) {
    if (confirm("Bạn có chắc muốn xóa tài khoản: " + acc.username)) {
      this.iService.deleteByID(PATH_URI.ACCOUNT, acc.id).subscribe(
        result => {
          alert("Xóa thành công!");
          this.findAllAcc();
        }
      );
    }
  }

  /**
   * Method edit acc
   * @param acc 
   */
  editAcc(acc: any) {
    this.dataAccountById = acc;
  }

  /**
   * Method find token from localstorage
   */
  findToken() {
    let acc = localStorage.getItem("account_^((!");
    if (acc != null) {
      this.accontInfo = JSON.parse(acc);
      if (this.accontInfo.role_id != Utils.ROLE.ADMIN) {
        this.login();
      }
    }
  }

  /**
   * Method back to login
   */
  login() {
    this.router.navigate(['/login']);
  }

  /**
   * Method innit
   */
  init() {
    this.findToken();
    this.findAllAcc();
  }

  ngOnInit() {
    this.init();
  }

}
